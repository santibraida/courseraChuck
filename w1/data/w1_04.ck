SinOsc s => dac;

// for loops
for ( 440 => int i; i >= 0; i--) {
    i => s.freq;
    <<< i >>>;
    .01::second => now;
}
for ( 0 => int i; i <= 440; i++) {
    i => s.freq;
    <<< i >>>;
    .01::second => now;
}

// while loops
SinOsc s => dac;

440 => int i1;

while ( i1 >= 0) {
i1 => s.freq;
<<< i1 >>>;
.01::second => now;
i1 --;
}

0 => int i2;

while (i2 <= 440) {
i2 => s.freq;
<<< i2 >>>;
.01::second => now;
i2 ++;
}