SinOsc s => dac;
SqrOsc q => dac;
SawOsc w => dac;
TriOsc t => dac;

0.2 => s.gain;
0.2 => q.gain;
0.2 => w.gain;
0.2 => t.gain;

for (0 => int i; i < 500; i++) {
i => s.freq;
i*2 => q.freq;
i*3 => w.freq;
i*4 => t.freq;
<<< i >>>;
.01::second => now;
}