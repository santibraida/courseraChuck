SinOsc s => dac;
SqrOsc q => dac;
SawOsc w => dac;
TriOsc t => dac;

// one by one. sin
0.5 => s.gain;
0 => q.gain;
0 => w.gain;
0 => t.gain;

for (0 => int i; i < 500; i++) {
    i => s.freq;
    <<< i >>>;
    .001::second => now;
}
// one by one. sqr
0 => s.gain;
0.5 => q.gain;
0 => w.gain;
0 => t.gain;

for (0 => int i; i < 500; i++) {
    i => q.freq;
    <<< i >>>;
    .001::second => now;
}

// one by one. saw
0 => s.gain;
0 => q.gain;
0.5 => w.gain;
0 => t.gain;

for (0 => int i; i < 500; i++) {
    i => w.freq;
    <<< i >>>;
    .001::second => now;
}

// one by one. tri
0 => s.gain;
0 => q.gain;
0 => w.gain;
0.5 => t.gain;

for (0 => int i; i < 500; i++) {
    i => t.freq;
    <<< i >>>;
    .001::second => now;
}

// all together.
0.5 => s.gain;
0.2 => q.gain;
0.5 => w.gain;
0.2 => t.gain;

for (0 => int i; i < 500; i++) {
    //i => s.freq;
    //i => q.freq;
    //i*2 => w.freq;
    i*2 => t.freq;
    <<< i >>>;
    .01::second => now;
}