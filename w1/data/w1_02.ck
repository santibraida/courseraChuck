<<< "Santiago Braida - Assignment 1" >>>;

// first sound
SinOsc s0 => dac;
0.1 => s0.gain;
440 => s0.freq;
0.2:: second => now;

// second sound
SqrOsc s1 => dac;
0.1 => s1.gain;
440 => s1.freq;
0.2:: second => now;

// third sound
TriOsc s2 => dac;
0.1 => s2.gain;
440 => s2.freq;
0.2:: second => now;

// fourth sound
SawOsc s3 => dac;
0.1 => s3.gain;
440 => s3.freq;
0.2:: second => now;