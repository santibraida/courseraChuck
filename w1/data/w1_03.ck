// sound network
SinOsc s => dac;

// middle C freq (in hz)
261.63 => float myFreq;

// volume
0.6 => float myVol;

// set freq
myFreq => s.freq;

// set vol
myVol => s.gain;

1::second => now;