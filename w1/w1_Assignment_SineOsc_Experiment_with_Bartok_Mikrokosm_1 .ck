<<< "Assignment1_SineOsc_Experiment_with_Bartok_Mikrokosm_1" >>>;

// sound network
SinOsc rightHand => dac;
SinOsc leftHand => dac;

// defining volume for left and right hand
0.5 => rightHand.gain;
0.5 => leftHand.gain;

// defining notes freq
261.63 => float c; // c4 freq
293.66 => float d; // d4 freq
329.63 => float e; // e4 freq
349.23 => float f; // f4 freq
392.00 => float g; // g4 freq 

// the loop goes adding up an octave each time and playing the music
for (1 => int i; i <= 2; i++) {
    
    // show loop number
    <<< "loop: " + i >>>;
    
    // the first time doesn't change the octave of the notes
    if (i > 1) {
        c * 2 => c;
        d * 2 => d;
        e * 2 => e;
        f * 2 => f;
        g * 2 => g;
    }
    
    c => rightHand.freq;
    // shows note "C" freq. in Hz.
    <<< "c" + (2 + i) + ": " + c + " Hz." >>>;
    c / 2 => leftHand.freq;
    <<< "c" + ((2 + i) - 1) + ": " + (c / 2) + " Hz." >>>;
    1 :: second => now;

    d => rightHand.freq;
    d / 2 => leftHand.freq;
    1 :: second => now;

    e => rightHand.freq;
    e / 2 => leftHand.freq;
    1 :: second => now;

    f => rightHand.freq;
    f / 2 => leftHand.freq;
    1 :: second => now;

    e => rightHand.freq;
    e / 2 => leftHand.freq;
    1 :: second => now;

    d => rightHand.freq;
    d / 2 => leftHand.freq;
    1 :: second => now;

    // setting volume to zero
    0 => rightHand.gain;
    0 => leftHand.gain;
    1 :: second => now;

    // setting volume back to normal
    0.5 => rightHand.gain;
    0.5 => leftHand.gain;

    e => rightHand.freq;
    e / 2 => leftHand.freq;
    1 :: second => now;

    f => rightHand.freq;
    f / 2 => leftHand.freq;
    1 :: second => now;

    g => rightHand.freq;
    g / 2 => leftHand.freq;
    1 :: second => now;

    f => rightHand.freq;
    f / 2 => leftHand.freq;
    1 :: second => now;

    e => rightHand.freq;
    e / 2 => leftHand.freq;
    1 :: second => now;

    d => rightHand.freq;
    d / 2 => leftHand.freq;
    1 :: second => now;

    c => rightHand.freq;
    c / 2 => leftHand.freq;
    2 :: second => now;

}