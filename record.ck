dac => WvOut2 w => blackhole;
// adds .wav to current filename 
// if called from score.ck, creates a sound file score.ck.wav
// by using path() it ends up in the same folder; path() gives folder + filename
me.path() + ".wav" => w.wavFilename;
// start recording
1 => w.record;

// here goes the chuck stuff

// stop recording sound file
0 => w.record;