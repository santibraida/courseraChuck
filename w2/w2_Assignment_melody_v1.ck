<<< "Assignment_2_Melody_1_First_Time_Ever" >>>;
// This is the first time in my whole life I wrote a melody,
// I'm not an accomplished musician (until now at least, I'm learning to play the piano).
// Left and right hands idea are from "as if playing" a piano.
// Please take a look at the console for information.

// Sound chain

// right hand setup
SawOsc rightHand => Pan2 rightPosition => dac.right;
1 => rightHand.gain;

// left hand setup
SqrOsc leftHand => Pan2 leftPosition => dac.left;
0.6 => leftHand.gain;

0 => int totalTime;

// Array declaration of MIDI notes and duration

// 50 MIDI equals to D
// 52 MIDI equals to E
// 53 MIDI equals to F
// 55 MIDI equals to G
// 57 MIDI equals to A
// 59 MIDI equals to B
// 60 MIDI equals to C
// 62 MIDI equals to D

[50, 55, 59, 60, 62, 60, 57, 57, 55, 57, 59, 60, 62, 60, 62, 64, 64, 62, 60, 59, 55, 57, 55, 53, 52, 50, 48] @=> int Melody[];

// Array declaration of notes duration

// 250 ms  equals to quarter note
// 500 ms  equals to half note
// 1000 ms equals to whole note

[250, 250, 250, 250, 250, 250, 500, 250, 250, 500, 250, 250, 500, 250, 250, 500, 250, 250, 500, 250, 250, 250, 250, 1000, 500, 500, 1000] @=> int Notes[];

// 1st. loop, 3 times to reach assignment 30 seconds, each time the octaves are randomly choosen and panning position reseted.
for ( 1 => int j; j <= 3; j++) {

    // Panning position origins
    1.0 => float rightPanPosition; // Rightmost position
    -1.0 => float leftPanPosition; // Leftmost position

    // Randomly chosen octaves, one for right hand and another for the left hand
    [-12, 0, 12, 24, 36] @=> int OctaveVariation[];
    Math.random2(0, 4) => int rightHandSelector;
    Math.random2(0, 4) => int leftHandSelector;

    // 2nd. loop, here melody is played
    for ( 0 => int i; i < Melody.cap(); i ++) {

        Std.mtof(Melody[i] + OctaveVariation[rightHandSelector]) => rightHand.freq;
        rightPanPosition => rightPosition.pan;
        rightPanPosition - .075 => rightPanPosition; // Moving to the left

        Std.mtof(Melody[i] + OctaveVariation[leftHandSelector]) => leftHand.freq;
        leftPanPosition => leftPosition.pan;
        leftPanPosition + .075 => leftPanPosition; // Moving to the right

        Notes[i] + totalTime => totalTime; // Total played time to moment

        Notes[i]::ms => now; // Advance time

        // information of what is going on is output to the console
        <<< "Index:", i >>>;

        <<< "Octave variation for Right Hand: ", OctaveVariation[rightHandSelector] >>>;
        <<< "Octave variation for Left Hand: ", OctaveVariation[leftHandSelector] >>>;

        <<< "Right Hand panning position: ", rightPanPosition >>>;
        <<< "Left Hand panning position: ", leftPanPosition >>>;

        <<< "Right Hand MIDI note:", (Melody[i] + OctaveVariation[rightHandSelector]) >>>;
        <<< "Left Hand MIDI note:", (Melody[i] + OctaveVariation[leftHandSelector]) >>>;

        <<< "Duration of actual note:", Notes[i], "Milliseconds." >>>;
        <<< "Total time of composition: ", totalTime, "Milliseconds." >>>;

    }

}