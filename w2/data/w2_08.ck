// sound chain
SqrOsc s => dac;

Math.srandom(134); // set seed

// infinite loop

while (true) {

    Math.random2(20, 500) => int i; // generate random int
    Math.random2f(.2, .5) => float v; // generate random float

    <<< i, v >>>; // print out variables

    i => s.freq; // update freq
    v => s.gain; // update gain

    .5::second => now;

}