// array declaration
[54, 56, 62, 54, 48, 50, 52] @=> int A[];

// array look up
// how do we get data out of our array?

A[1] => int data;

// what does this print out?
<<< data >>>;