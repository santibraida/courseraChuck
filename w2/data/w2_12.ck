// array declaration 1st way
int A[7];
54 => A[0];
56 => A[1];
62 => A[2];
54 => A[3];
48 => A[4];
50 => A[5];
52 => A[6];

// array declaration 2nd way
[54, 56, 62, 54, 48, 50, 52] @=> int A[];