// sound chain
SqrOsc s => dac;
0.2 => s.gain;

// array declaration
[54, 56, 62, 54, 48, 50, 52] @=> int A[];

// loop
for ( 0 => int i; i < A.cap(); i ++) {
    <<< i, A[i] >>>;
    Std.mtof(A[i]) => s.freq;
    .5::second => now;
}