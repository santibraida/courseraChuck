<<< "melody with instruments" >>>;

// Sound chain

// right hand setup
StifKarp rightHand => dac.right;

// left hand setup
StifKarp leftHand => dac.left;

0::second => dur totalTime;

// Array declaration of MIDI notes and duration

// 50 MIDI equals to D
// 52 MIDI equals to E
// 53 MIDI equals to F
// 55 MIDI equals to G
// 57 MIDI equals to A
// 59 MIDI equals to B
// 60 MIDI equals to C
// 62 MIDI equals to D

[50, 55, 59, 60, 62, 60, 57, 57, 55, 57, 59, 60, 62, 60, 62, 64, 64, 62, 60, 59, 55, 57, 55, 53, 52, 50, 48] @=> int Melody[];

// Array declaration of notes duration

// time reference
.25::second => dur bar;

// .5 second is a quarter
1::bar => dur q;
// 2 quarters is half
2::bar => dur h;
// 4 quarters is whole
4::bar => dur w;

[q, q, q, q, q, q, h, q, q, h, q, q, h, q, q, h, q, q, h, q, q, q, q, w, h, h, w] @=> dur Notes[];

// infinite loop
while (true) {

    // Randomly choosen octaves, one for right hand and another for the left hand
    [-12, 0, 12, 24, 36] @=> int OctaveVariation[];
    Math.random2(0, 4) => int selector;
    
    Math.random2f(.5, .8) => float rightOnOff;
    Math.random2f(.5, .8) => float leftOnOff;

    // 2Nd. loop, here melody is played
    for ( 0 => int i; i < Melody.cap(); i ++) {

        rightHand.noteOn(rightOnOff);
        rightHand.freq(Melody[i] + OctaveVariation[selector]);
        leftHand.noteOff(rightOnOff);

        leftHand.noteOn(leftOnOff);
        leftHand.freq(Melody[i] + OctaveVariation[selector]);
        leftHand.noteOff(leftOnOff);

        Notes[i] + totalTime => totalTime; // Total played time to moment

        Notes[i] => now; // Advance time

        // information of what is going on is output to the console
        <<< "Index:", i >>>;

        <<< "Octave variation: ", OctaveVariation[selector] >>>;

        <<< "MIDI note:", Melody[i] >>>;
        <<< "Duration of actual note:", Notes[i], "seconds." >>>;
        <<< "Total time of composition: ", totalTime, "seconds." >>>;

    }

}