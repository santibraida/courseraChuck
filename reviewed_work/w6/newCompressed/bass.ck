// bass.ck
// Assignment 6 - "Using a Spork to Fork"

// Our sound chain
Mandolin bass => NRev r => Gain master => dac;

// Scale to play
[51,53,54,53,51,46,48,49,48,46] @=> int scale[];

// Params
0.1 => r.mix;
0.0 => bass.stringDamping;
0.02 => bass.stringDetune;
0.05 => bass.bodySize;
0.3 => master.gain;
0 => int pos;

// Loop de loop
while(true){
    if( pos >= scale.cap() ) 0 => pos;
    Std.mtof(scale[pos] - 12) => bass.freq;
    0.3 => bass.pluckPos;
    1 => bass.noteOn;
    0.62::second => now;
    1 => bass.noteOff;
    0.005::second => now;
    pos++;
}