// drums.ck
// Assignment 6 - "Using a Spork to Fork"

// Our sound chain
SndBuf hi1 => Gain master => dac;
SndBuf hi2 => master => dac;
SndBuf hi3 => master => dac;
SndBuf kick => Gain minimaster => dac;
SndBuf snare => minimaster => dac;
SndBuf stick => minimaster => dac;

0.5 => master.gain;
0.5 => minimaster.gain;

// Load audio files
me.dir(-1) + "/audio/hihat_01.wav" => hi1.read;
me.dir(-1) + "/audio/hihat_02.wav" => hi2.read;
me.dir(-1) + "/audio/hihat_03.wav" => hi3.read;
me.dir(-1) + "/audio/kick_03.wav" => kick.read;
me.dir(-1) + "/audio/snare_01.wav" => snare.read;
me.dir(-1) + "/audio/snare_03.wav" => stick.read;
// Set playheads
hi1.samples() => hi1.pos;
hi2.samples() => hi2.pos;
hi3.samples() => hi3.pos;
kick.samples() => kick.pos;
snare.samples() => snare.pos;
stick.samples() => stick.pos;

repeat( 4 ){
    0 => stick.pos;
    0.625::second => now;
}

// Create counter
0 => int count;

// Loop de loop
while(true){
    if( count%4 == 0 ) 0 => hi2.pos;
    if( count%4 == 1 ) 0 => hi2.pos => kick.pos;
    if( count%4 == 2 ) 0 => hi3.pos => snare.pos;
    if( count%4 == 3 ) 0 => hi1.pos => kick.pos;
    0.625::second => now;
    count++;
}