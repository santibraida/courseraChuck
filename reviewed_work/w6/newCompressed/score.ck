// score.ck
// Assignment 6 - "Using a Spork to Fork"

SinOsc s => Pan2 p => dac;
0.0 => s.gain;

[51,53,54,53,51,46,48,49,48,46] @=> int scale[];
fun void sine(){
  0.5 => s.gain;
  for( Math.random2(0, scale.cap()) => int i; i < scale.cap(); i++ ){
    Std.mtof(scale[i]) => s.freq;
    Math.sin(now/1::second*2*pi) => p.pan;
    0.2::second => now;
  }
}

Machine.add(me.dir() + "/drums.ck") => int drumID;
Machine.add(me.dir() + "/sitar.ck") => int sitarID;

10::second => now;

Machine.add(me.dir() + "/bass.ck") => int bassID;

15::second => now;

Machine.remove(drumID);
Machine.remove(bassID);
Machine.remove(sitarID);

spork ~ sine();
5::second => now;