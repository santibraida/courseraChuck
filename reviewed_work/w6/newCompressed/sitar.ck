// sitar.ck
// Assignment 6 - "Using a Spork to Fork"

// Our sound chain
Sitar sitar => NRev r => Gain master => dac;

// Scale to play
[51,53,54,53,51,46,48,49,48,46] @=> int scale[];

// Params
0.05 => r.mix;
0.8 => master.gain;
0 => int pos;

// Loop de loop
while(true){
    if( pos >= scale.cap() ) 0 => pos;
    Std.mtof(scale[pos] - 12) => sitar.freq;
    1 => sitar.noteOn;
    0.62::second => now;
    1 => sitar.noteOff;
    0.005::second => now;
    pos++;
}