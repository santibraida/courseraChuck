//oscar.ck
<<<"Assignment_6_HipChuck">>>;
//for panning oscilator 
Pan2 oscPan => dac;
0.5 => oscPan.gain;
// sin osc for tune
SinOsc osc => oscPan;
0.0 => osc.gain;
// adding some delay with reverb
Delay del => NRev rev => oscPan;
// delay/delay reverb settings
0.3 => rev.mix;
0.2::second => del.max => del.delay;
0.5 => del.gain;

// osc to delay
osc => del;

// subset of scale
[46,     49, 51, 53,     56, 58] @=> int playScale[];

// quarter note, "main compositional pulse"
0.625::second => dur n4;
n4 * 4 => dur n1;
n4 / 2 => dur n8;
n4 / 4 => dur n16;
n4 / 8 => dur n32;

// octave multiplier - freq * octaveFactor
2 => float octaveFactor;
// play volume setting for osc
0.1 => float gainSetting;

// function to pan osc, to be sporked
fun void oscarPan()
{
    while( true )
    {
        // pan between -1 and 1
        Math.sin(now / second) => float panner;
        //<<< panner >>>;
        panner => oscPan.pan;
        5::ms => now;
    }
}

// spork function
spork ~oscarPan();

// main loop, play oscillator tones
while( true )
{
    // getting a little lazy, cut and paste to make it work :)
    gainSetting => osc.gain;
    Std.mtof( playScale[0] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[1] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[2] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[3] ) * octaveFactor => osc.freq;
    n4 => now;
    Std.mtof( playScale[4] ) * octaveFactor => osc.freq;
    n8 => now;

    0.0 => osc.gain;
    n4 => now;

    n4 => now;
    n4 => now;
    n4 => now;
    n8 => now;

    0.0 => osc.gain;
    n8 => now;

    gainSetting => osc.gain;
    Std.mtof( playScale[0] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[1] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[2] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[3] ) * octaveFactor => osc.freq;
    n4 => now;
    Std.mtof( playScale[4] ) * octaveFactor => osc.freq;
    n4 => now;
    Std.mtof( playScale[3] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[2] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[1] ) * octaveFactor => osc.freq;
    n8 => now;
    Std.mtof( playScale[0] ) * octaveFactor => osc.freq;
    n8 => now;

    0.0 => osc.gain;
    n4 => now;
    n4 => now;
    n8 => now;
}
