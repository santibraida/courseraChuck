// drums.ck
<<<"Assignment_6_HipChuck">>>;

// quarter note, "main compositional pulse"
0.625::second => dur n4;
n4 / 2 => dur n8;

//all drums through master control
Pan2 drumMaster => dac;
0.4 => drumMaster.gain;
//sndBuf for kick drum
SndBuf kick => drumMaster;
//sndBuf for snare drum, with reverb
SndBuf tap => NRev tapRev => drumMaster;
0.1 => tapRev.mix;

// load wav files
me.dir(-1) + "/audio/" => string audioDir;
audioDir + "snare_03.wav" => tap.read; 
tap.samples() => tap.pos;
audioDir + "kick_03.wav" => kick.read; 
0.8 => kick.rate;
kick.samples() => kick.pos;

// initialize counter and beat for drum loop
0 => int counter => int beat;
while(true)
{
    // TWO bars by 1/8 notes ... 16 beats
    counter % 16 => beat;

    // kick
    if( (beat==0) || (beat==7) )
        0 => kick.pos;
    if( (beat==4) || (beat==9) || (beat==12) )
        0 => tap.pos;

    // tap
    n8 => now;
    counter++;
}
