// score.ck
<<<"Assignment_6_HipChuck">>>;

// Add your composition files when you want them to come in

0.625::second => dur n4;
n4 / 2 => dur n8;
n4 / 4 => dur n16;

Machine.add(me.dir() + "/drums.ck") => int drumID;
8::n4 => now;

Machine.add(me.dir() + "/bass.ck") => int bassID;
8::n4 => now;

Machine.add(me.dir() + "/piano.ck") => int pianoID;
15::n8 => now;

Machine.add(me.dir() + "/oscar.ck") => int oscarID;
17::n8 => now;

7::n4 => now;
Machine.remove(oscarID);
1::n4 => now;

Machine.remove(drumID);
8::n4 => now;

Machine.remove(bassID);
Machine.remove(pianoID);
