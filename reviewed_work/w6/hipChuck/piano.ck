// piano.ck
<<<"Assignment_6_HipChuck">>>;

// scale
[46, 48, 49, 51, 53, 54, 56, 58] @=> int scale[];
// quarter note, "main compositional pulse"
0.625::second => dur n4;
n4 / 2 => dur n8;

// piano instrument x 3 for chord
Rhodey piano[3];
// some reverb
Pan2 pianoMaster => NRev pianoRev => dac;
0.1 => pianoRev.mix;

int i;

// init piano settings, send to dac
for(0=>i;i<piano.cap();i++)
{
    piano[i] => pianoMaster;
    1.0 / piano.cap() => piano[i].gain;
}

0.4 => pianoMaster.gain;

// piano note/beat, index of scale
//  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15 // beat number
[  -1,  0, -1, -1, -1, -1, -1,  0, -1,  1, -1, -1, 0, -1,  0, -1  ] @=> int pianoBeat[];

// two chords to play
[ [0, 2, 4] , [2, 4, 6] ] @=> int pianoChord[][];


// initialize counter and beat for drum loop
0 => int counter => int beat;
// main loop, play chords on beats defined in pianoBeat[]
while(true)
{
    counter % 16 => beat;
    if( pianoBeat[beat] == -1 )  // don't play when -1
    {
        // all pianos set noteOff
        for(0=>i;i<piano.cap();i++)
            1 => piano[i].noteOff;
    }
    else // play
    {
        // all pianos set noteOn and freq... play chord
        for(0=>i;i<piano.cap();i++)
        {
            Std.mtof( scale[ pianoChord[pianoBeat[ beat ]][i] ] ) * 2.0 => piano[i].freq;
            0.5 => piano[i].noteOn;
        }
    }
    n8 => now;

    counter++;
}
