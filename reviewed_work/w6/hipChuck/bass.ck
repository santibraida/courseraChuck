// bass.ck
<<<"Assignment_6_HipChuck">>>;

//scale
[46, 48, 49, 51, 53, 54, 56, 58] @=> int scale[];
// quarter note, "main compositional pulse"
0.625::second => dur n4;
n4 / 2 => dur n8;

// use low piano for bass
Rhodey bass => Pan2 bassMaster => dac;
// trying some bass settings, these seem to work
0.5 => bass.lfoDepth;
//6 => bass.lfoSpeed;
//0.5 => bass.afterTouch;
0.4 => bass.controlOne;
0.0 => bass.controlTwo;
0.4 => bassMaster.gain;

// bass note/beat, index of scale
//  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
[  -1,  0, -1, -1, -1, -1, -1,  0,  2,  3, -1, -1,  2, -1,  0, -1  ] @=> int bassBeat[];

// initialize counter and beat for drum loop
0 => int counter => int beat;
// main loop, play bass notes defined in bassBeat
while(true)
{
    counter % 16 => beat;
    if( bassBeat[beat] == -1 )  // don't play if -1
    {
        1 => bass.noteOff;
    }
    else // play
    {
        Std.mtof( scale[ bassBeat[ beat ] ] ) / 2.0 => bass.freq;
        0.5 => bass.noteOn;
    }
    n8 => now;

    counter++;
}
