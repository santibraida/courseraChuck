// Assignment6_SmallBand
// clarinet.ck

0.625::second => dur quarter_note;

// Bb Aeolian mode
[
Std.mtof(46) / 2.0,         // Bb1
Std.mtof(48) / 2.0,         // C2
Std.mtof(49) / 2.0,         // Db2
Std.mtof(51) / 2.0,         // Eb2
Std.mtof(53) / 2.0,         // F2
Std.mtof(54) / 2.0,         // Gb2
Std.mtof(56) / 2.0,         // Ab2
Std.mtof(46),               // Bb2
Std.mtof(48),               // C3
Std.mtof(49),               // Db3
Std.mtof(51),               // Eb3
Std.mtof(53),               // F3
Std.mtof(54),               // Gb3
Std.mtof(56),               // Ab3
Std.mtof(58),               // Bb3
Std.mtof(48) * 2.0,         // C4
Std.mtof(49) * 2.0,         // Db4
Std.mtof(51) * 2.0,         // Eb4
Std.mtof(53) * 2.0,         // F4
Std.mtof(54) * 2.0,         // Gb4
Std.mtof(56) * 2.0,         // Ab4
Std.mtof(58) * 2.0,         // Bb5
Std.mtof(48) * 2.0 * 2.0,   // C5
Std.mtof(49) * 2.0 * 2.0,   // Db5
Std.mtof(51) * 2.0 * 2.0,   // Eb5
Std.mtof(53) * 2.0 * 2.0,   // F5
Std.mtof(54) * 2.0 * 2.0,   // Gb5
Std.mtof(56) * 2.0 * 2.0,   // Ab5
Std.mtof(58) * 2.0 * 2.0    // Bb5
] @=> float midi_notes[];

// setup the clarinet
Clarinet clarinet => JCRev r => Pan2 p => dac;
.5 => r.gain;
.04 => r.mix;

1.0 => clarinet.clear;

0.3 => clarinet.reed;
0.05 => clarinet.noiseGain;
10 => clarinet.vibratoFreq;
0.5  => clarinet.vibratoGain;
0.03 => clarinet.pressure;

Math.srandom(42);

// note duration pattern 1
[
1.0, 0.5, 0.5, 0.0, 0.5, 1.0, 0.5
] @=> float pattern1[];

// note duration pattern 2
[
1.0, 0.5, 0.5, 1.0, 1.5, 0.0, 0.5
] @=> float pattern2[];

//
// playNotes
//
// note - base note to play
// pattern - timing pattern for the notes
//
// plays notes using the given timing pattern
//
fun void playNotes(int note, float pattern[])
{
    Math.random2f(-1.0, 1.0) => p.pan;

    midi_notes[note] => clarinet.freq;
    0.6 => clarinet.noteOn;
    quarter_note * pattern[0] => now;

    midi_notes[note + 2] => clarinet.freq;
    0.5 => clarinet.noteOn;
    quarter_note * pattern[1] => now;

    midi_notes[note] => clarinet.freq;
    0.35 => clarinet.noteOn;
    quarter_note * pattern[2] => now;

    1.0 => clarinet.noteOff;
    quarter_note * pattern[3] => now;

    midi_notes[note + 3] => clarinet.freq;
    0.5 => clarinet.noteOn;
    quarter_note * pattern[4] => now;

    midi_notes[note] => clarinet.freq;
    0.6 => clarinet.noteOn;
    quarter_note * pattern[5] => now;

    1.0 => clarinet.noteOff;
    quarter_note  * pattern[6] => now;
}

playNotes(15, pattern1);
playNotes(17, pattern1);
playNotes(18, pattern1);

playNotes(16, pattern1);
playNotes(18, pattern1);
playNotes(19, pattern1);

1.0 => clarinet.noteOff;
quarter_note => now;

playNotes(15, pattern2);
playNotes(18, pattern1);


playNotes(16, pattern2);
playNotes(19, pattern1);

1.0 => clarinet.noteOff;
quarter_note * 3 => now;

midi_notes[15] => clarinet.freq;
1.0 => clarinet.noteOn;
quarter_note * 2 => now;
