// Assignment6_SmallBand
// initialize.ck

<<< "Assignment6 ", "SmallBand" >>>;
//
// Use of Machine.add() to launch files
//      initialize.ck, score.ck
// Use of spork ~ to call functions concurrently
//      drums.ck
// Use of Oscillator
//      elec.ck
// Use of SndBuf
//      drums.ck
// Use of at least one STK instrument
//      bass.ck, guitar.ck, clarinet.ck
// Use of at least one STK audio effect (e.g. Chorus, Delay, JCRev)
//      clarinet.ck, elec.ck
// Use of if/else statements
//      bass.ck, elec.ck
// Use of for loop or while 
//      bass.ck, guitar.ck, drums.ck, elec.ck
// Use of variables
//      score.ck, bass.ck, guitar.ck, clarinet.ck, drums.ck, elec.ck
// Use of comments
//      all files
// Std.mtof()
//      bass.ck, guitar.ck, clarinet.ck, elec.ck
// Random Number
//      clarinet.ck
// Use of Arrays
//      bass.ck, guitar.ck, clarinet.ck, elec.ck
// Use of Panning
//      clarinet.ck
// Use of right timing (0.625::second quarter notes)
//      bass.ck, guitar.ck, clarinet.ck, drums.ck, elec.ck
// Use of right melodic notes (Bb Aeolian scale)
//      bass.ck, guitar.ck, clarinet.ck, elec.ck
// Use of at least one hand-written function
//      bass.ck, guitar.ck, clarinet.ck, drums.ck, elec.ck
//

// Add score file
Machine.add(me.dir() + "/score.ck");