// Assignment6_SmallBand
// elec.ck

0.625::second => dur quarter_note;

// Bb Aeolian mode
[
Std.mtof(46) / 2.0,         // Bb1
Std.mtof(48) / 2.0,         // C2
Std.mtof(49) / 2.0,         // Db2
Std.mtof(51) / 2.0,         // Eb2
Std.mtof(53) / 2.0,         // F2
Std.mtof(54) / 2.0,         // Gb2
Std.mtof(56) / 2.0,         // Ab2
Std.mtof(46),               // Bb2
Std.mtof(48),               // C3
Std.mtof(49),               // Db3
Std.mtof(51),               // Eb3
Std.mtof(53),               // F3
Std.mtof(54),               // Gb3
Std.mtof(56),               // Ab3
Std.mtof(58),               // Bb3
Std.mtof(48) * 2.0,         // C4
Std.mtof(49) * 2.0,         // Db4
Std.mtof(51) * 2.0,         // Eb4
Std.mtof(53) * 2.0,         // F4
Std.mtof(54) * 2.0,         // Gb4
Std.mtof(56) * 2.0,         // Ab4
Std.mtof(58) * 2.0,         // Bb5
Std.mtof(48) * 2.0 * 2.0,   // C5
Std.mtof(49) * 2.0 * 2.0,   // Db5
Std.mtof(51) * 2.0 * 2.0,   // Eb5
Std.mtof(53) * 2.0 * 2.0,   // F5
Std.mtof(54) * 2.0 * 2.0,   // Gb5
Std.mtof(56) * 2.0 * 2.0,   // Ab5
Std.mtof(58) * 2.0 * 2.0    // Bb5
] @=> float midi_notes[];

//
// setup the electronic instrument
//
SinOsc elec => ADSR adsr => PRCRev reverb => Gain adsr_gain => dac.left;
1.0 => elec.gain;

1.0 => reverb.mix;
0.15 => adsr_gain.gain;
20::ms => adsr.attackTime;
10::ms => adsr.decayTime;
0.3 => adsr.sustainLevel;
100::ms => adsr.releaseTime;

//
// playNotes
//
// note - the note to play from midi_notes
//
// play a note
//
fun void playNotes(int note)
{
    midi_notes[note] => elec.freq;
    1 => adsr.keyOn;
    quarter_note => now;
    
    1 => adsr.keyOff;
    quarter_note => now;
}

//play the electronic part
for (0=> int i; i < 4; i++)
{
    playNotes(16);
    playNotes(19);
    playNotes(18);
    playNotes(21);
    if (i == 3)
    {
        quarter_note * 2 => now;
        //playNotes(19);
        playNotes(16);
    }
    else
    {
        playNotes(16);
        playNotes(19);
    }
}