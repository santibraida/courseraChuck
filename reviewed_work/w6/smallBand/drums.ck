// Assignment6_SmallBand
// drums.ck

0.625::second => dur quarter_note;

//
// playSnare
//
// sets up and plays the snare part
//
fun void playSnare()
{
    SndBuf snare => dac;
    me.dir(-1) + "/audio/snare_01.wav" => snare.read;
    snare.samples() => snare.pos;

    for (0 => int i; i < 12; i++)
    {
        0 => snare.pos;
        1.0 => snare.gain;
        
        quarter_note * 2 => now;

        0 => snare.pos;
        0.6 => snare.gain;
        quarter_note / 2 => now;

        0 => snare.pos;
        0.6 => snare.gain;
        quarter_note * 1.5 => now;
    }
}

//
// playHiHat
//
// sets up and plays the hihat part
//
fun void playHiHat()
{
    SndBuf hihat => dac;
    me.dir(-1) + "/audio/hihat_03.wav" => hihat.read;
    hihat.samples() => hihat.pos;

    for (0 => int i; i < 12; i++)
    {
        0 => hihat.pos;
        0.4 =>hihat.gain;
        quarter_note => now;

        0 => hihat.pos;
        0.3 =>hihat.gain;
        quarter_note => now;

        0 => hihat.pos;
        0.3 =>hihat.gain;
        quarter_note * 2 => now;
    }
}

//
// playKick
//
// sets up and plays the kick part
//
fun void playKick()
{
    SndBuf kick => dac;
    me.dir(-1) + "/audio/kick_03.wav" => kick.read;
    kick.samples() => kick.pos;

    for (0 => int i; i < 12; i++)
    {
        quarter_note * 3 => now;

        0 => kick.pos;
        quarter_note => now;
    }
}

//
// playCrash
//
// sets up and plays the crash part
//
fun void playCrash()
{
    SndBuf crash => JCRev crash_rev => dac;
    me.dir(-1) + "/audio/hihat_04.wav" => crash.read;
    0.4 => crash_rev.mix;
    0.8 => crash.rate;
    crash.samples() => crash.pos;

    for (0 => int i; i < 3; i++)
    {
        quarter_note * 15 => now;

        0 => crash.pos;
        quarter_note => now;
    }
    // to let crash complete without abrupt cutoff
    1::second => now;
}

spork ~ playSnare();
spork ~ playHiHat();
spork ~ playKick();
spork ~ playCrash();

// to let crash complete without abrupt cutoff
31::second => now;
