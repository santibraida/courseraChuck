// Assignment6_SmallBand
// bass.ck

0.625::second => dur quarter_note;

// Bb Aeolian mode
[
Std.mtof(46) / 2.0,         // Bb1
Std.mtof(48) / 2.0,         // C2
Std.mtof(49) / 2.0,         // Db2
Std.mtof(51) / 2.0,         // Eb2
Std.mtof(53) / 2.0,         // F2
Std.mtof(54) / 2.0,         // Gb2
Std.mtof(56) / 2.0,         // Ab2
Std.mtof(46),               // Bb2
Std.mtof(48),               // C3
Std.mtof(49),               // Db3
Std.mtof(51),               // Eb3
Std.mtof(53),               // F3
Std.mtof(54),               // Gb3
Std.mtof(56),               // Ab3
Std.mtof(58),               // Bb3
Std.mtof(48) * 2.0,         // C4
Std.mtof(49) * 2.0,         // Db4
Std.mtof(51) * 2.0,         // Eb4
Std.mtof(53) * 2.0,         // F4
Std.mtof(54) * 2.0,         // Gb4
Std.mtof(56) * 2.0,         // Ab4
Std.mtof(58) * 2.0,         // Bb5
Std.mtof(48) * 2.0 * 2.0,   // C5
Std.mtof(49) * 2.0 * 2.0,   // Db5
Std.mtof(51) * 2.0 * 2.0,   // Eb5
Std.mtof(53) * 2.0 * 2.0,   // F5
Std.mtof(54) * 2.0 * 2.0,   // Gb5
Std.mtof(56) * 2.0 * 2.0,   // Ab5
Std.mtof(58) * 2.0 * 2.0    // Bb5
] @=> float midi_notes[];

// setup the bass
StifKarp bass => dac.left;
0.5 => bass.sustain;
0.9999 => bass.stretch;
0.4 => bass.pickupPosition;

//
// playNotes
//
// note - the note to play from midi_notes
//
// play a little pattern
//
fun void playNotes(int note)
{
    midi_notes[note] => bass.freq;
    1.0 => bass.noteOn;
    quarter_note => now;
    0.5 => bass.noteOn;
    quarter_note/2 => now;
    0.5 => bass.noteOn;
    quarter_note/2 => now;
}

//play the bass part
for (0=> int i; i < 4; i++)
{
    playNotes(7);
    playNotes(10);
    playNotes(9);
    playNotes(12);
    if (i == 3)
    {
        //playNotes(10);
        quarter_note * 2 => now;
        playNotes(7);
    }
    else
    {
        playNotes(7);
        playNotes(10);
    }
}
