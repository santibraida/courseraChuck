// Assignment6_SmallBand
// guitar.ck

0.625::second => dur quarter_note;
10::ms => dur strumTime;

// Bb Aeolian mode
[
Std.mtof(46) / 2.0,         // Bb1
Std.mtof(48) / 2.0,         // C2
Std.mtof(49) / 2.0,         // Db2
Std.mtof(51) / 2.0,         // Eb2
Std.mtof(53) / 2.0,         // F2
Std.mtof(54) / 2.0,         // Gb2
Std.mtof(56) / 2.0,         // Ab2
Std.mtof(46),               // Bb2
Std.mtof(48),               // C3
Std.mtof(49),               // Db3
Std.mtof(51),               // Eb3
Std.mtof(53),               // F3
Std.mtof(54),               // Gb3
Std.mtof(56),               // Ab3
Std.mtof(58),               // Bb3
Std.mtof(48) * 2.0,         // C4
Std.mtof(49) * 2.0,         // Db4
Std.mtof(51) * 2.0,         // Eb4
Std.mtof(53) * 2.0,         // F4
Std.mtof(54) * 2.0,         // Gb4
Std.mtof(56) * 2.0,         // Ab4
Std.mtof(58) * 2.0,         // Bb5
Std.mtof(48) * 2.0 * 2.0,   // C5
Std.mtof(49) * 2.0 * 2.0,   // Db5
Std.mtof(51) * 2.0 * 2.0,   // Eb5
Std.mtof(53) * 2.0 * 2.0,   // F5
Std.mtof(54) * 2.0 * 2.0,   // Gb5
Std.mtof(56) * 2.0 * 2.0,   // Ab5
Std.mtof(58) * 2.0 * 2.0    // Bb5
] @=> float midi_notes[];

// setup the guitar
Mandolin guitar[3];
Gain master => dac.right;

connectGain();
setBodySize(0.8);
setPluckPos(0.4);
setStringDamping(0.9);
setStringDetune(0.0);

//
// connectGain
//
// connect all of the guitar "strings" to the master gain
//
fun void connectGain()
{
    for (0=> int i; i < guitar.cap(); i++)
    {
        guitar[i] => master;
        1.0 / guitar.cap() => master.gain;
    }
}

//
// setBodySize
//
// v - value to to be used for body size
//
// set all of the guitar "strings" to the same body size
//
fun void setBodySize(float v)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        v => guitar[i].bodySize;
    }
}
    
//
// setPluckPos
//
// v - value to to be used for pluck position
//
// set all of the guitar "strings" to the same pluck position
//
fun void setPluckPos(float v)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        v => guitar[i].pluckPos;
    }
}

//
// setStringDamping
//
// v - value to to be used for string damping
//
// set all of the guitar "strings" to the same string damping
//
fun void setStringDamping(float v)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        v => guitar[i].stringDamping;
    }
}

//
// setStringDetune
//
// v - value to to be used for string detune
//
// set all of the guitar "strings" to the same string detune
//
fun void setStringDetune(float v)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        v => guitar[i].stringDetune;
    }
}

//
// setupChord
//
// note - base note of chord
//
// setup the guitar "strings" to play a chord
//
fun void setupChord(int note)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        midi_notes[note + i * 2] => guitar[i].freq;
    }
}

//
// strumUp
//
// force - how hard to strum
//
// activate guitar "strings" starting with the highest note
//
fun dur strumUp(float force)
{
    for (guitar.cap() -1=> int i; i >= 0; i--)
    {
        force => guitar[i].pluck;
        strumTime => now;
    }
    return guitar.cap() * strumTime;
}

//
// strumDown
//
// force - how hard to strum
//
// activate guitar "strings" starting with the lowest note
//
fun dur strumDown(float force)
{
    for (0 => int i; i < guitar.cap(); i++)
    {
        force => guitar[i].pluck;
        strumTime => now;
    }
    return guitar.cap() * strumTime;
}

//
// playNotes
//
// note - base note for the part
//
// play guitar chords
//
fun void playNotes(int note)
{
    setupChord(note);
    strumDown(1.0) => dur strumTime;
    quarter_note - strumTime => now;
    strumUp(0.5);
    quarter_note /2 - strumTime => now;
    strumDown(0.5);
    quarter_note /2 - strumTime => now;
    quarter_note => now;
    
    setupChord(note - 2);
    strumUp(0.5);
    quarter_note /2 - strumTime => now;
    setupChord(note);
    strumDown(0.5);
    quarter_note /2 - strumTime => now;
    
}

quarter_note * 4 => now;

playNotes(19);
playNotes(20);

quarter_note * 4 => now;
playNotes(20);
playNotes(21);

playNotes(15);
playNotes(17);

playNotes(15);
playNotes(17);
quarter_note * 4 => now;

playNotes(15);

