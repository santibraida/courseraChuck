// Assignment6_SmallBand
// score.ck
// Insert the title of your piece here

// start the instruments
Machine.add(me.dir() + "/drums.ck") => int drumID;
Machine.add(me.dir() + "/bass.ck") => int bassID;
Machine.add(me.dir() + "/guitar.ck") => int guitarID;
Machine.add(me.dir() + "/clarinet.ck") => int clarinetID;
Machine.add(me.dir() + "/elec.ck") => int elecID;

// wait for the instruments to complete
31::second => now;

// cleanup
Machine.remove(drumID);
Machine.remove(bassID);
Machine.remove(guitarID);
Machine.remove(clarinetID);
Machine.remove(elecID);
