// print name of assignment
<<< "Assignment_6_the_band" >>>;

// sound chain
SndBuf hihat => dac;
SndBuf kick => dac;
SndBuf snare => dac;

// note duration
0.625 => float quarterDuration;
quarterDuration/2 => float eighthDuration;
quarterDuration/4 => float sixteenthDuration;

// me.dirUp 
me.dir(-1) + "/audio/hihat_01.wav" => hihat.read;
me.dir(-1) + "/audio/kick_05.wav" => kick.read;
me.dir(-1) + "/audio/snare_03.wav" => snare.read;

// parameter setup
.5 => hihat.gain;

// loop 
while( true )  
{
    // play kick
    if (Math.random2(0,10) > 6) {
        0 => kick.pos;
        Math.random2f(0.2,.5) => kick.gain;
    } else {
        0 => kick.gain;
    }
    
    // play snare
    if (Math.random2(0,10) > 5) {
        0 => snare.pos;
        Math.random2f(0.2,.5) => snare.gain;
    } else {
        0 => snare.gain;
    }

    Math.random2f(.2, 1.2) => hihat.rate;
    Math.random2f(.9,1.2) => hihat.rate;
    (Math.random2(1,2) * sixteenthDuration) :: second => now;
    0 => hihat.pos;
}
