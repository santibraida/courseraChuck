// print name of assignment
<<< "Assignment_6_the_band" >>>;

// paths to chuck file
me.dir() + "/piano.ck" => string pianoPath;
me.dir() + "/bass.ck" => string bassPath;
me.dir() + "/drums.ck" => string drumsPath;

// note duration
0.625 => float quarterDuration;
quarterDuration/2 => float eighthDuration;
quarterDuration/4 => float sixteenthDuration;
(quarterDuration + 7 * eighthDuration) => float beatDuration;

// start piano
Machine.add(pianoPath) => int pianoID;
beatDuration::second => now;

// start drums
Machine.add(drumsPath) => int drumsID;
2*beatDuration::second => now;

// start bass
Machine.add(bassPath) => int bassID;
4*beatDuration::second => now;

// remove bass and drums
Machine.remove(bassID);
Machine.remove(drumsID);
beatDuration::second => now;

// add bass and drums back in 
Machine.add(bassPath) => bassID;
Machine.add(drumsPath) => drumsID;
3*beatDuration::second => now;

// remove piano
Machine.remove(pianoID);

// remove drums
beatDuration::second => now;
Machine.remove(drumsID);

// remove bass
2*beatDuration::second => now;
Machine.remove(bassID);