// print name of assignment
<<< "Assignment_6_the_band" >>>;

// sound chain
Rhodey piano[6];
piano[0] => dac.left;
piano[1] => dac; 
piano[2] => dac;
piano[3] => dac.right; 
piano[4] => dac.left;
piano[5] => dac.right;

// note duration
0.625 => float quarterDuration;
quarterDuration/2 => float eighthDuration;
quarterDuration/4 => float sixteenthDuration;

// chord 2D array
[[48,51,53,56],[46,49,53,56]] @=> int chordz[][];

// loop 
while( true )  
{
    // build first chord
    for( 0 => int i; i < 6; i++ )  
    {
        if (i < 4) {
            Std.mtof(chordz[0][i]) => piano[i].freq;
        } else {
            Std.mtof(chordz[0][Math.random2(0, chordz[0].cap()-1)]) => piano[i].freq;
        }
        Math.random2f(0.3,.7) => piano[i].noteOn;
    }
    1 => piano[4].noteOff => piano[5].noteOff;
    
    quarterDuration :: second => now;
    
    // play melody notes
    Math.random2f(0.0,.4) => piano[4].noteOn;
    sixteenthDuration :: second => now;
    1 => piano[4].noteOff;
    
    Math.random2f(0.0,.4) => piano[5].noteOn;
    sixteenthDuration :: second => now;
    
    // turn off melody notes
    1 => piano[4].noteOff => piano[5].noteOff;
    
    // build second chord
    for( 0 => int i; i < 6; i++ )  
    {
        if (i < 4) {
            Std.mtof(chordz[1][i]) => piano[i].freq;
        } else {
            Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[i].freq;
        }
        Math.random2f(0.3,.7) => piano[i].noteOn;
    }
    1 => piano[4].noteOff => piano[5].noteOff;
    quarterDuration :: second => now;
    
    // play melody notes
    Math.random2f(0.0,.3) => piano[4].noteOn;
    Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[4].freq;
    sixteenthDuration :: second => now;
    1 => piano[4].noteOff;
    
    Math.random2f(0.05,.4) => piano[5].noteOn;
    Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[5].freq;
    sixteenthDuration :: second => now;
    1 => piano[5].noteOff;
    
    Math.random2f(0.1,.5) => piano[4].noteOn;
    Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[4].freq;
    sixteenthDuration :: second => now;
    1 => piano[4].noteOff;
    
    Math.random2f(0.1,.4) => piano[5].noteOn;
    Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[5].freq;
    sixteenthDuration :: second => now;
    1 => piano[5].noteOff;
    
    Math.random2f(0.0,.4) => piano[4].noteOn;
    Std.mtof(chordz[1][Math.random2(0, chordz[1].cap()-1)]) => piano[4].freq;
    sixteenthDuration :: second => now;
    
    // turn off melody notes
    1 => piano[4].noteOff => piano[5].noteOff;
}
