// print name of assignment
<<< "Assignment_6_the_band" >>>;

// sound chain (mandolin for bass)
PulseOsc bass => NRev r => dac;

[46, 48, 49, 51, 53, 54, 56, 58] @=> int scale[]; 

// parameter setup
0.1 => r.mix;
0.1 => bass.gain;

// note duration
0.625 => float quarterDuration;
quarterDuration/2 => float eighthDuration;
quarterDuration/4 => float sixteenthDuration;

Std.mtof(scale[0]) => bass.freq;

// loop
while( true )  
{
    Math.random2f(0.01,0.5) => bass.width;
    Math.random2f(0.05, 0.25) => bass.gain;
    
    if( Math.random2(0,1) == 1)
    {
        Std.mtof(scale[Math.random2(0, scale.cap()-1)]) => bass.freq;
    }
    
    (Math.random2(1,2) * sixteenthDuration) :: second => now;
}


