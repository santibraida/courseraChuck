// score.ck
//
// Introduction to Programming for Musicians and Digital Artists 
// Assignment 6
//
// Author: me
// Date: 30 Nov 2013
//
// Title: Rainy Day Saturday
//
// Well it was a rainy Saturday so I wrote an abstract meloncholy 
//   piece to sound good with the drops on a steel roof.
//
//--------------------------------------------------------------

// global variables
625::ms => dur quarter; // 48 quarters in total for 30 seconds
int guitarID1, guitarID2; // these need to be global

// paths to files
me.dir() + "/jazzdrummer.ck" => string drumPath;
me.dir() + "/piano.ck" => string pianoPath;
me.dir() + "/violin.ck" => string violinPath;
me.dir() + "/guitar.ck" => string guitarPath;

// add voices 
Machine.add( violinPath ) => int violinID;
4::quarter => now;

Machine.add( pianoPath ) => int pianoID;
4::quarter => now;

Machine.add( drumPath ) => int drumID;
4::quarter => now;

// spork threads for guitar coming in and out
spork ~ Guitar();
spork ~ DoubleGuitar();

20::quarter => now;
Machine.remove( violinID );

8::quarter => now;
Machine.remove( pianoID );

// move remaining time in piece
8::quarter => now;

// clean up
Machine.remove( drumID );
Machine.remove( guitarID1);
Machine.remove( guitarID2 );

// adds a guitar 
fun void Guitar() 
{
    while (true)
    {       
        Math.random2(800, 2000)::ms => now;        
        Machine.add( guitarPath ) => guitarID1;
        Math.random2(600, 4000)::ms => now;
        Machine.remove( guitarID1 );
    }
}

// adds a second guitar after some time to create nice effect
fun void DoubleGuitar() 
{
    while (true)
    {
        // Start at an offset
        0.2::quarter + 4*2::quarter => now;
        Machine.add( guitarPath ) => guitarID2;
        Math.random2(4,6)::quarter => now;
        Machine.remove( guitarID2);
    }
}


