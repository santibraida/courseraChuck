// guitar.ck
//
// Introduction to Programming for Musicians and Digital Artists 
// Assignment 6
//
// Author: me
// Date: 30 Nov 2013
//
// Title: Rainy Day Saturday
//
// Well it was a rainy Saturday so I wrote an abstract meloncholy 
//   piece to sound good with the drops on a steel roof.
//
//--------------------------------------------------------------

// adds some improv jazz guitarish sound

// set up instrumetn and sound chain
StifKarp karp => ADSR karpADSR => Delay karpDelay => Pan2 karpPan => dac;
1 => karp.pickupPosition;
karpADSR.set( 500::ms, 10::ms, .3, 300::ms );
0.3 => karp.gain;
karpDelay => karpDelay;
150::ms => karpDelay.max;
150::ms => karpDelay.delay;
0.6 => karpDelay.gain;
-0.25 => karpPan.pan;

// Set up valid note to improvise
[46-12, 48-12, 49-12, 51-12, 53-12, 54-12, 56-12,
 46, 48, 49, 51, 53, 54, 56, 
 46+12, 48+12, 49+12, 51+12, 53+12, 54+12, 56+12,
 46+24, 48+24, 49+24, 51+24, 53+24, 54+24, 56+24,
 46+36 ] @=> int Scale[];

// variables
14 => int ScalePointer;

while (true)
{
    // load newly calculated frequency
    Math.mtof( Scale[ScalePointer] ) => karp.freq;
    1 => karp.noteOn;

    if(Math.randomf() > 0.1) // turn on 90% of the time
        karpADSR.keyOn();
    
    Math.random2f(10, 300)::ms => now; // advance by random time (jazz kind off)
    karpADSR.keyOff(); // turn off ADSR
    10::ms => now;
    
    // get next note
    Math.random2(-2,2) +=> ScalePointer;
    if ( ScalePointer >= Scale.cap())
        12 -=> ScalePointer;
    if ( ScalePointer < 0)
        12 +=> ScalePointer;

}