// piano.ck
//
// Introduction to Programming for Musicians and Digital Artists 
// Assignment 6
//
// Author: me
// Date: 30 Nov 2013
//
// Title: Rainy Day Saturday
//
// Well it was a rainy Saturday so I wrote an abstract meloncholy 
//   piece to sound good with the drops on a steel roof.
//
//--------------------------------------------------------------

// adds some piano triplets for flavour

// global variables
625::ms => dur quarter;

// set up sound chain
Wurley wurl => JCRev wurlRev => Pan2 wurlPan => dac;
0.2 => wurlRev.mix;
0.16 => wurlRev.gain;
0.55 => wurlPan.pan;

// valid notes
[46, 48, 49, 51, 53, 54] @=> int Scale[];
// melody line 
[0, 2, 4, 1, 3, 5,
 0, 2, 4, 1, 3, 5,
 4, 4, 3, 2, 2, 1,
 0, 2, 4, 1, 3, 5 ] @=> int melody[];

// variables
0 => int melPointer;

while (true)
{
    // load up next note
    Math.mtof( Scale[melody[melPointer]]) => wurl.freq;
    1 => wurl.noteOn;
    
    0.66666666666666::quarter => now; // triplet notes
    // increment pointer
    (melPointer + 1) % melody.cap() => melPointer;
}