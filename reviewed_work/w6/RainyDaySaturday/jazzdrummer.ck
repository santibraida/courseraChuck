// jazzdrummer.ck
//
// Introduction to Programming for Musicians and Digital Artists 
// Assignment 6
//
// Author: me
// Date: 30 Nov 2013
//
// Title: Rainy Day Saturday
//
// Well it was a rainy Saturday so I wrote an abstract meloncholy 
//   piece to sound good with the drops on a steel roof.
//
//--------------------------------------------------------------

// Global variables
625::ms => dur quarter;

// beat sound chain
SndBuf snare;
me.dir(-1) + "/audio/snare_01.wav" => snare.read;
snare.samples() => snare.pos;
0.4 => snare.gain;
snare => Pan2 snarePan => dac;
0.3 => snarePan.pan;

SndBuf hihat[4];
me.dir(-1) + "/audio/hihat_01.wav" => hihat[0].read;
me.dir(-1) + "/audio/hihat_02.wav" => hihat[1].read;
me.dir(-1) + "/audio/hihat_03.wav" => hihat[2].read;
me.dir(-1) + "/audio/hihat_03.wav" => hihat[3].read;
Pan2 hihatPan;
for (0 => int i; i < hihat.cap(); i++)
{
    hihat[i].samples() => hihat[i].pos;
    hihat[i] => hihatPan => dac;
}
-0.4 => hihatPan.pan;
0.1 => hihatPan.gain;


SndBuf bass => dac;
me.dir(-1) + "/audio/kick_03.wav" => bass.read;
0.7 => bass.rate;  // make a cool bass
bass.samples() => bass.pos;
0.3 => bass.gain;

// variables
0 => int counter;

while (true)
{   
    // sets up standard kind of jazzy swing beat
     
    if (counter % 24 == 0 || counter % 24 == 11 || counter % 24 == 12 )
    {
        0 => bass.pos;
    }
    
    
    if (counter % 6 == 0 || counter % 6 == 2 ) 
    {
        0 => hihat[Math.random2(0,1)].pos;    
            
    }
    
    if (counter % 6 == 3 ) 
    {
        0 => hihat[Math.random2(2,3)].pos;    
        
    }    

    if (counter % 48 > 45 ) 
    {
        0 => snare.pos;
    }
    
    // this creates swing in the beat
    if (counter % 3 == 0)
    {
        0.38::quarter => now;
    }
    else 
        0.31::quarter => now;   
    
    counter++;
}
