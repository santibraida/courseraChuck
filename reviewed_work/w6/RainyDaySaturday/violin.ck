// violin.ck
//
// Introduction to Programming for Musicians and Digital Artists 
// Assignment 6
//
// Author: me
// Date: 30 Nov 2013
//
// Title: Rainy Day Saturday
//
// Well it was a rainy Saturday so I wrote an abstract meloncholy 
//   piece to sound good with the drops on a steel roof.
//
//--------------------------------------------------------------

// Makes a moody violin backdrop for the piece

// Global variables
625::ms => dur quarter;

// valid notes to play
[ 46+24, 48+24, 49+24, 51+24, 53+24, 54+24, 56+24, 
  46+36, 48+36, 49+36, 51+36, 53+36, 54+36, 56+36 ] @=> int Scale[];

// create instruments
Bowed violins[4];
JCRev violinRev => Pan2 violinPan => dac;
0.1 => violinRev.mix;
0.8 => violinRev.gain;
for (0 => int i; i < violins.cap(); i++)
{
    violins[i] => violinRev;
    0.8 * Math.pow(0.8, i) => violins[i].volume;
}

//
// This is used to creat a weird sweep panning
//
fun void modulateViols()
{
    // make some really trippy stuff here
    Phasor p => blackhole;
    
    // define LFO sweep frequency in sweeps per quarter note
    1/(4*0.625)/2 => p.freq;
    
    while (true)
    {
            p.last()*2 -1 => violinPan.pan;
            0.01::quarter => now;
    }

}

// variables
0 => int root;
float bowTime;

// start the  L->R modulation
spork ~ modulateViols();

while ( true )
{
    // put each note in the instrument and change its sound randomly
    for (0 => int i; i < violins.cap(); i++)
    {
        Std.mtof(Scale[root + 2*i]) => violins[i].freq;
        
        Math.random2f( 0.4, 0.5 ) => violins[i].bowPressure;
        Math.random2f( .4, .7 ) => violins[i].bowPosition;
        Math.random2f( 0.5, 4 ) => violins[i].vibratoFreq;
        Math.random2f( .001, .01 ) => violins[i].vibratoGain;
/*
// just for diagnostics                
            <<< "---", i, " ---" >>>;
            <<< "bow pressure:", violins[i].bowPressure() >>>;
            <<< "bow position:", violins[i].bowPosition() >>>;
            <<< "vibrato freq:", violins[i].vibratoFreq() >>>;
            <<< "vibrato gain:", violins[i].vibratoGain() >>>;
            <<< "volume:", violins[i].volume() >>>;
            <<< "freq:", violins[i].freq() >>>;
*/
    }
    
    // play 16th notes for a 1 quarter lengths
    repeat ( 4 )
    {
        for (0 => int i; i < violins.cap(); i++)
        {
            Math.random2f( .7, .8 ) => violins[i].noteOn;
        }
        // play sound for a random part of the 16th note.
        Math.random2f(0.3, 0.8) => bowTime;
        bowTime * 0.25::quarter => now;
        for (0 => int i; i < violins.cap(); i++)
        {
            0 => violins[i].noteOff;
        }
        // play the rest of the 16 note out in silence
        0.25 * quarter - (now % (0.25*quarter)) => now;
    }

    // increment root note counter
    (root+2) % 8 => root;
}
