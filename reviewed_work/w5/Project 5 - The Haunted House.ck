// Project 5 -  The Haunted House
<<< "Project 5 -  The Haunted House" >>>;

// global variables
// Db Phrygian mode scale
[49, 50, 52, 54, 56, 57, 59, 61] @=> int scale[];
// same scale an octave up
[61, 62, 64, 66, 68, 69, 71, 73] @=> int scaleOctave[];
// set quarter note duration
.75::second => dur quarter;

// sound chains
NRev rev => Gain master => dac; // chuck reverb to master gain to dac

0 => master.gain;  // set master gain

// set up sound buffer
SndBuf boom => rev;
// read sound file
me.dir() + "/audio/kick_01.wav" => string path;
path => boom.read;
//set playhead to end of file so no sound
boom.samples() => boom.pos;
.5 => boom.gain; // set sound buffer gain


// set up oscillator
SinOsc vib => SinOsc ther => Pan2 p;
p.left => NRev revL => dac.left;   // allow proper stereo panning
p.right => NRev revR => dac.right;

Std.mtof(49) => ther.freq; // change pitch
0 => ther.gain;    // silence theramin until needed
6.0 => vib.freq;    // vibrato frequency
2 => ther.sync;     // chuck mode 2 to sync so vib modulates viol (set sync mode to FM)
3 => vib.gain; // change vibrato amount

// STK setup
VoicForm voices[3];
for( 0=> int i; i < voices.cap(); i++ )
{
    voices[i] => rev;
    1 => voices[i].gain;
    "ooo" => voices[i].phoneme;
}

// theramin note change function
fun void theraminPlay(int current, int next)
{
    // convert MIDI notes to frequencies
    Std.mtof(current) => float c;
    Std.mtof(next) => float n;    
    // change to next note same, up or down
    if( c < n ) // glissando up to next note
    {       
        for( c; c <= n; c + 1 => c )
        {
            c => ther.freq;
            4::ms => now;
        }
    }
    else if( c > n) // glissando down to next note
    {
        for( c; c >= n; c - 1 => c )
        {
            c => ther.freq;
            4::ms => now;
        }
    }
    else    // same note
    {
        // don't change note
    }
}

// STK chord function
fun void stkchord(int root, string quality, float length)
{
    // make major or minor chords
    // takes in midi notes and converts to frequencies
    // root    
    Std.mtof(scale[root]) => voices[0].freq;
    1 => voices[0].noteOn;
    
    // third
    if( quality == "major" )
    {
        Std.mtof( scale[root] + 4 ) => voices[1].freq;
        1 => voices[1].noteOn;
    }
    else if( quality == "minor" )
    {
        Std.mtof( scale[root] + 3 ) => voices[1].freq;
        1 => voices[1].noteOn;
    }
    else
    {
        <<< "Must specify 'major' or 'minor'" >>>;
    }
    // fifth
    Std.mtof( scale[root] + 7 ) => voices[2].freq;
    1 => voices[2].noteOn;
    // play for input length of time
    length::ms => now;
}

// MAIN PROGRAM

0 => int count; // beat count
61 => int current; // Hold note playing
61 => int next; // Hold next note

// loop for main body
while (count < 17)
{    
    if( count > 1 ) // bring in oscillator on third count
    {
        if( count == 2 )
        {
            for( 0 => float i; i <= .15; i+.001 => i ) // fade in oscillator
            {
                i => ther.gain;
                .01::ms => now;
            }
        }
        scaleOctave[Math.random2( 0, (scaleOctave.cap()-1) )] => next; // random melody note
        Math.random2f(-.7, .7) => p.pan; // randomly set oscillator pan
        theraminPlay( current, next ); // theraminPLay function moves to next note
        next => current; // next note becomes current note
    }
    if( count % 2 == 0 ) //restrict chords and drum to whole notes
    {        
        0 => boom.pos; // set drum file to start and for play
        .1 => boom.rate; // slow drum rate
        // choose random chord root
        Math.random2(0, scale.cap()-1) => int root;
        // call chord function for voices
        stkchord( root, "minor", 0 );
    }
    
    // fade in master gain at beginning
    if( count == 0)
    {
        for( 0 => float i; i <= .7; i+.01 => i )
        {
            i => master.gain;            
            20::ms => now;
        }        
    }
    
    // fade out at end
    if( count > 11 )
    {        
        master.gain() => float holder;
        for( holder => float i; i >= (holder - .15); i -.01 => i)
        {            
            i => master.gain;
            if( ther.gain() >= 0 ) // fade master
            {                
                ther.gain() - .002 => ther.gain; // fade oscillator
            }
        }
    }    
    
    // advance beat
    count++;
    quarter * 2 => now; // play half notes
}