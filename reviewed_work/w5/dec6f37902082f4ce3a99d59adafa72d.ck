// Assignment_5_Sounds_like_HavaNagila
<<< "Assignment_5_Sounds_like_HavaNagila" >>>;
<<< "Listen with headphones to get a better pan & bass impression" >>>;


// Assignment specifications:
//Random number: see lines #244-245
//Panning: see lines #21, 23, 26, 28
//Timing: see lines #55-60
//functions: see lines #223-248


// stores in memory the time at which the program starts
now => time program_start;

// Mixer Section
Gain master => dac;
0.8 => master.gain;

// Oscillators section
SinOsc bass1 => ADSR env1 => Pan2 p_bass1 => dac;
(0.05::second, 0.1::second, 0.1, 2::second) => env1.set;
-1 => p_bass1.pan;
0.12 => bass1.gain;

SinOsc bass2 => ADSR env2 => Pan2 p_bass2 => dac;
(0.05::second, 0.1::second, 0.1, 2::second) => env2.set;
1 => p_bass2.pan;
0.12 => bass2.gain;

// Collect sample files
me.dir() + "/audio/kick_03.wav" => string kick_sample;

//Sample settings
SndBuf kick => Gain v_kick => master;
0.5 => v_kick.gain;
kick_sample => kick.read;
kick.samples() => kick.pos;

//STK instrument settings
//Tambourine
Shakers tamb => Gain v_tamb => master;
0.5 => v_tamb.gain;
6 => tamb.preset; //select tambourine preset
1.0 => tamb.decay;
64 => tamb.objects;

//Mandolin
Mandolin lead => Gain v_lead => master;
0.2 => v_lead.gain;
0.4 => lead.pluckPos;
0.02 => lead.stringDetune;
0.3 => lead.bodySize;

// Time frames construction
0.75::second => dur quarter; // as required
    //Derived values
    0.5::quarter => dur eight;
    0.25::quarter => dur sixteenth;
// In 2/4, this would make the composition length = 20 bars at 80 bpm

// Basic Scale construction, Db Phrygian mode
[49, 50, 52, 54, 56, 57, 59, 61] @=> int midi_notes[];

//song construction
//2x theme (8 bars), once with bass chord, once with alternating bass
//conclusion

// Chord progression
//theme
[1,1,1,3,2,1,3,3,3,5,4,3,4,4,4,6,5,4,3,2,1] @=> int mel_note1[]; //index indicates note in scale array
[2,2,1,1,1,1,2,2,1,1,1,1,2,2,1,1,1,1,2,2,4] @=> int mel_note_dur1[]; //Duration of note in eights

//conclusion
[3,3,2,1,1,1,2,2,1,7,7,7] @=> int mel_note2[]; //index indicates note in scale array
[1,2,1,1,1,2,1,2,1,1,1,2] @=> int mel_note_dur2[]; //Duration of note in eights

//bass notes for theme (first repetition)
[1,1,1,1] @=> int bass1_note1[];
[5,5,6,5] @=> int bass2_note1[];
[8,8,8,8] @=> int bass_note_dur1[];

//bass notes for theme (second repetition)
[1,1,1,1,2,2,1,1] @=> int bass1_note2[];
[5,5,5,5,6,6,5,5] @=> int bass2_note2[];

//bass notes for conlusion
[8,7] @=> int bass1_note3[];

//see embedded function at the end of the script
ScaleToArray(mel_note1);
ScaleToArray(mel_note2);
ScaleToArray(bass1_note1);
ScaleToArray(bass2_note1);
ScaleToArray(bass1_note2);
ScaleToArray(bass2_note2);
ScaleToArray(bass1_note3);

//MAIN PROGRAM

//initialize the variables
0 => float atime;
0 => int btime;
0 => int btime_a;
0 => int btime_b;
0 => int ctime;
0 => int notenum1;
0 => int notenum2;
1 => int count1;
1 => int count2;  

//theme
//while loop will repeat music theme 2 times (indicated by count1) : each iteration of the loop is a melody note
while(count1<=2)
{
    //sets the melody note, advancing in notes array with index notenum1
    Std.mtof(midi_notes[mel_note1[notenum1]]+12) => lead.freq;
    1 => lead.noteOn;
        
    //for loop calculates how many eight notes beats must pass before playing next melody note and plays bass notes accordingly
    for(1=>int index; index<=mel_note_dur1[notenum1]; index++)
    {
        //Checks if a percussion needs to be played on actual beat
        PercussionScore(1,atime);
        
        //time index in eights       
        atime+1 => atime;
        <<<atime>>>;
        
        //bass part on first repetition, only on beginning of bars
        if(count1==1 && (atime-1-8*btime)%bass_note_dur1[btime]==0)
        {          
            Std.mtof(midi_notes[bass1_note1[btime]]) => bass1.freq;
            Std.mtof(midi_notes[bass2_note1[btime]]) => bass2.freq;
            1 => env1.keyOn;
            1 => env2.keyOn;
            btime+1 => btime;
            //resets the array for next repetition
            if(btime==bass1_note1.cap())
            {
                0 => btime;
            }
            
        }
        //bass part on second repetition
        if(count1==2)
        {
            //change bass release time to "staccato"
            0.1::second => env1.releaseTime;
            0.1::second => env2.releaseTime;
            if((atime/2)%2.0==0.5)
            {
                Std.mtof(midi_notes[bass1_note2[btime_a]]) => bass1.freq;
                1 => env1.keyOn;
                btime_a+1 => btime_a;
            }
            if((atime/2)%2.0==1.5)
            {
                Std.mtof(midi_notes[bass2_note2[btime_b]]-12) => bass2.freq;
                1 => env2.keyOn;
                btime_b+1 => btime_b;
            } 
        }
    
        //advance time and release bass note if it was played
        1::eight => now;
        1 => env1.keyOff;
        1 => env2.keyOff;
    
    }
    //goes to the next melody note for next while loop iteration
    notenum1+1 => notenum1;
    //increment count1 when 1st repetition is done(arrived at the end of theme notes array
    if(notenum1>=mel_note_dur1.cap())
    {
        0 => notenum1;
        count1+1 => count1;
    }
}

//conclusion (based on same structure of the theme section)
while(count2<=1)
{    
    //sets the melody note, advancing in notes array with index notenum2
    Std.mtof(midi_notes[mel_note2[notenum2]]+12) => lead.freq;
    1 => lead.noteOn;
    
    //bass part, only on beginning of bars
    1::second => env1.releaseTime;
    for(1=>int index; index<=mel_note_dur2[notenum2]; index++)
    {
        if((atime/2)%4.0==0)
        {
            Std.mtof(midi_notes[bass1_note3[ctime]]-12) => bass1.freq;
            1 => env1.keyOn;
            ctime+1 => ctime;
        }
        
        //Checks if a percussion needs to be played on actual beat    
        PercussionScore(1,atime);
        
        //advance time and release bass note if it was played
        atime+1 => atime;
        <<<atime>>>;
        1::eight => now;
        1 => env1.keyOff;
    }
    
    //goes to the next melody note for next while loop iteration
    notenum2+1 => notenum2;
    //condition to exit the loop (all conclusion notes were played)
    if(notenum2>=mel_note_dur2.cap())
    {
        0 => notenum1;
        count2+1 => count2;
    }
}

// prints the real running time, independently of the running time shown in the Virtual Machine
<<< "Running time is effectively:", (now-program_start)/second, "seconds." >>>;


//Functions
fun void ScaleToArray(int notevector[])
{
    //function transforms a note index in a scale array(begins at 1) to an array index (begins at 0)
    //independent of array length
    for(0 => int i;i<notevector.cap();i++)
    {
        notevector[i]-1 => notevector[i];
    }
}

fun void PercussionScore(int condition, float timevar)
{
    //function plays kick drum on beat and tambourine on counterbeat
    if(timevar%2==0)
    {
        0 => kick.pos;
    }
    if(timevar%2==1)
    {
        //energy in tambourine is random each time
        Math.random2(64,128) => tamb.objects;
        Math.random2f(.5,.65) => v_tamb.gain;
        1 => tamb.noteOn;
    }
}