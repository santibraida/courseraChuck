// Assignment_4_Half_Way

/*

Again, I have not had as much time as i was hoping to this week.
I have taken last week's attempt, which was mainly made up of random
percussive functions, and played around with the oscillators and instruments
used.

Comments from last week:

This is another attempt at a semi-random percussion generator. It
works by giving the probability of a note playing at any given
16th interval. You can see these probabilities defined for the
kick, snare, hihat and clap just after the sound files are loaded.

There is a boom-y kick drum and rev cymbal for accentuating
every 16 bars.

The chords and the bass drone are the only thing in this that
have no random elements to them.

The first 16 bars are a kind of intro section. After 16 bars,
a lead synth comes in, playing, again, semi-random notes
(every other note is an octave of the root note of the scale)

Everything after 32 seconds is looped (although, depending
on your frame of reference, the even finish point could be
at ~48 seconds)

Enjoy.
*/

// set quarter notes to 0.6 seconds, then set the step to
// a quarter of that that to allow for a little syncopation
.75::second => dur quarter;
0.25::quarter => dur step;

// define the Db Phrygian Scale (1 octave below [50, 52, 53, 55, 57, 59, 60])
[49 - 12, 50 - 12, 52 - 12, 54 - 12, 56 - 12, 57 - 12, 59 - 12] @=> int notes[];


/*                  _   _                 
/ _|_   _ _ __   ___| |_(_) ___  _ __  ___ 
| |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
|  _| |_| | | | | (__| |_| | (_) | | | \__ \
|_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|__*/                                            


/*
* ==========================
* | resolveMidiToFrequency |
* ==========================
*
* the resolveMidiToFrequency function makes it easier
* to generate many octaves of notes out of the notes[]
* array defined at the top of the project.
*
* an index of 0-6 will produce notes 1-7 in the dorian scale
* (as defined in `notes[]`).
*
* indices of 7-13 will produce the same scale, but one
* octave higher. 14-20 one octave above that, and so on.
**/

fun int resolveMidiOctave( int index, int octave )
{
    return notes[ index % notes.cap() ] + octave * 12;
}

fun int resolveMidi( int index )
{
    return resolveMidiOctave( index, index / notes.cap() );
}

fun float resolveMidiToFrequency( int index )
{
    return Std.mtof(resolveMidi(index));
}

/*
* ================================
* | playBufferBasedOnProbability |
* ================================
*
* This is the function that handles the probability based percussion.
* It is passed a buffer, a probability, and a range of rates to play
* back at.
*
*/

fun void playBufferBasedOnProbability( SndBuf buffer, float probability, float rateRange[] )
{
    // only play the given buffer if the probability pans out
    if( probability > Math.random2f(0, 1) )
    {
        // generate and set the rate
        Math.random2f(rateRange[0], rateRange[1]) => float rate;
        buffer.rate( rate );
        
        // if the rate is positive, set the playhead to the start, else set it to the end of the sample
        if (rate >= 0)
        {
            buffer.pos( 0 );
        }
        else
        {
            buffer.pos( buffer.samples() );
        }
    }
    
}


/*
* ======================
* | playRandomLeadNote |
* ======================
*
* These functions are to play random lead notes and octaves of the
* scale root alternitavely.
*
*/

fun void playOctaveLeadNote( Saxofony osc )
{
    // set the lead to a root octave
    7 * Math.random2(1,2) => int note;
    resolveMidiToFrequency( note ) => osc.freq;
}

fun void playNote(Saxofony osc, int index)
{
    // set the lead to a random scaled note
    resolveMidiToFrequency( index ) => osc.freq;
    
}

fun void playRandomNote( Saxofony osc, int range[] )
{
    // set the lead to a random scaled note
    Math.random2(range[0], range[1]) => int octaveIndex;
    resolveMidiToFrequency( octaveIndex ) => osc.freq;
}

fun void fluctuatePan( Pan2 pan )
{
    Math.random2f(-0.55, 0.55) => pan.pan;
}

fun void playRandomLeadNote( Saxofony osc, Pan2 pan, int position )
{
    // every quarter beat
    if ( position % 4 == 0 )
    {
        playOctaveLeadNote( osc );
    }
    else if ( position % 4 == 2)
    {
        playRandomNote( osc, [0,18] );
    }
    // subtly randomise the pan and gain
    if (position % 2 == 0)
    {
        fluctuatePan( pan );
        Math.random2f(0.045, 0.075) => osc.gain;
    }
}

/*
* =============
* | playChord |
* =============
*
* a helper function for chords
*
*/

fun void playChord( Moog oscs[], int chord[] )
{
    for( 0 => int xi; xi < oscs.cap(); xi++  )
    {
        // apply each note of the chord to the oscillators
        chord[xi] => int note;
        resolveMidiToFrequency( note ) => oscs[xi].freq;
        0.485 => oscs[xi].gain;
        0.04 => oscs[xi].filterSweepRate;
        // 2 => oscs[xi].filterQ;
        
        1 => oscs[xi].noteOn;
    }
}

fun void stopChord( Moog oscs[] )
{
    for( 0 => int xi; xi < oscs.cap(); xi++  )
    {
        1 => oscs[xi].noteOff;
    }
}

/*               
___  ___| |_ _   _ _ __  
/ __|/ _ \ __| | | | '_ \ 
\__ \  __/ |_| |_| | |_) |
|___/\___|\__|\__,_| .__/ 
|*/

/*
* ====================
* | Percussion Setup |
* ====================
*/

// first, setup the percussion files
["kick_02", "hihat_02", "snare_01", "clap_01", "clap_01", "kick_05", "hihat_04"] @=> string percussionFiles[];

/*
* the sound buffer objects array
*/
SndBuf percussion[ percussionFiles.cap() ];
/*
* the panning objects array
*/
Pan2 percussionPan[ percussionFiles.cap() ];
/*
* a 2d array to hold the rhythm for each buffer
*
* each of the 16 elements holds the probability of the given buffer playing
* at its position in the sequencer. for example, the kick drum's rhythm
* array starts [1.0, 0.0, ...], so it will always play on the first beat
* of every bar.
*
* Note that these are initialised as all zeroes, so that nothing will play
* by default
*/
float percussionRhythm[ percussionFiles.cap() ][ 16 ];
/*
* a 2d array to hold the rate variance for each buffer
*
* whenever the buffer plays, it will play at a random rate between the
* two given in this array
*/
float percussionRate[ percussionFiles.cap() ][ 2 ];

// initialiase the buffers
for( 0 => int xi; xi < percussionFiles.cap(); xi++ )
{
    // load up the sound file
    percussion[xi].read( me.dir() + "/audio/" + percussionFiles[xi] + ".wav" );
    // link up dac and pan
    // nb. i'm passing these straight to dac as i can't get a stereo output
    // out of the Gain object (as talked about in forum thread #684).
    // i'm valuing stereo placement over easy gain management.
    percussion[xi] => percussionPan[xi] => dac;
    // set the buffer playhead to the end (i.e. don't play at start)
    percussion[xi].samples() => percussion[xi].pos;
    
    // set the default percussion rates to 1
    [1.0, 1.0] @=> percussionRate[xi];
}

// the kick drum
0.4 => percussion[0].gain;
[
1.0, 0, 0, 0, 0.2, 0, 0.5, 0, 0.7, 0, 0, 0, 0, 0, 0, 0
] @=> percussionRhythm[0];
0.2 => percussionPan[0].pan;

// hi hat - will probably play every bar
0.175 => percussion[1].gain;
[
0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85
] @=> percussionRhythm[1];
-0.5 => percussionPan[1].pan;
[0.4, 2.0] @=> percussionRate[1];

// snare
0.3 => percussion[2].gain;
[
0.0, 0, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0.5, 0, 0, 0
] @=> percussionRhythm[2];
-0.3 => percussionPan[0].pan;

// clap right
0.6 => percussion[3].gain;
[
0.0, 0, 0, 0.2, 0.7, 0, 0.3, 0.4, 0, 0, 0.2, 0, 0.5, 0.4, 0, 0
] @=> percussionRhythm[3];
0.55 => percussionPan[3].pan;
[1.3, 2.0] @=> percussionRate[3];

// clap left
0.6 => percussion[4].gain;
[
0.0, 0, 0, 0.3, 0.7, 0, 0.2, 0.2, 0, 0, 0.4, 0, 0.5, 0.1, 0.1, 0.1
] @=> percussionRhythm[4];
-0.55 => percussionPan[4].pan;
[1.3, 2.0] @=> percussionRate[4];

// subtle booooom
0.2 => percussion[5].gain;

// not so subtle reverse cymbol
0.3 => percussion[6].gain;

/*
* ====================
* | Oscillator Setup |
* ====================
*/

// a droning bass that sticks to the root D note
Bowed bass => dac;
0.098 => bass.gain;
1 => bass.noteOn;
resolveMidiToFrequency(6) => bass.freq;

// a lead synth. this will play semi-random notes
Saxofony lead => Pan2 leadPan => dac;
0 => lead.gain;

// a pad array - this will play chords
Moog pad[3];
Pan2 padPan[3];

// initialise the pads
for ( 0 => int xi; xi < 3; xi++)
{
    // same as with the percussion
    pad[ xi ] => padPan[ xi ] => dac;
    // initially mute
    0 => pad[ xi ].gain;
    // set pan, left to right
    -0.5 + ( xi * 0.5 ) => padPan[xi].pan;
}

// some chords
[
[1, 3, 5],
[1, 4, 5],
[2, 4, 7],
[2, 5, 7], // not very nice
[1, 4, 6],
[3, 5, 7],
[4, 6, 7],
[5, 6, 8],
[5, 8, 10]
] @=> int chords[][];

// this is the order the chords will play in
[ 7, 8, 2, 5, 1, 0, 2, 3 ] @=> int chordSequence[];
[ 7, 5 ] @=> int endingChordSequence[];

// this is a counter to track the chords
0 => int chordSequenceCounter;


/*  _                  _               _                   
| |_| |__   ___   _ __ | | __ _ _   _  | | ___   ___  _ __  
| __| '_ \ / _ \ | '_ \| |/ _` | | | | | |/ _ \ / _ \| '_ \ 
| |_| | | |  __/ | |_) | | (_| | |_| | | | (_) | (_) | |_) |
\__|_| |_|\___| | .__/|_|\__,_|\__, | |_|\___/ \___/| .__/ 
|_|            |___/                |*/

// initialise the sequencer count
0 => int position;

<<< "Intro" >>>;
while ( position < 127 )
{
    /*
    * =====================
    * | Percussion Output |
    * =====================
    */
    for( 0 => int xi; xi < percussionFiles.cap(); xi++ )
    {
        playBufferBasedOnProbability( percussion[ xi ], percussionRhythm[ xi ][ position % 16 ], percussionRate[ xi ] );
    }
    
    // boom every 4 bars
    if( position % 64 == 0 )
    {
        percussion[5].pos(0);
    }
    // rev cymbol into boom
    if( position % 64 == 50 )
    {
        percussion[6].pos( percussion[6].samples() );
        percussion[6].rate( -0.63 );
    }
    
    /*
    * ============
    * | THE LEAD |
    * ============
    *
    * The lead synth only starts playing after 16 bars. It alternates
    * between a random octave of the root note, and a random note
    * in the dorian scale
    */
    
    // when 4 bars in
    if( position > 63) {
        1 => lead.stopBlowing;
        1 => lead.noteOn;
        playRandomLeadNote( lead, leadPan, position );
        
    }
    
    /*
    * ===========
    * | THE PAD |
    * ===========
    *
    * The pad changes every 2 bars, looping through the
    * chordSequence array.
    */
    
    // if two bars have passed
    if ( position % 32 == 0 )
    {
        // loop over the chordSequence array
        chords[ chordSequence[ chordSequenceCounter % chordSequence.cap() ] ] @=> int chord[];
        playChord( pad, chord );
        
        chordSequenceCounter++;
    }
    // if two bars are about to have passed
    if ( position % 32 == 28 )
    {
        stopChord( pad );
    }
    
    position++; step => now; // advance time!
}

0 => chordSequenceCounter;
0.75 => percussion[5].gain;
0.4 => percussion[5].rate;

<<< "Second Section" >>>;
while( position < 160 )
{
    /*
    * =====================
    * | Percussion Output |
    * =====================
    */
    for( 0 => int xi; xi < percussionFiles.cap(); xi++ )
    {
        // playBufferBasedOnProbability( percussion[ xi ], percussionRhythm[ xi ][ position % 16 ], percussionRate[ xi ] );
    }
    
    // boom every 2 bars with a syncopation of the second beat
    if( position % 32 == 0 || position % 32 == 26 )
    {
        percussion[5].pos(0);
        percussion[0].pos(0);
    }
    // rev cymbol into boom
    if( position % 64 == 50 )
    {
        percussion[6].pos( percussion[6].samples() );
        percussion[6].rate( -0.78 );
    }
    
    // if four bars have passed
    if ( position % 32 == 0 )
    {
        // loop over the chordSequence array
        chords[ endingChordSequence[ chordSequenceCounter % chordSequence.cap() ] ] @=> int chord[];
        playChord( pad, chord );
        
        chordSequenceCounter++;
    }
    
    if (position % 2 == 0 )
    {
        1 => lead.noteOn;
    }
    else
    {
        1 => lead.stopBlowing;
    }
    playRandomLeadNote( lead, leadPan, position );
    
    position++; step => now; // advance the time!
}

<<< "End Boom" >>>;

// silence oscillators
0 => bass.gain;
0 => lead.gain;
0 => pad[0].gain;
0 => pad[1].gain;
0 => pad[2].gain;

// one final boom
percussion[5].pos(0);
percussion[0].pos(0);

// play until a few samples before the end of this sample,
// otherwise a nasty click happens at the end
percussion[5].samples() - 15 => int remainingSamples;

now + remainingSamples::samp => now;
