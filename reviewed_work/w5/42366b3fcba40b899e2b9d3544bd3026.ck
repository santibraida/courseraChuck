<<<"Assignment 04 My g-g-generators" >>>;

// Defining the scale
[49, 50, 52, 54, 56, 57, 59, 61] @=> int SCALE[];

// Array of sounds
["/audio/kick_03.wav","/audio/hihat_01.wav","/audio/clap_01.wav","/audio/snare_03.wav","/audio/cowbell_01.wav"]@=> string sounds[];

// Sound Network
Moog moog => Pan2 moog_pan => Gain master => dac;
Rhodey rhodes => Pan2 rhodes_pan => master;
Saxofony saxo =>  NRev sax_rev => master;
SinOsc sin => master;
 
SndBuf drums[4];
NRev drum_rev => master;
for(0=>int i;i<drums.cap();i++){
    (me.dir() + sounds[i])=>drums[i].read;
    drums[i].samples()=>drums[i].pos;
    drums[i]=>drum_rev;
}

0.03 => drum_rev.mix;
0.05 => sax_rev.mix;
0.5 => sin.gain;

// Drum patterns
[[[ 1,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,1,0],
[ 1,0,1,0, 1,0,1,1, 1,0,1,0, 1,0,1,1],
[ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0],
[ 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]],

[[ 1,0,0,0, 0,0,0,0, 1,0,0,0, 0,0,1,0],
[ 1,0,1,0, 1,0,1,1, 1,0,1,0, 1,0,1,1],
[ 0,0,0,1, 0,0,0,0, 0,1,0,1, 0,0,0,1],
[ 0,0,1,0, 0,0,1,0, 0,0,0,0, 0,1,1,0]]] @=> int drumlines[][][];


// Time definitions
0.75::second => dur quarter;
0.25*quarter => dur sixteenth;
now => time start_time;
0 => int beat;

// Function to play a specific beat
fun void playBeat(int b){
    0 => int dline;
    if(b>=48){
        1 => dline;
    }
    Math.random2f(0.15,0.5) => drums[1].gain;
    for(0=>int i;i<drums.cap();i++){
        
        if(drumlines[dline][i][b%16]){
            0=>drums[i].pos;
            
        }
    }
}

//Main loop (30 seconds)
while(now < start_time + 30::second){
    
    1 => int step_beat_nb;

    if(now-start_time-(30::second)<quarter){
        Math.random2(1,2) => int step_beat_nb;
    }
    
    step_beat_nb*quarter => dur step_dur;

    Math.random2(0,SCALE.cap()-1) => int n;

    // Setting pitch
    Std.mtof(SCALE[n]-12) => sin.freq;
    Std.mtof(SCALE[n]) => moog.freq;
    Std.mtof(SCALE[(n+5)%SCALE.cap()]) => rhodes.freq;

//Modulating parameter
    1./0.75*Math.random2(1,4) => moog.vibratoFreq;
    Math.random2f(0,1) => moog.filterQ;
    Math.random2f(0,1) => moog.filterSweepRate;
    Math.random2f(0,0.05) => moog.vibratoGain;
    
    // Setting pan
    Math.random2f(-0.5,0.5) => float p;
    p => moog_pan.pan;
    -p => rhodes_pan.pan; 

// Note on
    1 => rhodes.noteOn;
    Math.random2f(0.5,1) => moog.noteOn;
    
    // Play corresponding beat
    for(0=>int k; k<4*step_beat_nb;k++){
         playBeat(beat);
         if(beat>=96){
             Std.mtof(SCALE[Math.random2(0,SCALE.cap()-1)]+12)=>saxo.freq;
             Math.random2f(0,0.4)=>saxo.noteOn;
         }
         sixteenth => now;
         1 +=> beat;
         if(beat>140){
             0.75*master.gain() =>master.gain;
         }
    }
}