// Assignment_3_technovibe33

// sound chain
Gain master => dac;
SndBuf kick => master;
SndBuf hat => Pan2 p => master;
SndBuf snare => master;
SndBuf wash => master;
SinOsc sin => Pan2 q => master;

//  Allowed tones
[50, 52, 53, 55, 57, 59, 60, 62] @=> int tones[];

["/audio/kick_04.wav","/audio/hihat_01.wav", "/audio/snare_02.wav", "/audio/stereo_fx_01.wav"]@=> string names[];
// read wav files into the sound buffers
me.dir()+names[0]=>kick.read;
me.dir()+names[1]=>hat.read;
me.dir()+names[2]=>snare.read;
me.dir()+names[3]=>wash.read;

// prevent automatic play
kick.samples()=>kick.pos;
hat.samples()=>hat.pos;
snare.samples()=>snare.pos;
wash.samples()=>wash.pos;

// initial sound levels
.3=> master.gain;
.5 => kick.gain;
.3=>hat.gain;
.3=>snare.gain;
.0 => sin.gain;

// initialise variable to track duration of assignment
now + 30::second => time assignment_length;

// we are looping on the quarter 
0=> int counter;

// reverse audio lead in
for (0=> int i; i<4; i++) {
-1 => hat.rate;
hat.samples()-1 => hat.pos;
.5::second=>now;
master.gain()+.2=> master.gain;
}

// hold to recover beat
.38::second=>now;
.05=>sin.gain;

// lead in with a wash
0=> wash.pos;

// main loop of song
while(now <= assignment_length){
    
    // 8 beats per measure actually counts sixteenth notes
    counter % 16 => int beat; // 1-e-&-a, etc.
    
    Math.random2(0,7)=>int note;
    Std.mtof(tones[note])*3 => sin.freq;
    Math.sin(now/1::second*2*pi)*.05=> sin.gain;

    // kick plays on quarters 2 and 4
    if (beat == 1 || beat == 7 || beat == 9 || Math.random2f(0, 1)>.9) {
        0=>kick.pos;
    }
    
    // sname plays on downbeat and 
    if (beat == 0 || beat == 8) {
        Math.random2f(.7, 1.2)=>snare.rate;
        0=>snare.pos;
    }
    
    // hihat plays in tripping style
    if (beat % 2 == 0 || Math.random2f(0, 1)>.8) {
        Math.random2f(.8, 1.2)=>hat.rate;
        Math.random2f(-1, 1)=>p.pan;
        0=>hat.pos;
    }
    
    counter++;
    .25::second => now;
    
    // suck back wash if at end of play head
    // once reverse to beginning, restart forward
    if (wash.pos() >= wash.samples()) {
        wash.samples()-1 => wash.pos;
        -2=>wash.rate;
    } else if (wash.pos()<=0) {
        0=>wash.pos;
        1 => wash.rate;
    }
}

// reverse audio outro
for (0=> int i; i<4; i++) {
-1 => hat.rate;
hat.samples()-1 => hat.pos;
.5::second=>now;
master.gain()*.5=> master.gain;
}
