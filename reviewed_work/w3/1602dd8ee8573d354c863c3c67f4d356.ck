<<< "Assignment Three Techno Music: bluepill5" >>>;

// Assign duration of melody
30::second + now => time END;
3::second + now => time start;
25::second + now => time final;

// Sound chain
Gain master => Pan2 p_m => dac;

SndBuf kick  => master;
SndBuf hihat => master;
SndBuf snare => master;
SndBuf hihat_r => dac;
SndBuf fx => dac;

// set MIDI volume
0.03 => float gain; 
// set volume
0.5 => master.gain;
0.5 => hihat_r.gain;
0.5 => fx.gain;

SawOsc m1 => Pan2 p => dac;
SqrOsc m2 => dac;

0.0 => m1.gain;
0.0 => m2.gain;
// Create a melody with MIDI notes
// [50, 52, 53, 55, 57, 59, 60, 62]: D Dorian scale
[74, 76, 74, 77, 77, 79, 77, 81, 81, 84, 81, 86, 81, 77, 76] @=> int M1[];
[50, 52, 53, 50, 52, 53, 50, 52, 53, 50, 52, 53, 50, 52, 53] @=> int M2[];

string kick_samples[2];
string hihat_samples[2];
string snare_samples[3];
string fx_samples[1];

// load soundfiles
me.dir() + "/audio/kick_02.wav" => kick_samples[0];
me.dir() + "/audio/kick_04.wav" => kick_samples[1];
me.dir() + "/audio/hihat_02.wav" => hihat_samples[0];
me.dir() + "/audio/hihat_04.wav" => hihat_samples[1];
me.dir() + "/audio/snare_01.wav" => snare_samples[0];
me.dir() + "/audio/snare_02.wav" => snare_samples[1];
me.dir() + "/audio/snare_03.wav" => snare_samples[2];
me.dir() + "/audio/hihat_03.wav" => hihat_r.read;
me.dir() + "/audio/stereo_fx_01.wav" => fx_samples[0];

// set all playheads to end so no sound is made
kick.samples() => kick.pos;
hihat.samples() => hihat.pos;
snare.samples() => snare.pos;
hihat_r.samples() => hihat_r.pos;
fx.samples() => fx.pos;

// Declare the size of decrement
0.001 => float dec;

// initialize a counter variable
0 => int counter;

// infinite loop
while( now < END )
{
    // panning
    Std.rand2f(-1.0, 1.0) => p.pan;
    Std.rand2f(-1.0, 1.0) => p_m.pan;
    // beat goes from 0 to M1.cap() - 1
    counter % M1.cap() => int beat;
    
    if(now > start && now < final)
    {
        // kick1 on 1 and 6
        if(beat == 1 || beat == 6)
        {
            kick_samples[0] => kick.read;
        }
        // kick2 on 0 and 4
        if(beat % 3  == 0)
        {
            kick_samples[1] => kick.read;
            2 => kick.rate;
        }
        // random stereo fx
        if(beat > Math.random2(4, 8))
            1.5 => fx.rate;
        else
            1.0 => fx.rate;
    
        // snare on 2 and 6
        if(beat == 2 || beat == 6)
        {
            //0 => snare.pos;
            snare_samples[Math.random2(0, snare_samples.cap() - 1)] => snare.read;
            Math.random2f(.6, 1.4) => snare.rate;
        }
        // hihat1
        hihat_samples[0] =>  hihat.read;
        0.2 => hihat.gain;
        // MIDI2
        0.01 => m2.gain;
        Std.mtof(M2[beat]) => m2.freq;
    }
    
    // hihat_r on 7
    if(beat == 7 && now > start - 3::second)
    {
        hihat_r.samples() => hihat_r.pos;
        -1.0 => hihat_r.rate;
    }
       
    // hihat2 on 7
    if(beat == 7 && now > start - 3::second)
        hihat_samples[1] =>  hihat.read;
    // set stereo fx position
    fx_samples[0] => fx.read;
    if(beat > 12)
        0.5 => fx.rate;
    else
        1.0 => fx.rate;
    // MIDI1
    gain => m1.gain;
    gain - dec/3 => gain;
    Std.mtof(M1[beat]) => m1.freq;
    // advance counter
    counter++;
    // advance time
    0.25::second => now;
}

// Final "note"
SqrOsc m3 => dac;
0.01 => m3.gain;
Std.mtof(86) => m3.freq;
1::second => now;




