/* ***************************************************************************
 *<Info>                                                                     *
 *<Filename> Scale.ck </Filename>                                            *
 *<Brief> Class that allow the general music settings of the song </Brief>   *
 *</Info>                                                                    *
 *****************************************************************************/
 
/* **********************************************************************************
 * <Class name="Scale" brief="Object use to control scale settings">                *
 ************************************************************************************/
public class Scale {
    
    //<Subsection name="Member data">
    12 => int OCTAVE;
    int scale[];
    //</Subsection>
    
    //<Subsection name="Member funtions">
   /* ***********************************************************************************
    * <Function name="init" brief="Init the scale objects settings">                    *                                           
    * <Param name="scale" brief="Scale to set ">                           *
    *************************************************************************************/
    fun void init(int scale[]) {
        scale @=> this.scale;
    }
    //</Subsection>

}