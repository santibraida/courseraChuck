/* ***********************************************
 *<Info>
 *<Filename> piano.ck </Filename>
 *<Brief> Saxophone sequence of the jazz band </Brief>
 *</Info>
 ************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Constant music setting">
BPM tempo;
/*<Action name="Start Scale settings">*/
Scale scale;
//<Action name="Set scale values" brief ="C Ionian [C, D, E, F, G, A, B]"/>
[48, 50, 52, 53, 55, 57, 59, 60]  @=> int C_Ionian[];
scale.init(C_Ionian);
//<Action name="Set chords"/>
[[scale.scale[1],scale.scale[3],scale.scale[5],scale.scale[7]], [scale.scale[4],scale.scale[6],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE],
[scale.scale[5],scale.scale[7],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE], [scale.scale[0],scale.scale[2],scale.scale[4],scale.scale[6]]] @=> int chords[][];
//</Subsection>

//<Subsection name="Set equalizer characteristics">
//<Action name="Create basic equalizer channels"/>
Gain gainMaster => dac;

//<Action name="Set equalizer values"/>
0.1 => gainMaster.gain;

//</Subsection>

//<Subsection name="Set instruments">
//<Action name="Set necesary STK instruments">
Flute sax => gainMaster;
Flute sax2 => gainMaster;

//</Subsection>

/*<Section type="musical_section" name="Play The Sax">*/
0 => int actLoop ;
0::second => dur timeIterator;
while (actLoop < 4) {

    if (Math.random2(0,5) > 1) {
        Math.random2(0, chords[actLoop].cap()-1) => int note;
        Std.mtof(chords[actLoop][note]) => sax.freq;
        spork ~playWithMe (chords[actLoop][note] + scale.OCTAVE, tempo.crotchet);
        1 => sax.noteOn;
        tempo.semiquaver => now;
        tempo.semiquaver +=> timeIterator;
    } else {
        tempo.semiquaver => now;
        tempo.semiquaver +=> timeIterator;
        1 => sax.noteOff;
        1 => sax2.noteOff;
    }
    

    if (timeIterator >= tempo.semibreve) {
       actLoop++;
       0::second => timeIterator;
   }
}
/*</Section>*/


/*<Section name="Functions">*/

/* ***********************************************************************************
 * <Function name="v1Sax" brief="Play the version one of the sax">               *                                           
 *************************************************************************************/
fun void playWithMe (int note, dur duration) {
    Std.mtof(note) => sax2.freq;
    1 => sax2.noteOn;
    duration => now;
}
/*</Section>*/