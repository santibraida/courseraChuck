/* ***********************************************
 *<Info>                                         *
 *<Filename> BPM.ck </Filename>                  *
 *<Brief> Class that calculate the BPM </Brief>  *
 *</Info>                                        *
 *************************************************/
 
/* **********************************************************************************
 * <Class name="BPM" brief="Object use to control BPM settings">                    *
 ************************************************************************************/
public class BPM {
    
    //<Subsection name="Member data">
    static float tempo; /*< Tempo */
    int timeSignature[2]; /*< Time signartue ex: 4/4 */
    static dur semibreve; /*< Whole note */
    static dur minim; /*< Half note */
    static dur crotchet; /*< Quarter note */
    static dur quaver; /*< Eighth note */
    static dur semiquaver; /*< Sixteenth note */
    //</Subsection>
    
    //<Subsection name="Member funtions">
   /* ***********************************************************************************
    * <Function name="setBPM" brief="Init the bpm settings">                            *                                           
    * <Param name="tempo" brief="Tempo in the bpm object">                              *
    * <Param name="beatsPerMeasure" brief="Beats to fill a meassure ">                  *
    * <Param name="beatDuration" brief="Duration of a beat">                            *
    *************************************************************************************/
    fun void setBPM(float tempo, int beatsPerMeasure, int beatDuration) {
        tempo => this.tempo;
        beatsPerMeasure => this.timeSignature[0];
        beatDuration => this.timeSignature[1];
        (60.0/tempo) => float floatBeat;
        (floatBeat)::second => dur durBeat;
        (timeSignature[1] == 1)  ? durBeat:(durBeat*timeSignature[0])        => this.semibreve; 
        (timeSignature[1] == 2)  ? durBeat:(durBeat*(timeSignature[0]/2.0))  => this.minim; 
        (timeSignature[1] == 4)  ? durBeat:(durBeat*(timeSignature[0]/4.0))  => this.crotchet; 
        (timeSignature[1] == 8)  ? durBeat:(durBeat*(timeSignature[0]/8.0))  => this.quaver; 
        (timeSignature[1] == 16) ? durBeat:(durBeat*(timeSignature[0]/16.0)) => this.semiquaver; 
    }
    //</Subsection>

}