/* ***********************************************
 *<Info>
 *<Filename> drums.ck </Filename>
 *<Brief> Drums sequence of the jazz band </Brief>
 *</Info>
 ************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Constant music setting">
BPM tempo;
//</Subsection>

//<Subsection name="Set equalizer characteristics">
//<Action name="Create basic equalizer channels"/>
Gain gainMaster => dac;
Gain gainBass => gainMaster;
Gain gainMiddle => gainMaster;
Gain gainHigh => gainMaster;
//<Action name="Set equalizer values"/>
0.5 => gainMaster.gain;
0.5 => gainBass.gain;
1.0 => gainMiddle.gain;
0.3 => gainHigh.gain;
//</Subsection>

//<Subsection name="Load external audio elements">
//<Action name="Load audio directory path">
me.dir(-1) + "/audio/" => string soundPath;
//<Action name="Creating necesary SndBuf objects">
SndBuf sbufKick;
SndBuf sbufSnare;
SndBuf sbufCloseHitHat;
//<Action name="Set hihat configs">
NRev hithatRev => gainHigh;
sbufCloseHitHat => hithatRev;
0.05 => hithatRev.mix;
setSndBufInitialConfigs(sbufCloseHitHat, soundPath + "hihat_02.wav", gainHigh);
setSndBufInitialConfigs(sbufKick, soundPath + "kick_02.wav", gainBass);
setSndBufInitialConfigs(sbufSnare, soundPath + "snare_03.wav", gainMiddle);
//</Subsection>

/*<Section type="musical_section" name="Play The Drumm">*/
4 => int totalMeassures;
spork ~v1HiHat(4);
spork ~v1Snare(4);
spork ~v1Kick(4);
repeat(totalMeassures) {tempo.semibreve => now;}
/*</Section>*/

/*<Section name="Functions">*/
//<Subsection name="Music phrases">
/* ***********************************************************************************
 * <Function name="v1HiHat" brief="Play the version one of the hithats"> *                                           
 *************************************************************************************/
fun void v1HiHat(int loops) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    while (actLoop < loops) {
        if (timeIterator == (tempo.semiquaver * 0) || timeIterator == (tempo.semiquaver * 4) || timeIterator == (tempo.semiquaver * 7) || timeIterator == (tempo.semiquaver * 8)) {
            0 => sbufCloseHitHat.pos;
        }

        tempo.semiquaver => now;
        tempo.semiquaver +=> timeIterator;
        if (timeIterator >= tempo.semibreve) {
            actLoop++;
            0::second => timeIterator;
        }
    }
}
/* ***********************************************************************************
 * <Function name="v1Snare" brief="Play the version one of the snare"> *                                           
 *************************************************************************************/
fun void v1Snare(int loops) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    while (actLoop < loops) {
        if (timeIterator == (tempo.quaver * 1)  || timeIterator == (tempo.crotchet * 2)) {
            0 => sbufSnare.pos;
        }
        tempo.quaver => now;
        tempo.quaver +=> timeIterator;
        if (timeIterator >= tempo.semibreve) {
            actLoop++;
            0::second => timeIterator;
        }
    }
}
/* ***********************************************************************************
 * <Function name="v1Kick" brief="Play the version one of the kick"> *                                           
 *************************************************************************************/
fun void v1Kick(int loops) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    while (actLoop < loops) {
        if (timeIterator == 0::second) {
            0 => sbufKick.pos;
        }
        tempo.crotchet => now;
        tempo.crotchet +=> timeIterator;
        if (timeIterator >= tempo.semibreve) {
            actLoop++;
            0::second => timeIterator;
        }
    }
}
//</Subsection>
//<Subsection name="Generics">
/* ***********************************************************************************
 * <Function name="setSndBufInitialConfigs" brief="Init the sound objects settings"> *                                           
 * <Param name="sndBuf" brief="Object to be initializate">                           *
 * <Param name="file" brief="File path of the sound file ">                          *
 * <Param name="gainOutput" brief="Gain output of the sndBuf object">                *
 *************************************************************************************/
fun void setSndBufInitialConfigs(SndBuf sndBuf, string file, Gain gainOutput) {
       file => sndBuf.read;
       sndBuf => gainOutput;
       sndBuf.samples() => sndBuf.pos;      
}
//</Subsection>
/*</Section>*/