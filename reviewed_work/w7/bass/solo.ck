/* ***********************************************
 *<Info>
 *<Filename> solo.ck </Filename>
 *<Brief> Saxophone sequence of the jazz band </Brief>
 *</Info>
 ************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Constant music setting">
BPM tempo;
/*<Action name="Start Scale settings">*/
Scale scale;
//<Action name="Set scale values" brief ="C Ionian [C, D, E, F, G, A, B]"/>
[48, 50, 52, 53, 55, 57, 59, 60]  @=> int C_Ionian[];
scale.init(C_Ionian);
//</Subsection>

//<Subsection name="Set equalizer characteristics">
//<Action name="Create basic equalizer channels"/>
Gain gainMaster => dac;

//<Action name="Set equalizer values"/>
0.1 => gainMaster.gain;

//</Subsection>

//<Subsection name="Set instruments">
//<Action name="Set necesary STK instruments">
Flute sax => gainMaster;

//</Subsection>

/*<Section type="musical_section" name="Play The Sax">*/
0 => int actLoop ;
0::second => dur timeIterator;
while (actLoop < 4) {
if (actLoop == 0) {
    if (timeIterator == 0::second) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 1)) {
        Std.mtof(scale.scale[3] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 2)) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 3)) {
        Std.mtof(scale.scale[4] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 4)) {
        Std.mtof(scale.scale[5] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn; 
    } else if (timeIterator == (tempo.quaver * 5)) {
        1 => sax.noteOff;
    } else if (timeIterator == (tempo.quaver * 6)) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 7)) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;

    }
} else if (actLoop == 1) {
    if (timeIterator == 0::second) {
        Std.mtof(scale.scale[3] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 1)) {
    } else if (timeIterator == (tempo.quaver * 2)) {
    } else if (timeIterator == (tempo.quaver * 3)) {
        Std.mtof(scale.scale[5] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 4)) {
        Std.mtof(scale.scale[5] + scale.OCTAVE) => sax.freq;
    } else if (timeIterator == (tempo.quaver * 5)) {
        1 => sax.noteOff;
    } else if (timeIterator == (tempo.quaver * 6)) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
    } else if (timeIterator == (tempo.quaver * 7)) {
        Std.mtof(scale.scale[1] + scale.OCTAVE) => sax.freq;
    }
}  else if (actLoop == 2) {
    if (timeIterator == 0::second) {
        Std.mtof(scale.scale[5] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 1)) {
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 2)) {
        1 => sax.noteOn;
    } else if (timeIterator == (tempo.quaver * 3)) {
        Std.mtof(scale.scale[6] + scale.OCTAVE) => sax.freq;
        1 => sax.noteOn;
    }
}
    tempo.quaver => now;
    tempo.quaver +=> timeIterator;
    if (timeIterator >= tempo.semibreve) {
       actLoop++;
       0::second => timeIterator;
   }
}
/*</Section>*/
