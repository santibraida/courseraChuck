/* ***********************************************
 *<Info>                                         *
 *<Filename> score.ck </Filename>                *
 *<Brief> Sequence of song </Brief>              *
 *</Info>                                        *
 *************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;
//<Subsection name="Initializa global settings">
/*<Action name="Start BPM settings">*/
BPM tempo;
tempo.setBPM(100, 3, 4);

/*<Action name="Start sequence">*/
Machine.add(me.dir() + "/piano.ck") => int pianoID;
tempo.semibreve*4 => now;

Machine.add(me.dir() + "/piano.ck") => pianoID;
Machine.add(me.dir() + "/bass.ck") => int bassID;
tempo.semibreve*4 => now;

Machine.add(me.dir() + "/piano.ck") => pianoID;
Machine.add(me.dir() + "/solo.ck") => int soloID;
tempo.semibreve*4 => now;

Machine.add(me.dir() + "/piano.ck") => pianoID;
Machine.add(me.dir() + "/bass.ck") =>  bassID;
Machine.add(me.dir() + "/drums.ck") =>  int drumID;
tempo.semibreve*4 => now;

Machine.add(me.dir() + "/solo2.ck") =>  soloID;
Machine.add(me.dir() + "/piano.ck") => pianoID;
Machine.add(me.dir() + "/bass.ck") =>  bassID;
Machine.add(me.dir() + "/drums.ck") => drumID;
tempo.semibreve*4 => now;
//</Subsection>