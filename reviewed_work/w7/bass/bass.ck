/* ***********************************************
 *<Info>
 *<Filename> bass.ck </Filename>
 *<Brief> Bass sequence of the jazz band </Brief>
 *</Info>
 ************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Constant music setting">
BPM tempo;
/*<Action name="Start Scale settings">*/
Scale scale;
//<Action name="Set scale values" brief ="C Ionian [C, D, E, F, G, A, B]"/>
[48, 50, 52, 53, 55, 57, 59, 60]  @=> int C_Ionian[];
scale.init(C_Ionian);

//<Action name="Set chords"/>
[[scale.scale[1],scale.scale[3],scale.scale[5],scale.scale[7]], [scale.scale[4],scale.scale[6],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE],
[scale.scale[5],scale.scale[7],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE], [scale.scale[0],scale.scale[2],scale.scale[4],scale.scale[6]]] @=> int chords[][];
for (0 => int i; i < chords.cap(); i++) {
    for (0 => int j; j < chords[i].cap(); j++) {
        chords[i][j] - scale.OCTAVE => chords[i][j];
    }
}
//</Subsection>

//<Subsection name="Set equalizer characteristics">
//<Action name="Create basic equalizer channels"/>
Gain gainMaster => dac;

//<Action name="Set equalizer values"/>
0.2 => gainMaster.gain;

//</Subsection>

//<Subsection name="Set instruments">
//<Action name="Set necesary STK instruments">
Mandolin bass => NRev r => gainMaster;
0.1 => r.mix;
0.0 => bass.stringDamping;
0.02 => bass.stringDetune;
0.05 => bass.bodySize;

//</Subsection>

/*<Section type="musical_section" name="Play The Bass">*/
4 => int maxLoop;
spork ~v1Bass(4);
repeat (maxLoop) tempo.semibreve => now;
/*</Section>*/


/*<Section name="Functions">*/

/* ***********************************************************************************
 * <Function name="v1Piano" brief="Play the version one of the piano"> *                                           
 *************************************************************************************/
fun void v1Bass(int loops) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    while (actLoop < loops) {
        if (actLoop == 0) {  spork ~arpeggiatorAsc(bass, chords[0], tempo.crotchet);}
        else if (actLoop == 1) {  spork ~arpeggiatorAsc(bass, chords[1], tempo.crotchet);}
        else if (actLoop == 2) { spork ~arpeggiatorAsc(bass, chords[2], tempo.crotchet);}
        else if (actLoop == 3) {  spork ~arpeggiatorAsc(bass, chords[3], tempo.crotchet);}
        
        tempo.semibreve => now;
        tempo.semibreve +=> timeIterator;
        if (timeIterator >= tempo.semibreve) {
            actLoop++;
            0::second => timeIterator;
        }
    }
}
/* ***********************************************************************************
 * <Function name="arpeggiator" brief="Play an intrument in arpegio"> *                                           
 *************************************************************************************/
fun void arpeggiatorAsc(Mandolin piano, int notes[], dur noteDuration) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    0 => int note;
    while (timeIterator <= tempo.semibreve) {
        Std.mtof(notes[note]) => piano.freq;
        1 => piano.noteOn;
        
        if (note < notes.cap()-1) 
            note++; 
        else 
            0 => note;
         
        noteDuration +=> timeIterator;
        noteDuration => now;
    }
}
/*</Section>*/