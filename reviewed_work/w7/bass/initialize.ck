/* ***********************************************
 *<Info>                                         *
 *<Filename> initialize.ck </Filename>           *
 *<Brief> Load the song info </Brief>            *
 *</Info>                                        *
 *************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Loading classes">
/*<Action name="Load class BPM">*/
Machine.add(me.dir() + "/myBPM.ck");
/*<Action name="Load class Scale">*/
Machine.add(me.dir() + "/Scale.ck");

//</Subsection>

/*<Action name="Load score">*/
Machine.add(me.dir() + "/score.ck");

