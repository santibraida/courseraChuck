/* ***********************************************
 *<Info>
 *<Filename> piano.ck </Filename>
 *<Brief> Piano sequence of the jazz band </Brief>
 *</Info>
 ************************************************/
 
/*<Action name="Print file name">*/
<<< "Assigment_7_ClasStyle by Mass90" >>>;

//<Subsection name="Constant music setting">
BPM tempo;
/*<Action name="Start Scale settings">*/
Scale scale;
//<Action name="Set scale values" brief ="C Ionian [C, D, E, F, G, A, B]"/>
[48, 50, 52, 53, 55, 57, 59, 60]  @=> int C_Ionian[];
scale.init(C_Ionian);
//<Action name="Set chords"/>
[[scale.scale[1],scale.scale[3],scale.scale[5],scale.scale[7]], [scale.scale[4],scale.scale[6],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE],
[scale.scale[5],scale.scale[7],scale.scale[1] + scale.OCTAVE,scale.scale[3] + scale.OCTAVE], [scale.scale[0],scale.scale[2],scale.scale[4],scale.scale[6]]] @=> int chords[][];
//</Subsection>

//<Subsection name="Set equalizer characteristics">
//<Action name="Create basic equalizer channels"/>
Gain gainMaster => dac;

//<Action name="Set equalizer values"/>
0.5 => gainMaster.gain;

//</Subsection>

//<Subsection name="Set instruments">
//<Action name="Set necesary STK instruments">
Rhodey piano[4];
setArrayOutputs(piano, gainMaster);
piano[0].gain(.3);
piano[1].gain(.3);
piano[2].gain(.3);
piano[3].gain(.3);
//</Subsection>

/*<Section type="musical_section" name="Play The Piano">*/
4 => int maxLoop;
spork ~v1Piano(4);
repeat (maxLoop) tempo.semibreve => now;
/*</Section>*/


/*<Section name="Functions">*/
/* *********************************************************************************************************
 * <Function name="setChord" brief="Set the chords of the arr">                                            *                                           
 * <Param name="arr" brief="Array to set the chords">                                                      *
 * <Param name="chord" brief="Array of notes">                                                             *
 ***********************************************************************************************************/
fun void setChord(Rhodey arr[], int chord[]) {
    for (0 => int i; i < arr.cap(); i++) {
        Std.mtof(chord[i]) => arr[i].freq;
    }
}
/* *********************************************************************************************************
 * <Function name="setArrayOutputs" brief="Set the output Gain of all elements in an array">               *                                           
 * <Param name="arr" brief="Intrument to set the output">                                                  *
 * <Param name="output" brief="Gain output">                                                               *
 ***********************************************************************************************************/
fun void setArrayOutputs(Rhodey arr[], Gain output) {
    for (0 => int i; i < arr.cap(); i++) {
        arr[i] => output;
    }
}

/* *********************************************************************************************************
 * <Function name="playPiano" brief="Set the output Gain of all elements in an array">               *                                           
 * <Param name="piano" brief="Array to play">                                                  *
 ***********************************************************************************************************/
fun void playPiano(Rhodey piano[]) {
    for (0 => int i; i < piano.cap(); i++) {
        1 => piano[i].noteOn;
    }
}


/* ***********************************************************************************
 * <Function name="v1Piano" brief="Play the version one of the piano"> *                                           
 *************************************************************************************/
fun void v1Piano(int loops) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    while (actLoop < loops) {
        if (actLoop == 0) { setChord(piano, chords[0]);  spork ~arpeggiatorAsc(piano[0], chords[0], tempo.quaver);}
        else if (actLoop == 1) { setChord(piano, chords[1]);  spork ~arpeggiatorAsc(piano[1], chords[1], tempo.quaver);}
        else if (actLoop == 2) { setChord(piano, chords[2]); spork ~arpeggiatorAsc(piano[2], chords[2], tempo.quaver);}
        else if (actLoop == 3) { setChord(piano, chords[3]); spork ~arpeggiatorAsc(piano[3], chords[3], tempo.quaver);}

        if (timeIterator == 0::second ) {
            playPiano(piano);
        }
        
        tempo.semibreve => now;
        tempo.semibreve +=> timeIterator;
        if (timeIterator >= tempo.semibreve) {
            actLoop++;
            0::second => timeIterator;
        }
    }
}
/* ***********************************************************************************
 * <Function name="arpeggiator" brief="Play an intrument in arpegio"> *                                           
 *************************************************************************************/
fun void arpeggiatorAsc(Rhodey piano, int notes[], dur noteDuration) {
    0 => int actLoop ;
    0::second => dur timeIterator;
    0 => int note;
    while (timeIterator <= tempo.semibreve) {
        Std.mtof(notes[note]) => piano.freq;
        1 => piano.noteOn;
        
        if (note < notes.cap()-1) 
            note++; 
        else 
            0 => note;
         
        noteDuration +=> timeIterator;
        noteDuration => now;
    }
}
/*</Section>*/