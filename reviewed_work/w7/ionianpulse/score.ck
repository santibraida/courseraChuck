// score.ck
// Title: Ionian Pulse
// Date: 12/8/2013
//see initialize.ck for full description of piece


//Create tempo object and variable tempo control shred
BPM tempo;
tempo.tempo(96.0); //.625 seconds/beat = 96 bpm

ScaleClass myScale;
myScale.setScale([48, 50, 52, 53, 55, 57, 59, 60]);

//time check
now => time start;

//GO!
Machine.add(me.dir() + "/bass.ck") => int bassID;
Machine.add(me.dir() + "/drums.ck") => int drumID;
Machine.add(me.dir() + "/oscillator.ck") => int oscID;

16::tempo.quarter => now;

//introduce tempo controller
Machine.add(me.dir()+"/varBPM.ck") => int bpmID;
1::samp => now; //advance time 1 sample to kick off the loop in varBPM

24::tempo.quarter => now;

Machine.remove(bpmID);
Machine.remove(drumID);
Machine.remove(bassID);

//take back control of the tempo for the outro
tempo.tempo(96);

8::tempo.quarter => now;

Machine.remove(oscID);

//time check
<<<(now - start)/second>>>;