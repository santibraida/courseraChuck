// bass.ck
// Title: Ionian Pulse
// Date: 12/8/2013

//sound chain
Mandolin bass => NRev r=> Gain master => dac;

//Load classes for BPM and scale
BPM tempo;
ScaleClass myScale;

//scale data
myScale.notes @=> int scale[];
[scale[2], scale[4], scale[7]] @=> int scale1[];
[scale[1], scale[3], scale[5], scale[6]] @=> int scale2[];

//parameter setup
0.1 => r.mix;
10.0 => bass.stringDamping;
0.01 => bass.bodySize;

.2 => bass.gain;

0 => int count;

// loop
while(true)
{    
    //Set note duration - declared within the loop because it varies throughout the piece
    tempo.eighth => dur eighth;

    //change notes every measure, alternating different subsets of the scale
    if (count % 8 == 0)
    {
        Math.random2(0, scale1.cap()-1) => int note;
        Std.mtof(scale1[note]-12) => bass.freq;
    }
    
    else if (count % 8 == 4)
    {
        Math.random2(0, scale2.cap()-1) => int note;
        Std.mtof(scale2[note]-12) => bass.freq;
    }
    
    //always play the root note every 16 beats
    if (count % 16 == 0)
    {
        Std.mtof(scale[0]-12) => bass.freq;
    }
    
    //play note with a random attack
    Math.random2f(0.8,0.85) => bass.noteOn;
    
    eighth => now;
    count++;
    
}