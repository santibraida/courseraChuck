//Controls the tempo of the piece. Adapted from the Week 7 lecures.
public class BPM
{
    //member variables
    dur myDuration[4];
    static dur quarter, eighth, sixteenth, thirtysecond;
    
    //function to define quarter -> 32nd notes; input "beat" is BPM
    fun void tempo (float beat)
    {
        60.0/beat => float SPB; //second per beat
        SPB::second => quarter;
        quarter*0.5 => eighth;
        eighth*0.5 => sixteenth;
        sixteenth*0.5 => thirtysecond;
        
        //store data in array
        [quarter, eighth, sixteenth, thirtysecond] @=> myDuration;
    }
    
}
