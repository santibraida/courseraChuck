//scale.ck
// Title: Ionian Pulse
// Date: 12/1/2013

//holds a static array to control the scale used in the piece

public class ScaleClass 
{
    
    //member variable to hold scale array.
    static int notes[];
    
    
    //load the given array into the variable
    fun void setScale(int scaleInput[])
    {
        scaleInput @=> notes;
    }
    
}