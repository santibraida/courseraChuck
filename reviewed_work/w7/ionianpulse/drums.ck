// drums.ck
// Title: Ionian Pulse
// Date: 12/8/2013

//load classes for tempo
BPM tempo;

//Sound Deck
Gain master => dac;
SndBuf snare => master;
SndBuf kick => master;
SndBuf hihat => master;

.3 => master.gain;

//Load samples and set positions to 0
me.dir(-1) + "/audio/kick_03.wav" => kick.read;
me.dir(-1) + "/audio/snare_02.wav" => snare.read;

//load hihat as an array
string hihat_samples[2];

me.dir(-1) + "/audio/hihat_03.wav" => hihat_samples[0];
me.dir(-1) + "/audio/hihat_04.wav" => hihat_samples[1];


snare.samples() => snare.pos;
kick.samples() => kick.pos;

//detune everything
0.6 => kick.rate;
0.6 => snare.rate;


//drum patterns
//most are not used in this composition but could be used elsewhere
[1,1,1,1,1,1,1,1] @=> int kick_ptrn_1[];
[1,0,0,0,1,0,1,0] @=> int kick_ptrn_2[];
[0,0,0,0,1,1,1,0] @=> int kick_ptrn_3[];

[1,0,0,0,1,0,0,0] @=> int snare_ptrn_1[];
[1,1,1,1,1,1,1,1] @=> int snare_ptrn_2[];
[0,0,0,0,0,0,1,1] @=> int snare_ptrn_3[];

[1,0,1,0,1,0,1,0] @=> int hihat_ptrn_1[];
[0,0,1,0,0,0,1,0] @=> int hihat_ptrn_2[];
[1,0,0,0,0,0,0,0] @=> int hihat_ptrn_3[];




//Function to play a full measure. Takes arguments:
//arrays for the kick, snare and hihat patterns
//the duration of one beat
fun void measure( int kickArray[], int snareArray[], int hihatArray[] )
{
    //sequencer
    for (0 => int i; i < kickArray.cap(); i++)
    {
        //play kick pattern
        if (kickArray[i] == 1)
        {
            0 => kick.pos;   
            Math.random2f(0.9, 1.0) => kick.gain;
            
        }
        
        //play snare pattern
        if (snareArray[i] == 1)
        {
            Math.random2f(0.9, 1.0) => snare.gain;
            
            0 => snare.pos;   
        }
        
        //play hihat from set of samples
        if (hihatArray[i] == 1)
        {
            Math.random2(0, hihat_samples.cap() -1) => int which;
            hihat_samples[which] => hihat.read;
            Math.random2f(0.3, 0.5) => hihat.gain;
        }
        
        //Set duration
        tempo.sixteenth => dur sixteenth;
        sixteenth => now;
        
        
        
    }
}


while(true)
{
    
    measure(kick_ptrn_2, snare_ptrn_3, hihat_ptrn_3);
}