//oscillator.ck
// Title: Ionian Pulse
// Date: 12/8/2013

//Triple triangle oscillator
//plays in a round
TriOsc s[3];
Pan2 p[3];

//Load classes for BPM and scale
BPM tempo;
ScaleClass myScale;

//Oscillator paramters
//set gain and pan on each
for ( 0 => int i; i < s.cap(); i++)
{
    s[i] => p[i] => dac;
    0.5/s.cap() => float maxGain;
    maxGain => s[i].gain;
    
    //notes are panned to stero
    if (i == 0)
    {
        0.7 => p[i].pan;
    }
    else if (i == 1)
    {
        -0.7 => p[i].pan;
    }
    else
    {
        0 => p[i].pan;
    }
    
}

//declare scale
myScale.notes @=> int scale[];

//plays a round by passing a random midi value 
//from the specified scale sequentially 
//through an array of oscillators
fun void play(int scaleArray[])
{
    int notes[3];
    
    Math.random2(0, scale.cap()-1) => notes[0];
    Std.mtof(scale[notes[0]]) => s[0].freq;
    
    notes[0] => notes[1];
    Std.mtof(scale[notes[1]]) => s[1].freq;
    
    
    Math.random2(0, scale.cap()-1) => notes[0];
    Std.mtof(scale[notes[0]]) => s[0].freq;
    
    while(true)
    {
        //Set durations
        tempo.sixteenth => dur sixteenth;
        
        notes[1] => notes[2];
        Std.mtof(scale[notes[2]]) => s[2].freq;
        
        
        notes[0] => notes[1];
        Std.mtof(scale[notes[1]]) => s[1].freq;
        
        
        Math.random2(0, scale.cap()-1) => notes[0];
        Std.mtof(scale[notes[0]]) => s[0].freq;
        
        sixteenth => now;
        
    }
    
    
}

play(scale);
