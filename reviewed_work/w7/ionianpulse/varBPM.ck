//varBPM.ck
// Title: Ionian Pulse
// Date: 12/8/2013

//Controls the BPM object
//varies the initial tempo sinusoidally around the starting tempo

//initialize BPM class
BPM tempo;

96.0 => float i; //.625 seconds/beat = 96 bpm
tempo.tempo(i); //set tempo

//counter used to drive the Math.sin function
0 => int count;

//change tempo once per measure
//following a sine wave pattern
while (true)
{
    (96.0 + Math.sin(count)*16) => i;
    tempo.tempo(i);
    
    <<<"Current Tempo: " + i>>>; //print the current tempo
    
    tempo.quarter*4 => now;
    count++;
}