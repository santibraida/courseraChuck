// initialize.ck
// Title: Ionian Pulse
// Date: 12/8/2013

// Musically this is a variation to my submission for week 6,
// changing the scale used, the drum patterns and much of the timing.

// The most signficant changes are structural: 
// using classes to store tempo and scale for the piece and
// sporking a shred to vary the tempo dynamically, 
// giving it a rhythmic pulse that varies sinusoidally throughout
// around the main compositional pulse given in the assignment.

//Add classes for Tempo and Scale
Machine.add(me.dir()+"/BPM.ck");
Machine.add(me.dir()+"/scale.ck");


// Add score file
Machine.add(me.dir() + "/score.ck");