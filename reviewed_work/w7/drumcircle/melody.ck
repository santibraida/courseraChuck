SinOsc sin;

[48, 50, 52, 53, 55, 57, 59, 60] @=> int scale[];

scale[2] => sin.freq;
0.5 => sin.gain;
NRev reverb => dac.left;

.1 => reverb.mix;

BPM tempo;

spork ~ chords();
spork ~ melody();

fun void chords()
{
    while (true)
    {
        tempo.quarterNote => dur quarter;
        
        for (0 => int beat; beat < 4; beat++)
        {
            quarter => now;
        }
    }
}

fun void melody()
{
    while (true)
    {
    while (true)
    {
        tempo.quarterNote => dur quarter;
        
        for (0 => int beat; beat < 4; beat++)
        {
            quarter => now;
        }    
    }
}