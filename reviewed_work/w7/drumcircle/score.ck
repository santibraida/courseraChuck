me.dir() => string ruta;

BPM tempo;

// .625::second
tempo.tempo(96.0);

Machine.add(ruta + "/kick.ck") => int kickId;
8.0 * tempo.quarterNote => now;

Machine.add(ruta + "/snare.ck") => int snareId;
8.0 * tempo.quarterNote => now;

Machine.add(ruta + "/hat.ck") => int hatId;
Machine.add(ruta + "/cow.ck") => int cowId;
8.0 * tempo.quarterNote => now;

Machine.add(ruta + "/clap.ck") => int clapId;
8.0 * tempo.quarterNote => now;

Machine.add(ruta + "/melody.ck") => int melodyId;
8.0 * tempo.quarterNote => now;

<<< "Set tempo to 80 BPM" >>>;
80.0 => float newTempo;
tempo.tempo(newTempo);
8.0 * tempo.quarterNote => now;

<<< "Now set tempo to 160 BPM" >>>;
160.0 => newTempo;
tempo.tempo(newTempo);
8.0 * tempo.quarterNote => now;

<<< "Gradually decrease tempo" >>>;
while (newTempo > 60.0)
{
    newTempo - 20 => newTempo;
    tempo.tempo(newTempo);
    <<< "Tempo = ", newTempo >>>;
    0.625::second => now;
}

Machine.remove(kickId);
Machine.remove(snareId);
Machine.remove(hatId);
Machine.remove(cowId);
Machine.remove(clapId);