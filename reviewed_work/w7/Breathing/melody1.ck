// melody1.ck
// Assignment_07_Breathing

// flute part
SinOsc s1 => dac;

// set gain
0.0 => s1.gain;

// timing parameters
BPM tempo;
tempo.quarterNote => dur quarter;


// Melody
12 => int octave;
48 => int root;
// scale
// root + interval
[ 0, 4, 7, 9, 11 ] @=> int scale[];

// 48, 50, 52, 53, 55, 57, 59, 60 (the C Ionian mode)
//  0   2   4   5   7   9  11  12 intervals

// random number generator seed
Math.srandom(35);

// our main time loop
0 => int i;

while (i < 21)
{
    // fade in
    if( i < 4)
    {
        s1.gain() + 0.1 => s1.gain;
    }
    root + scale[Math.random2(0, scale.cap() - 1)] + octave => int mNote;
    Std.mtof(mNote) => s1.freq;
    i++;
    // fade out
    if( i > 17)
    {
        s1.gain() - 0.1 => s1.gain;
    }   
    quarter => now;
}
