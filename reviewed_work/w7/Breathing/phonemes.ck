// phonemes.ck
// Assignment_07_Breathing

public class Phonemes
{
    // global member variables
    // named phonemes available in chucK
   
   ["eee",  "ihh",  "ehh",  "aaa", 
    "ahh",  "aww",  "ohh",  "uhh", 
    "uuu",  "ooo",  "rrr",  "lll", 
    "mmm",  "nnn",  "nng",  "ngg", 
    "fff",  "sss",  "thh",  "shh", 
    "xxx",  "hee",  "hoo",  "hah", 
    "bbb",  "ddd",  "jjj",  "ggg", 
    "vvv",  "zzz",  "thz",  "zhh"] @=> static string ph[];
        
    // subsets of the named phonemes of possible use 
    // not actually used
    
    // sibilants
    [ph[17], ph[19], ph[26], ph[29], ph[30], ph[31]] @=> static string s[];
       
    // nasals
    [ph[12], ph[13], ph[14], ph[15], ph[16]] @=> static string n[];
    
    
    // plosives
    [ph[24], ph[25], ph[27]] @=> static string p[];
        
   
}