// singing.ck
// Assignment_07_Breathing

VoicForm voc=> JCRev r => Pan2 v => dac;
0.8 => r.gain;
0.2 => r.mix;

// objects
Phonemes P;
BPM tempo;

// use full range of phonemes available
P.ph @=> string q[];

// run this routine at twice the tempo 
tempo.eighthNote => dur eighth;   
    
// settings
0.8 => voc.gain;

0.2 => voc.loudness;
0.05 => voc.vibratoGain;

// scale
[ 0, 4, 7, 9, 11 ] @=> int scale[];
// 48, 50, 52, 53, 55, 57, 59, 60 (the C Ionian mode)
//  0   2   4   5   7   9  11  12 intervals


Math.srandom(35);
48 => int root;
12 => int octave;

// infinite loop
// generate random phoneme, frequency and pan position to play

while ( true ) 
{
    
    for(0 => int i ; i < q.cap() ; i++ )
    {
        Math.random2(0, q.cap() - 1) => int k;
        q[k] => voc.phoneme;
//        <<< voc.phoneme() >>>;
        Std.mtof(root + scale[Math.random2(0, scale.cap() - 1)] + Math.random2(-1,1) * octave) => voc.freq;
        0.2 => voc.speak;
        Math.random2(-1,1) => v.pan;
        eighth - 10::ms  => now;
        0.2 => voc.quiet;
        10::ms => now;
    }

}
