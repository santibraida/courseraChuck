// ModalBar.ck
// Assignment_07_Breathing
// 

ModalBar BarOne => dac.left;
ModalBar BarTwo => dac.right;

// set initial parameterw

4 => BarOne.preset;
1 => BarTwo.preset;
0.5 => BarOne.strikePosition => BarTwo.strikePosition;
0.3 => BarOne.gain;
0.3 => BarTwo.gain;

12 => int octave;
48 => int root;
[ 0, 4, 7, 9, 11 ] @=> int scale[];

BPM tempo;
tempo.quarterNote => dur quarter;

fun void oneBeat()
{
    while ( true )
    {
        root + scale[Math.random2(0, scale.cap() - 1)] + octave => int mNote;
        Std.mtof(mNote) => BarOne.freq;
        Math.random2f(0,1) => BarOne.strikePosition;
        1 => BarOne.strike;
        quarter => now;
    }
}

fun void twoBeat()
{
    while ( true )
    {
        root + scale[Math.random2(0, scale.cap() - 1)] + octave => int mNote;
        Std.mtof(mNote) => BarTwo.freq;
        Math.random2f(0,1) => BarTwo.strikePosition;
        1 => BarTwo.strike;
        quarter * 2 => now;
    }
}

spork ~ oneBeat();
spork ~ twoBeat();

while( true ) 1:: second => now;


