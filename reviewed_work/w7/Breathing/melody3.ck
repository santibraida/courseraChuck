// Bottle.ck 
// Assignment_07_Breathing

BlowBotl bottle => NRev r1 => dac;
// set up bottle parameters with reverb
0.5 =>r1.gain;
1 => bottle.gain;
6 => float noisegain;
2 => float vibratofreq;
64 => float vibratogain;
128 => float volume;

// timing parameters
BPM tempo;
tempo.quarterNote => dur quarter;


// Melody
12 => int octave;
48 => int root;

// scale
// root + interval
[ 0, 4, 7, 9, 11 ] @=> int scale[];

// 48, 50, 52, 53, 55, 57, 59, 60 (the C Ionian mode)
//  0   2   4   5   7   9  11  12 intervals

// random seed
Math.srandom(35);

fun void playBottle(int note )
{
    // bottle parameters
    bottle.controlChange( 4, noisegain );
    bottle.controlChange( 11, vibratofreq );
    bottle.controlChange( 1, vibratogain );
    
    Std.mtof(note) => bottle.freq;
    
    // play bottle
    0.8 => bottle.noteOn;    
}


// since timing unit is 625 ms then 48 beats for 30 sec composition
// set to fade out

0 => int i;

while (i < 48)
{
    root + scale[Math.random2(0, scale.cap() - 1)] +  octave => int mNote;
    
    playBottle(mNote);
    i++;
    
    // fadeout routine
    if( i > 43)
    {
        bottle.gain() - 0.2 => bottle.gain;
    }   
    quarter => now;
}
