// score.ck
// Assignment_07_Breathing

// paths to chuck file

me.dir() + "/singing.ck" => string singPath;
me.dir() + "/melody3.ck" => string melPath;
me.dir() + "/melody1.ck" => string mel1Path;

// percussion
me.dir() + "/Shaker.ck" => string shakPath;
me.dir() + "/ModalBar.ck" => string mBarPath;


// set tempo
// quarter is set to 625 ms => 96 beats per minute
BPM tempo;
tempo.tempo(96);

// start singing
// Voices
Machine.add(singPath) => int singID;
Machine.add(melPath) => int melID;
// Percussion: modal bar, shaker, hihat
Machine.add(mBarPath) => int mBarID;
Machine.add(shakPath) => int shakID;
15::second => now;

// add "flute"
Machine.add(mel1Path) => int mel1ID;
10::second => now;

// leave bottle melody playing till the end
Machine.remove(shakID);
Machine.remove(singID);
Machine.remove(mBarID);

10::ms => now;

