// Shaker.ck
// Assignment_07_Breathing
// 

//Timing parameters
BPM tempo;

tempo.quarterNote => dur quarter;


SndBuf hihat => Pan2 h1 => dac;

// read in drums
me.dir(-1) + "/audio/hihat_01.wav" => hihat.read;
// mute hihat for starters
hihat.samples() => hihat.pos;
0.3 => hihat.gain;

Shakers shak => NRev r2 => dac;
0.6 => r2.gain;
1.0 => shak.energy;
2 => shak.preset; // Sekere

// Sound field
-1.0 => float left;
0.0 => float middle;
1.0 => float right;

left => h1.pan;

Math.srandom(29);

fun void playShaker()
{
    Math.random2f(0.0,100.0) => shak.objects;
    Math.random2f(0.0,1.0) => shak.decay;
    1 => shak.noteOn;    
}


0 => int i;

while(true)
{
    if( (i % 4 == 0) || (i % 4 == 2))
    {
        playShaker();
    }
    else
    {
        if ( i < 32 )
        {
            0 => hihat.pos;
        }
    }
    quarter => now;
    i++;
}