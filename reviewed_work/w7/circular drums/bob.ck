// bob.ck
// sound chain
Bowed bass => NRev r => dac;

// scale data

[48, 50, 52, 53, 55, 57, 59, 60] @=> int scale[];

//parameter setup
0.05 => r.mix;
//0.0 => bass.stringDamping;
//0.02 => bass.stringDetune;
//0.05 => bass.bodySize;
4 => int walkPos;
0.6 => float noteValue;
0.025 => float restValue;
0.3 => bass.gain;

//loop
/*
for( 0 => int i; i <4; i++ )
{
    scaleintro[i] => bass.freq;
    
    Math.random2f(1.0,1.0) => bass.pluckPos;
    .3 => bass.noteOn;
    noteValue*2::second => now; // .6 second plus .025 noteoff = required .0625 required pulse
    1 => bass.noteOff;
    restValue*2::second => now;
}
*/
for( 0 => int i; i <15; i++ )
{
    Math.random2(-1,1) +=> walkPos;
    if (walkPos < 0) 1 => walkPos;
    if (walkPos >= scale.cap()) scale.cap() - 2 => walkPos;
    Std.mtof(scale[walkPos] - 12) => bass.freq;
    Math.random2f(0.0,1.0) => bass.bowPosition;
    .5 => bass.noteOn;
    noteValue::second => now; // .6 second plus .025 noteoff = required .0625 required pulse
    1 => bass.noteOff;
    restValue::second => now;
}

for( 0 => int i; i <8; i++ )
{
    //scaleintro[i] => bass.freq;
    //Math.random2f(1.0,1.0) => bass.pluckPos;
    .1 => bass.noteOn;
    noteValue*2::second => now; // .6 second plus .025 noteoff = required .0625 required pulse
    1 => bass.noteOff;
    restValue*2::second => now;
}

