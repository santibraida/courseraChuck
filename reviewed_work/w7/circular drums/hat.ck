// hat.ck

SndBuf hat => dac;
0.05 => hat.gain;
1.2 => hat.rate;


me.dir(-1) + "/audio/hihat_02.wav" => hat.read;

BPM tempo;

while( 1 )
{
    tempo.eighthNote => dur eighth;// set up 8th not pulse
    
    
    for( 0 => int beat; beat < 8; beat++ )
    {
        
        if ((beat !=4) && (beat !=8))// do not sound on beats 4&8
        {
            0 => hat.pos;
        }
        eighth => now;
    }
}