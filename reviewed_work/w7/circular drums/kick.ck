// kick.ck
// sound chain

SndBuf kick => dac;
0.6 => kick.gain;

me.dir(-1) + "/audio/kick_02.wav" => kick.read;

//create BPM object
BPM tempo;
// tempo.tempo(100);

while( true )
{
    tempo.eighthNote => dur eighth;
    
    //play a measure of rest snare rest sna-snare
    0 => kick.pos;
    eighth => now;
    0 => kick.pos;
    2.0*eighth => now;
    0 => kick.pos;
    4.0*eighth => now;
    0 => kick.pos;
    2.0*eighth => now;
}