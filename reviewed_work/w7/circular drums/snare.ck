// snare.ck

SndBuf snare => dac;
0.5 => snare.gain;

me.dir(-1) + "/audio/snare_03.wav" => snare.read;
-1.0 => snare.rate;
//snare.samples() => snare.pos;
snare.samples() => int snsamples;

BPM tempo;

while( 1 )
{
    tempo.quarterNote => dur quarter;
    
    //play a measure of rest snare rest sna-snare
    quarter => now;
    snsamples => snare.pos;
    2*quarter => now;
    snsamples => snare.pos;
    2*quarter => now;
    snsamples => snare.pos;
    quarter => now;
    snsamples => snare.pos;
    quarter => now;
    /*snsamples => snare.pos;
    quarter => now;
    snsamples => snare.pos;*/
}