// score.ck


BPM tempo;
tempo.tempo(96.0); // 96BPM equivalent to 0.625=quarter

Machine.add(me.dir()+"/kick.ck") => int kickID;
8.0*tempo.quarterNote => now;

Machine.add(me.dir()+"/snare.ck") => int snareID;
8.0*tempo.quarterNote => now;

Machine.add(me.dir()+"/hat.ck") => int hatID;
Machine.add(me.dir()+"/cow.ck") => int cowID;
Machine.add(me.dir()+"/bob.ck") => int bobID;
Machine.add(me.dir()+"/stifkarp.ck") => int stifkarpID;
8.0*tempo.quarterNote => now;

Machine.add(me.dir()+"/clap.ck") => int clapID;
8.0*tempo.quarterNote => now;




48.0 => float newtempo;
tempo.tempo(newtempo);
2.0*tempo.quarterNote => now;






/*<<< "gradually decrease tempo" >>>;
while (newtempo > 60.0)
{
    newtempo - 5 => newtempo;
    tempo.tempo(newtempo);
    <<< "tempo= ", newtempo >>>;
    2.0*tempo.quarterNote => now;
}
*/
Machine.remove(kickID);
Machine.remove(snareID);
Machine.remove(hatID);
Machine.remove(cowID);
Machine.remove(clapID);
Machine.remove(bobID);
// final bass drop
SinOsc s => dac;
TriOsc t => dac;

196 => float s_freq; // set pitch variable to G
(130.81/2) => t.freq; // set to C
0.4 => s.gain;
0.4 => float triGain; //gain variable for triangle
now + 5::second => time later; // 5 second fade

while( now < later )
{
    s_freq => s.freq;
    s_freq - 1 => s_freq; // bass drop
    triGain => t.gain;
    triGain - 0.004 => triGain; // fadeout
    .05::second => now;
}
5::second=> now;
Machine.remove(stifkarpID);
<<< "THE END" >>>;