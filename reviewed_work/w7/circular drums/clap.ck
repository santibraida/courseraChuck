// clap.ck

//SndBuf clap => dac;
//0.1 => clap.gain;
ModalBar solo => JCRev rev => dac;
solo => Delay d => d => rev;

//sound parameters
0.02 => rev.mix;
0.8::second => d.max => d.delay;
0.3 => d.gain;


//me.dir(-1) + "/audio/clap_01.wav" => clap.read;

BPM tempo;

while( 1 )
{
    tempo.sixteenthNote => dur sixteenth;
    
    //play a measure of 16 16th notes
    for( 0 => int beat; beat < 16; beat++ )
    {
        // clap randomly on 16th notes
        if (Math.random2(0,7) < 3)
        {
            //0 => clap.pos;
            1 => solo.strike;
        }
        sixteenth => now;
    }
}