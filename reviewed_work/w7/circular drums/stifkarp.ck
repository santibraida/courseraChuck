// stifkarp.ck
// sound chain
StifKarp mbar => NRev mbarverb => dac;
//set reverb mix
.2 => mbarverb.mix;
// modalbar params
.1 => mbar.sustain;
.9 => mbar.pickupPosition;
0.2 => mbar.gain;

//set note duration and pitch
0.625 => float noteValue;
Std.mtof((48)/2) => mbar.freq;

for( 0 => int i; i < 5; i++ )
{   
    Math.random2f(0.1,0.5) => mbar.pickupPosition;
    .05 => mbar.noteOn;
    noteValue*2::second => now; 
}



spork ~ warble();

for( 0 => int i; i <5; i++ )
{
    //note!
    1 => mbar.noteOn;
    noteValue::second => now;
    .7 => mbar.noteOn;
    noteValue::second => now;
    repeat (6)
    {
        .5 => mbar.noteOn;
        (noteValue/2)::second => now;
    }
    
}

fun void warble()
{
    for( 0 => int i; i <5; i++ )
    {
        //update freq
        (48*2) + Math.sin( now/second*Math.PI*.2)*2
        => Std.mtof => mbar.freq;
        // advance time
        noteValue::ms => now;
    }
}
//fade out
for( .2 => float i; i >0; i-.01 )
{

    i=>mbar.gain;
    .01::second=> now;   
}
    