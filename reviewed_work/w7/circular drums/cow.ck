// cow.ck

SndBuf cow => dac;
0.3 => cow.gain;


me.dir(-1) + "/audio/click_01.wav" => cow.read;

BPM tempo;

while( true )
{
    tempo.eighthNote => dur eighth;
    
    for( 0 => int beat; beat < 8; beat++ )
    {
        
        if ((beat == 2) || (beat == 7))
        {
            0 => cow.pos;
        }
        eighth => now;
    }
}