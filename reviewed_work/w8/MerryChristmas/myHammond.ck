public class myHammond
{
	// declares 3 instruments
	BeeThree inst[3];
	Pan2 p => dac;
	// initialises a volume
	.1 => p.gain => float vol;
	
	// Initialise to chuck the instruments to pan2
	fun void initialize()
	{
		vol => p.gain;
		-.1 => p.pan;
		for (0 => int i; i<3; i++)
			inst[i] => p;
	}
	
	fun void setVol(float v)
	{
		v => vol => p.gain;
	}
	
	// Sets the chords for 3 root notes
	fun void chord(string root)
	{
		if (root == "C")
		{
			Std.mtof(48) => inst[0].freq;
			Std.mtof(52) => inst[1].freq;
			Std.mtof(55) => inst[2].freq;
			
		}
		if (root == "E")
		{
			Std.mtof(52) => inst[0].freq;
			Std.mtof(55) => inst[1].freq;
			Std.mtof(59) => inst[2].freq;
			
		}
		if (root == "F")
		{
			Std.mtof(53) => inst[0].freq;
			Std.mtof(57) => inst[1].freq;
			Std.mtof(60) => inst[2].freq;
			
		}
		if (root == "G")
		{
			Std.mtof(55) => inst[0].freq;
			Std.mtof(59) => inst[1].freq;
			Std.mtof(62) => inst[2].freq;
			
		}

	}
	
	// play : sets volume for each noteOn
	fun void play(float st)
	{
		for (0 => int i; i<3; i++)
			st => inst[i].noteOn;
	}
	
	// Stops the play
	fun void stop()
	{
		for (0 => int i; i<3; i++)
		{
			1.0 => inst[i].noteOff;
			0 => inst[i].noteOn;
		}
	}
}