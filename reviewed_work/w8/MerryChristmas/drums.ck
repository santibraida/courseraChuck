// drums.ck

// sound chain
SndBuf hihat => Pan2 p1 => dac;
SndBuf kick => Pan2 p2 => dac;
0.25 => p1.pan;
.1 => p2.pan;
BPM t;

// me.dirUp 
me.dir(-1) + "/audio/hihat_01.wav" => hihat.read;
me.dir(-1) + "/audio/kick_01.wav" => kick.read;

// parameter setup
.5 => hihat.gain;
.3 => kick.gain;

// loop 
while( true )  
{
    0 => kick.pos;
    0.8 => kick.rate;
	// random gain and rate for the hihat
    Math.random2f(0.1,.3) => hihat.gain;
    Math.random2(-1,1)*1.1 => hihat.rate;
    if (hihat.rate() < 0)
        hihat.samples() / 2 => hihat.pos;
    else
        0 => hihat.pos;
	
    (Math.random2(1,2)/2.0 * t.quarterNote) => now;
    0 => hihat.rate;
}
