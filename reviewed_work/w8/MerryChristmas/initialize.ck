// Assignment_Final_MerryChristmas
<<< "Assignment_Final_MerryChristmas">>>;
// initialize.ck

// our conductor/beat-timer class
Machine.add(me.dir()+"/BPM.ck");

// my class to add some functions to an BeThree instrument
Machine.add(me.dir()+"/myHammond.ck");

// add score.ck
me.dir() + "/score.ck" => string scorePath;
Machine.add(scorePath);


