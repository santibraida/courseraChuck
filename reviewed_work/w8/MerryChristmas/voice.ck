//Soundchain

SinOsc s => SinOsc w => Envelope env => dac;

BPM t;
5 => s.freq;
.5 => s.gain;
.05::second => env.duration;
2 => w.sync; // Gets the vibrato.


.6 => float myGain => w.gain;

[55, 55, 55, 57,   55, 55, 52, 52,   52, 52, 52, 52,   55, 55, 55, 57,  55, 55, 52, 52, 
 52, 52, 52, 52,   62, 62, 62,  0,   62, 62, 59, 59,   59, 59, 59, 59,  60, 60, 60,  0,
 60, 60, 55, 55,   55, 55, 55, 55,   57, 57, 57,  0,   57, 57, 60, 60,  60, 59, 57, 57,
 55, 55, 55, 57,   55, 55, 52, 52,   52, 52, 52, 52,   62, 62, 62,  0,  62, 62, 65, 65, 
 65, 62, 59, 59,   60, 60, 60,  0,   60, 60, 64, 64,   64, 64, 64, 64,  60, 60, 55, 55,
 52, 52, 55, 55,   53, 53, 50, 50,   48, 48, 48, 48,   48, 48, 48, 48 ] @=> int Melody[];

while (true)
{
	for (0 => int i; i< Melody.cap(); i++)
	{ 
		// play the note if it's not a rest
		if (Melody[i] != 0) 
		{
			myGain => w.gain;
			Std.mtof(Melody[i]+12)=>w.freq;
			// for the enveloppe
			1 => env.keyOn;
		}
		else
		{
			0 => w.gain;
		}
		t.eighthNote => now;
		1 => env.keyOff;
	}
}