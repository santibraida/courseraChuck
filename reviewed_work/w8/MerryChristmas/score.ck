// score.ck

// Initialise tempo
BPM t;
t.tempo(96);  // 0.625::second for au quarter

// paths to chuck file
me.dir() + "/Hammond.ck" => string pianoPath;
me.dir() + "/drums.ck" => string drumsPath;
me.dir() + "/voice.ck" => string voicePath;

// start drums
Machine.add(drumsPath) => int drumsID;
t.quarterNote*4 => now;


// start piano and voice
Machine.add(pianoPath) => int pianoID; 
Machine.add(voicePath) => int voiceID;
t.quarterNote*12 => now; 
Machine.add(voicePath) => int voiceID2; 

t.quarterNote*76 => now;
 
 // fade
dac.gain() => float vol;
for (0 => int i; i< 16; i++)
{
	vol - vol*i/15 => dac.gain;
	t.sixteenthNote => now;
}

Machine.remove(pianoID);
Machine.remove(voiceID);
Machine.remove(voiceID2);
Machine.remove(drumsID);