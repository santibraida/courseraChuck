// hammond.ck
// sound chain

myHammond piano;
BPM t;

// root notes for chords and number of quarter to keep each chord
["C"] @=> string chords[];
[ 6 ] @=> int beats[]; 

// initializes the hammond organ

piano.initialize();
.2 => float vol => piano.vol;

while(true)
{
	for (0 => int i; i<chords.cap();i++)
	{
		// sets the chord
		piano.chord(chords[i]);
		// plays the chord at specified volume
		piano.play(vol);
		
		beats[i]*t.quarterNote => now;
	}
}


