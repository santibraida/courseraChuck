// Assignment_Final_DnB777


// hat.ck
//  global BPM conducting
SndBuf hat => dac;
me.dir(-1)+"/audio/hihat_02.wav" => hat.read;
0.2=>hat.gain;

// make a conductor for our tempo 
// this is set and updated elsewhere
BPM tempo;
0=>int counter;
while (1)  {
    // update our basic beat each measure
    tempo.sixteenthNote => dur sixteenthNote;
    
    // play a measure of sixteenthNote notes
    for (0 => int beat; beat < 8; beat++)  {
        // play each and other beat
       beat %2 =>counter;
        if (counter == 0) {
            0 => hat.pos;
        }
        sixteenthNote => now;
    }
}    
    
