// Assignment_7_DrumNBass777



// kick.ck
// BPM conducting
SndBuf kick => dac;
me.dir(-1)+"/audio/kick_04.wav" => kick.read;
0.3=>kick.gain;

// make a conductor for our tempo 
// this is set and updated elsewhere
BPM tempo;

while (1)  {
    // update our basic beat each measure
    tempo.sixteenthNote => dur sixteenthNote;

    // play a measure of sixteenthNote note kicks
    for (0 => int beat; beat < 16; beat++)  {
        if( beat==0 || beat==10 ){
        	0 => kick.pos;
        
        }
        sixteenthNote => now;
    }
}    
    
