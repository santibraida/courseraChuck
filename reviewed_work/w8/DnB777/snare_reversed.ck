// Assignment_Final_DnB777

SndBuf snare =>JCRev r=> dac;
0.2=>r.mix;
me.dir(-1)+"/audio/snare_01.wav" => snare.read;
0.5 => snare.gain;

snare.samples() => int numSamples;

-1.0 => snare.rate;

BPM tempo;

while (1)  {
    // update our basic beat each measure
   tempo.quarterNote => dur quarterNote;
    
    // play measure of: rest, snare
   
    for (0 => int beat; beat < 4; beat++)  {
      
        if (beat == 1 || beat ==3 ) {
           numSamples => snare.pos;;
        }
       quarterNote=> now;
    }
  
    
}    