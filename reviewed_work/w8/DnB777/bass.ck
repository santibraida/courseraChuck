// Assignment_Final_DnB777

LPF lpf =>dac;



// bass.ck
// sound chain (mandolin for bass)
Mandolin bass => NRev r => lpf  => Gain master =>  dac;

//layer osc sound for sub-bass
SinOsc fm=> TriOsc bass2 =>lpf => master=>dac;
600=>fm.freq;
2=>bass2.sync;

0.2=>bass2.gain;

scale scale_notes;
int melody[8];

// shared scale data
for (0=> int i; i<8; i++)
{
    
scale_notes.notes[i] @=> melody[i];
    }

[7,5,4] @=> int pattern1[];
BPM tempo;
[tempo.quarterNote*8, tempo.quarterNote*4, tempo.quarterNote*4 ] @=> dur beat_dur[];


3000=>lpf.freq;

// parameter setup
0.1 => r.mix;
0.0 => bass.stringDamping;
0.02 => bass.stringDetune;
0.05 => bass.bodySize;
0.2 => bass.gain;

0.05=>master.gain;
// loop
while( 1 )  
{
  for( 0 => int i; i < 3; i++ )  {
    Std.mtof(melody[pattern1[i]]-24) => bass.freq;
      Std.mtof(melody[pattern1[i]]-24) => bass2.freq;
    Math.random2f(0.05,0.5) => bass.pluckPos;
    1 => bass.noteOn;
    beat_dur[i]-( 0.25 :: second )  => now;
   
    1 => bass.noteOff;
    0.25 :: second => now;
  }
}


