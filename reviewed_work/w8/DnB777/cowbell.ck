// Assignment_7_DrumNBass777


// cow.ck
// global BPM conducting
SndBuf cow => dac;
me.dir(-1)+"/audio/cowbell_01.wav" => cow.read;
0.2=>cow.gain;

// make a conductor for our tempo 
// this is set and updated elsewhere
BPM tempo;

while (1)  {
    // update our basic beat each measure
    tempo.eighthNote => dur eighth;
    
    // play measure of eighths
    for (0 => int beat; beat < 8; beat++)  {
        
        if (beat == 6) {
            0 => cow.pos;
        }
        eighth => now;
    }
}    
    
