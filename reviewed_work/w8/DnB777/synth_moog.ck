// Assignment_Final_DnB777


Moog moog=>JCRev r=> dac;
0.3=>r.mix;
scale scale_notes;
int melody[8];

for (0=> int i; i<8; i++)
{
    
scale_notes.notes[i] @=> melody[i];
    }
//0.1  => moog.modDepth;	
//0.2  => moog.modSpeed;
0.3  => moog.filterQ;	
0.4  => moog.filterSweepRate;	
BPM tempo;
[tempo.eighthNote, tempo.eighthNote, tempo.sixteenthNote,tempo.sixteenthNote] @=> dur beat_dur[];
0.55=>moog.gain;
// loop
while( 1 )  
{
  for( 0 => int i; i < 4; i++ )  {
    Std.mtof(melody[0]) => moog.freq;
     
    1 => moog.noteOn;
    beat_dur[i]-( 0.01 :: second )  => now;
   
    1 => moog.noteOff;
    0.01 :: second => now;
  }

  for( 0 => int i; i < 4; i++ )  {
    Std.mtof(melody[5]) => moog.freq;
     
    1 => moog.noteOn;
    beat_dur[i]-( 0.01 :: second )  => now;
   
    1 => moog.noteOff;
    0.01 :: second => now;
  }

  for( 0 => int i; i < 4; i++ )  {
    Std.mtof(melody[4]) => moog.freq;
     
    1 => moog.noteOn;
    beat_dur[i]-( 0.01 :: second )  => now;
   
    1 => moog.noteOff;
    0.01 :: second => now;
  }
}