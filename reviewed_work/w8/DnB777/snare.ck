// Assignment_Final_DnB777



// snare.ck
// global BPM conducting
SndBuf snare => dac;
me.dir(-1)+"/audio/snare_01.wav" => snare.read;
0.7 => snare.gain;
snare.samples() => snare.pos;

// make a conductor for our tempo 
// this is set and updated elsewhere
BPM tempo;

while (1)  {
    // update our basic beat each measure
   tempo.sixteenthNote => dur sixteenthNote;
    
    // play 
   
    for (0 => int beat; beat < 16; beat++)  {
      
        if (beat == 4 || beat == 12 ) {
            0 => snare.pos;
        }
       sixteenthNote=> now;
    }
  
    
}    
    
