// Assignment_Final_DnB777



// clap.ck
// global BPM conducting
SndBuf clap => dac;
me.dir(-1)+"/audio/clap_01.wav" => clap.read;
0.25=>clap.gain;
clap.samples() => clap.pos;

// make a conductor
// this is set and controlled elsewhere
BPM tempo;

while (1)  {
    // our basic beat for this instrument
    // update this every measure
    tempo.sixteenthNote => dur sixteenthNote;
    
     for (0 => int beat; beat < 16; beat++)  {
      if (beat == 9 || beat == 12 ||beat == 13 ) {
            0 => clap.pos;
        }
       sixteenthNote=> now;
    
}    
}
    
