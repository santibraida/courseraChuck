// Assignment_Final_DnB777

SinOsc vox[3];
JCRev r;
0.4=>r.mix;
Pan2 pan1=>dac;
Pan2 pan2=>dac;
vox[0]=>pan1=>r=>dac;
vox[1]=>r=>dac;
vox[2]=>pan2=>r=>dac;
-1=>pan1.pan;
1=>pan2.pan;

for(0 => int i; i<3; i++){
	
0.03=>vox[i].gain;
}


scale scale_notes;
int melody[8];

for (0=> int i; i<8; i++)
{
    
scale_notes.notes[i] @=> melody[i];
    }

[[0,2,4],[5,0,2],[4,6,1]] @=> int pattern1[][];
BPM tempo;
[tempo.quarterNote*8, tempo.quarterNote*4, tempo.quarterNote*4 ] @=> dur beat_dur[];

while( 1 )  
{for( 0 => int x; x < 3; x++ )  {
  for( 0 => int i; i < 3; i++ )  {
    Std.mtof(melody[pattern1[x][i]]) => vox[i].freq;
     
        
  }
  beat_dur[x]  => now;
}
}