// Assignment_7_DrumNBass777


// score.ck
//  BPM conductingo
BPM tempo;
tempo.tempo(0.35);


Machine.add(me.dir()+"/hat.ck") => int hatID;
Machine.add(me.dir()+"/kick.ck") => int kickID;
Machine.add(me.dir()+"/snare.ck") => int snareID;
Machine.add(me.dir()+"/clap.ck") => int clapID;
16.0 * tempo.quarterNote => now;

Machine.add(me.dir()+"/bass.ck") => int bassID;
48.0 * tempo.quarterNote => now;

Machine.remove(snareID);
Machine.remove(clapID);
Machine.remove(bassID);
Machine.add(me.dir()+"/snare_reversed.ck") => int snarerevID;
Machine.add(me.dir()+"/synth_chords.ck") => int chordsID;
Machine.add(me.dir()+"/synth_moog.ck") => int moogID;

32.0 * tempo.quarterNote => now;

Machine.remove(snarerevID);
Machine.remove(chordsID);
Machine.remove(moogID);
Machine.add(me.dir()+"/snare.ck") => int snare2ID;
Machine.add(me.dir()+"/clap.ck") => int clap2ID;
Machine.add(me.dir()+"/bass.ck") => int bass2ID;
32.0 * tempo.quarterNote => now;

Machine.remove(snare2ID);
Machine.remove(kickID);
Machine.remove(clap2ID);
Machine.add(me.dir()+"/snare_reversed.ck") => int snarerev2ID;
16.0 * tempo.quarterNote => now;

Machine.remove(snarerev2ID);
Machine.remove(hatID);
16.0 * tempo.quarterNote => now;
Machine.remove(bass2ID);