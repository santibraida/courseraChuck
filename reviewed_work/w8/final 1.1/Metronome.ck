// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project

// Tempo setup.
BPM.beat => dur beat;

// The playing style for our instrument.
// Add a little randomness to the style's attack/decay just for lulz.
PlayingStyle metronomeStyle;
metronomeStyle.setAttackFactor(Math.random2f(0.5, 0.8));
metronomeStyle.setReleaseFactor(1.0 - metronomeStyle.getAttackFactor());

// Set up our instrument. Two sound buffers to mimic a metronome.
SndBuf sbuff[2];
ADSR   sbuffEnv[2];
Pan2   sbuffPan[2];
[ "snare_03.wav", "click_02.wav" ] @=> string sbuffSamples[];

for (0 => int i; i < sbuff.cap(); i++)
{
  SndBuf sb @=> sbuff[i];
  me.dir(-1) + "/audio/" + sbuffSamples[i] => sb.read;

  (0.01::beat, 0::beat, 1.0, 0.1::beat) => sbuffEnv[i].set;
  0.2 => sbuffPan[i].gain;

  sbuff[i] => sbuffEnv[i] => sbuffPan[i] => dac;
}

// Create the instrument object.
SBinstrument metronomeInstrument;
metronomeInstrument.initialise(sbuff, sbuffEnv);


// Define the metronome tick line.
// Format: [ number-of-beats, note-value-a, note-value-b ]

[
  [1.00, Note.PLAY, Note.REST],
  [1.00, Note.REST, Note.PLAY],
  [1.00, Note.REST, Note.PLAY]
] @=> float tickTockTock[][];  // time signature is 3/4.

// Set the metronome ticking until the shred this runs
// in is removed by a parent shred.
Player metronomePlayer;
metronomePlayer.initialise(metronomeInstrument, metronomeStyle);

while (true)
{
  metronomePlayer.playLine(tickTockTock);
}

//
// Ends.
//
