// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project

// Tempo setup.
BPM.beat => dur beat;

// The playing style for our instrument.
PlayingStyle melodyStyle;
melodyStyle.setAttackFactor(0.9);
melodyStyle.setReleaseFactor(0.1);
melodyStyle.setAverageVelocity(0.9);
melodyStyle.setVelocityDeviation(0.1);

// Set up our instrument. Currently a single STK but potential for >1.
StkInstrument instr[1];
ADSR          instrEnv[1];
NRev          instrRev[1];
Pan2          instrPan[1];

for (0 => int i; i < instr.cap(); i++)
{
  ModalBar mb @=> instr[i];
  0.9 => mb.stickHardness;
  0.7 => mb.strikePosition;
  22.0 => mb.vibratoFreq;
  1.0 => mb.vibratoGain;
  0.5 => mb.strike;

  (0.02::beat, 0.5::beat, 0.5, 0.1::beat) => instrEnv[i].set;
  0.06 => instrRev[i].mix;
  0.5  => instrPan[i].gain;

  instr[i] => instrEnv[i] => instrRev[i] => instrPan[i] => dac;
}

// Create the instrument object.
ModelledInstrument melodyInstrument;
melodyInstrument.initialise(instr, instrEnv);


// Define the melody line.
// Format: [ number-of-beats, note-value ]

[
  [1.00,  Note.Ab5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Gb6 ],
  [1.50,  Note.Bb6 ], // Bar 9
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.25,  Note.Eb6 ],
  [0.25,  Note.F6  ],
  [0.50,  Note.D6  ],
  [1.00,  Note.Eb6 ],
  [1.50,  Note.Bb6 ], // Bar 11
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.F6  ],
  [0.25,  Note.Eb6 ],
  [0.25,  Note.F6  ],
  [0.50,  Note.D6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ], // Bar 13
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ], // Bar 15
  [0.50,  Note.Ab5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Gb6 ], 
  [1.50,  Note.Bb6 ], // Bar 17
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.25,  Note.Eb6 ],
  [0.25,  Note.F6  ],
  [0.50,  Note.D6  ],
  [1.00,  Note.Eb6 ],
  [1.50,  Note.Bb6 ], // Bar 19
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.D6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ], // Bar 21
  [0.25,  Note.F6  ],
  [0.25,  Note.Gb6 ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Bb6 ],
  [0.25,  Note.Ab6 ], // Bar 23
  [0.25,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Db7 ],
  [0.57,  Note.C7  ], // Bar 25
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Gb5 ],
  [0.50,  Note.F5  ],
  [0.50,  Note.Eb5 ],
  [0.50,  Note.Db5 ],
  [0.50,  Note.C5  ],
  [0.50,  Note.Eb5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.G5  ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.Eb6 ],
  [0.25,  Note.F6  ], // Bar 29
  [0.25,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Bb6 ],
  [0.29,  Note.Ab6 ], // Bar 31
  [0.25,  Note.Bb6 ],
  [0.25,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.F7  ],
  [0.50,  Note.Eb7 ],
  [0.50,  Note.Db7 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.A5  ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.F5  ],
  [0.50,  Note.Gb5 ],
  [0.50,  Note.C5  ],
  [2.00,  Note.Db5 ], // Bar 36
  [1.00,  Note.F5  ],
  [0.50,  Note.F6  ],
  [0.25,  Note.F6  ],
  [0.25,  Note.Gb6 ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ], // Bar 38
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.25,  Note.Ab6 ],
  [0.25,  Note.Bb6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ], // Bar 40
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Db7 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.Db6 ], // Bar 42
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Gb5 ],
  [0.50,  Note.F5  ],
  [0.50,  Note.Eb5 ],
  [0.50,  Note.Db5 ],
  [0.50,  Note.C5  ],
  [0.50,  Note.Eb5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.G5  ], // Bar 44
  [0.50,  Note.Ab5 ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.F6  ],
  [0.25,  Note.Gb6 ],
  [0.25,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ], // Bar 46
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.E6  ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Bb6 ],
  [0.25,  Note.Ab6 ],
  [0.25,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ], // Bar 48
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.G6  ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.F7  ],
  [0.50,  Note.Eb7 ],
  [0.50,  Note.Db7 ],
  [0.50,  Note.C7  ],
  [0.50,  Note.Bb6 ],
  [0.50,  Note.Ab6 ],
  [0.50,  Note.Gb6 ],
  [0.50,  Note.F6  ],
  [0.50,  Note.Eb6 ],
  [0.50,  Note.Db6 ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.Ab5 ],
  [0.50,  Note.A5  ],
  [0.50,  Note.C6  ],
  [0.50,  Note.Bb5 ],
  [0.50,  Note.F5  ],
  [0.50,  Note.Gb5 ],
  [0.50,  Note.C5  ],
  [7.00,  Note.Db5 ]
] @=> float melody[][];



// Put it all together and take it away...
Player melodyPlayer;
melodyPlayer.initialise(melodyInstrument, melodyStyle);
melodyPlayer.playLine(melody);

//
// Ends.
//
