// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project

// Tempo setup.
BPM.beat => dur beat;

// The playing style for our instrument.
PlayingStyle bassStyle;
bassStyle.setAttackFactor(0.8);
bassStyle.setReleaseFactor(0.2);

// Set up our bass instrument: three oscillators plus effects units.
Osc    osc[3];
ADSR   oscEnv[3];
Pan2   oscPan[3];
LPF    oscFilt[3];
NRev rvb => dac; // A single, shared reverb unit (these are CPU-hungry).
0.04 => rvb.mix;
0.08 => rvb.gain;

for (0 => int i; i < osc.cap(); i++)
{
  SawOsc s @=> osc[i];
  (0.02::beat, 0.5::beat, 0.3, 0.2::beat) => oscEnv[i].set;

  // Low pass filter settings.
  590.0 => oscFilt[i].freq;
  2.0   => oscFilt[i].Q;

  // Add a small amount of (symmetrical) random panning to voices 0 and 2.
  Math.max(Math.randomf(), 0.3) => float panPos;
  panPos * (i-1)  => oscPan[i].pan;

  osc[i] => oscEnv[i] => oscFilt[i] => oscPan[i] => rvb;
}

// Create the instrument object.
OscInstrument bassInstrument;
bassInstrument.initialise(osc, oscEnv);


// Define the bass line.
// Format: [ number-of-beats, note-a, note-b, note-c ]

[
  [0.5,  Note.Db4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.Db5,  Note.F5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.Db5,  Note.F5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.F4,   Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.Db5,  Note.F5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.Db5,  Note.F5],
  [0.5,  Note.REST, Note.REST, Note.REST]
] @=> float melody01[][];

[
  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Eb4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST]

] @=> float melody02[][];

[
  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.C5,   Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST]

] @=> float melody03[][];

[
  [0.5,  Note.A3,   Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.F4,   Note.C5,   Note.Eb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.F4,   Note.C5,   Note.Eb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Bb3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.F4,   Note.Db5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.F4,   Note.Db5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.C4,   Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.A4,   Note.Eb5,  Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.A4,   Note.Eb5,  Note.Gb5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Db4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [1.0,  Note.REST, Note.Ab4,  Note.F5],
  [1.0,  Note.REST, Note.REST, Note.REST]

] @=> float melody04[][];

[
  [1.0,  Note.Gb4,  Note.REST, Note.REST],
  [1.0,  Note.REST, Note.Bb4,  Note.Eb5],
  [1.0,  Note.REST, Note.REST, Note.REST],

  [1.0,  Note.Ab3,  Note.REST, Note.REST],
  [1.0,  Note.F4,   Note.Ab4,  Note.Db5],
  [1.0,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Ab3,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.REST, Note.Gb4,  Note.Ab4],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Gb4,  Note.Ab4,  Note.C5],
  [0.5,  Note.REST, Note.REST, Note.REST],


  [0.5,  Note.Db4,  Note.REST, Note.REST],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [0.5,  Note.Ab4,  Note.Db5,  Note.F5],
  [0.5,  Note.REST, Note.REST, Note.REST],

  [1.0,  Note.REST, Note.REST, Note.REST]

] @=> float melody05[][];

[
  [3.0,  Note.C5,   Note.SKIP, Note.SKIP],
  [3.0,  Note.Bb4,  Note.SKIP, Note.SKIP],
  [3.0,  Note.C5,   Note.SKIP, Note.SKIP]
] @=> float melody06a[][];

[
  [1.0,  Note.SKIP, Note.REST, Note.REST],
  [0.5,  Note.SKIP, Note.Eb5,  Note.F5],
  [0.5,  Note.SKIP, Note.REST, Note.REST],
  [0.5,  Note.SKIP, Note.Eb5,  Note.F5],
  [0.5,  Note.SKIP, Note.REST, Note.REST],

  [1.0,  Note.SKIP, Note.REST, Note.REST],
  [1.0,  Note.SKIP, Note.Db5,  Note.F5],
  [1.0,  Note.SKIP, Note.Db5,  Note.F5],

  [1.0,  Note.SKIP, Note.REST, Note.REST],
  [0.5,  Note.SKIP, Note.Gb5,  Note.Ab5],
  [0.5,  Note.SKIP, Note.REST, Note.REST],
  [0.5,  Note.SKIP, Note.Gb5,  Note.Ab5],
  [0.5,  Note.SKIP, Note.REST, Note.REST],

  [1.0,  Note.Db5,   Note.REST, Note.REST],
  [1.0,  Note.REST,  Note.F5,   Note.Ab5],
  [1.0,  Note.REST,  Note.REST, Note.REST]
] @=> float melody06b[][];

[
  [1.0,  Note.Gb4,   Note.REST, Note.REST],
  [1.0,  Note.Db5,   Note.Eb5,  Note.Bb5],
  [1.0,  Note.REST,  Note.REST, Note.REST],

  [1.0,  Note.Ab3,   Note.REST, Note.REST],
  [1.0,  Note.F4,    Note.Ab4,  Note.Db5],
  [1.0,  Note.REST,  Note.REST, Note.REST],

  [1.0,  Note.Ab3,   Note.REST, Note.REST],
  [1.0,  Note.REST,  Note.Gb4,  Note.Ab4],
  [1.0,  Note.REST,  Note.Gb4,  Note.Ab4],

  [1.0,  Note.Db4,   Note.REST, Note.REST],
  [1.0,  Note.REST,  Note.Ab4,  Note.F5],
  [1.0,  Note.REST,  Note.REST, Note.REST]

] @=> float melody07[][];

[
  [1.0,  Note.A3,    Note.REST, Note.REST],
  [1.0,  Note.F4,    Note.C5,   Note.Eb5],
  [1.0,  Note.F4,    Note.C5,   Note.Eb5],

  [1.0,  Note.Bb3,   Note.REST, Note.REST],
  [1.0,  Note.REST,  Note.F4,   Note.Db5],
  [1.0,  Note.REST,  Note.F4,   Note.Db5],

  [1.0,  Note.C4,    Note.REST, Note.REST],
  [1.0,  Note.Ab4,   Note.Eb5,  Note.Gb5],
  [1.0,  Note.Ab4,   Note.Eb5,  Note.Gb5],

  [1.0,  Note.Db4,   Note.REST, Note.REST],
  [1.0,  Note.REST,  Note.Ab4,  Note.F5],
  [1.0,  Note.REST,  Note.REST, Note.REST]
] @=> float melody08[][];

[
  [1.0,  Note.Gb4,  Note.REST, Note.REST],
  [1.0,  Note.REST, Note.Bb4,  Note.Eb5],
  [1.0,  Note.REST, Note.REST, Note.REST],

  [1.0,  Note.Ab3,  Note.REST, Note.REST],
  [1.0,  Note.F4,   Note.Ab4,  Note.Db5],
  [1.0,  Note.REST, Note.REST, Note.REST],

  [1.0,  Note.Ab3,  Note.REST, Note.REST],
  [1.0,  Note.REST, Note.Gb4,  Note.Ab4],
  [1.0,  Note.Gb4,  Note.Ab4,  Note.C5],

  [1.0,  Note.Db4,  Note.REST, Note.REST],
  [1.0,  Note.Ab4,  Note.Db5,  Note.F5],
  [1.0,  Note.REST, Note.REST, Note.REST]
] @=> float melody09[][];

[
  [3.0,  Note.A4,   Note.SKIP,  Note.SKIP],
  [3.0,  Note.Bb4,  Note.SKIP,  Note.SKIP],
  [3.0,  Note.C5,   Note.SKIP,  Note.SKIP],
  [3.0,  Note.Db5,  Note.SKIP,  Note.SKIP]
] @=> float melody10a[][];

[
  [1.0,  Note.SKIP,  Note.REST,  Note.REST],
  [1.0,  Note.SKIP,  Note.Eb5,   Note.F5],
  [1.0,  Note.SKIP,  Note.Eb5,   Note.F5],

  [1.0,  Note.SKIP,  Note.REST,  Note.REST],
  [1.0,  Note.SKIP,  Note.Db5,   Note.F5],
  [1.0,  Note.SKIP,  Note.Db5,   Note.F5],

  [1.0,  Note.SKIP,  Note.REST,  Note.REST],
  [1.0,  Note.SKIP,  Note.Gb5,   Note.Ab5],
  [1.0,  Note.SKIP,  Note.Gb5,   Note.Ab5],

  [1.0,  Note.SKIP,  Note.REST,  Note.REST],
  [1.0,  Note.SKIP,  Note.F5,    Note.Ab5],
  [1.0,  Note.SKIP,  Note.REST,  Note.REST]

] @=> float melody10b[][];

[
  [1.0,  Note.Gb4,   Note.REST,  Note.REST],
  [1.0,  Note.Db5,   Note.Eb5,   Note.Bb5],
  [1.0,  Note.REST,  Note.REST,  Note.REST],

  [1.0,  Note.Ab3,   Note.REST,  Note.REST],
  [1.0,  Note.F4,    Note.Ab4,   Note.Db5],
  [1.0,  Note.REST,  Note.REST,  Note.REST],

  [1.0,  Note.Ab3,   Note.REST,  Note.REST],
  [1.0,  Note.REST,  Note.Gb4,   Note.Ab4],
  [1.0,  Note.REST,  Note.Gb4,   Note.Ab4],

  [6.0,  Note.Db4,   Note.REST,  Note.REST]
] @=> float melody11[][];


// Put it all together and take it away...
Player bassPlayer;
bassPlayer.initialise(bassInstrument, bassStyle);

bassPlayer.playLine(melody01);
bassPlayer.playLine(melody01);
bassPlayer.playLine(melody02);

bassPlayer.playLine(melody01);
bassPlayer.playLine(melody01);
bassPlayer.playLine(melody03);

bassPlayer.playLine(melody04);
bassPlayer.playLine(melody05);

spork ~ bassPlayer.playLine(melody06a); // For some lingering notes.
bassPlayer.playLine(melody06b);

bassPlayer.playLine(melody07);
bassPlayer.playLine(melody08);
bassPlayer.playLine(melody09);

spork ~ bassPlayer.playLine(melody10a); // Lingering notes again.
bassPlayer.playLine(melody10b);

bassPlayer.playLine(melody11);

//
// Ends.
//
