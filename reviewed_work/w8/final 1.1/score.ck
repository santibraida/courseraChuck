// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//
// ChucKian rendition of Frederic Chopin's Waltz in D-flat major,
// Opus 64, No. 1, ("the Minute Waltz") using a ModalBar, sine
// oscillator and sndbuf metronome.
//

// Seed ChucK's pseudo random number generator.
Math.srandom(0);

// Set the composition's tempo/main compositional pulse.
BPM bpm;
bpm.setTempo(160);
bpm.beat =>dur beat;

// A simplistic class representing playing styles.
Machine.add(me.dir()+"/PlayingStyle.ck");

// A common class used to represent notes.
Machine.add(me.dir()+"/Note.ck");

// Our instrument emulation classes.
Machine.add(me.dir()+"/Instrument.ck");
Machine.add(me.dir()+"/OscInstrument.ck");
Machine.add(me.dir()+"/SBinstrument.ck");
Machine.add(me.dir()+"/ModelledInstrument.ck");

// A class representing an instrument player.
Machine.add(me.dir()+"/Player.ck");

// Paths to the ChucK files we're going to invoke.
me.dir() + "/Bass.ck"      => string bassPath;
me.dir() + "/Melody.ck"    => string melodyPath;
me.dir() + "/Metronome.ck" => string metronomePath;

// First start the metronome ticking for six beats
// then add the other shreds in one go.
Machine.add(metronomePath) => int metronomeID;
6::beat => now;

// Begin melody line.
Machine.add(melodyPath)  => int melodyID;

// Bring the bass line in after twelve more beats.
12::beat => now;
Machine.add(bassPath)    => int bassID;

// Wait for around a minute - it's the minute waltz after all :) -
// then remove the metronome shred (which beats indefinitely).
(1::minute - 12::beat) => now;
Machine.remove(metronomeID);

// All done!
// Thanks for taking the time to review my assignment
// and I hope you've enjoyed the course as much as I have!

//
// Ends.
//
