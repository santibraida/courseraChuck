// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// BPM helps conveniently determine a composition's tempo.
public class BPM
{
    // Global variables.
    static dur quarterNote, eighthNote, sixteenthNote, thirtysecondNote, sixtyfourthNote;
    static dur beat;       // Alias for quarter note (name required by assignment rubric).
    
    // Set the tempo with this function and BPM will determine 
    // the durations for quarter notes, eighth notes etc.
    fun void setTempo(float bpm)
    {
        // bpm is beats per minute, example 120 beats per minute.
        
        60.0/(bpm)           => float SPB; // seconds per beat
        SPB::second          => quarterNote;
        quarterNote*0.5      => eighthNote;
        eighthNote*0.5       => sixteenthNote;
        sixteenthNote*0.5    => thirtysecondNote;
        thirtysecondNote*0.5 => sixtyfourthNote;

        quarterNote => beat;
    }
}

//
// Ends.
//
