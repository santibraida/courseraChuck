// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// A generic instrument superclass.
// Parent class for OscInstrument, SBinstrument and ModelledInstrument.
// Use this class as an umbrella instrument class which can
// conveniently represent any of the above subclasses in a single type.
public class Instrument
{
  fun void play(int voice, PlayingStyle style, int note, float beats)
  {
    <<< "Override me in a subclass!" >>>;
  }
}

//
// Ends.
//
