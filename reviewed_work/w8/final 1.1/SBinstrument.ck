// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// An instrument comprised of an arbitrary number of sound buffers.
public class SBinstrument extends Instrument
{
  SndBuf       mySndBufs[];
  ADSR         myEnvelopes[];

  fun void initialise(SndBuf sbs[], ADSR envs[])
  {
    sbs   @=> mySndBufs;
    envs  @=> myEnvelopes;
  }

  // Play a note on a given voice using the given style for the
  // given number of beats.
  fun void play(int voice, PlayingStyle style, int note, float beats)
  {
    // The value of note is basically ignored unless
    // it has the value Note.REST, in which case the 
    // buffer is silenced. Typically pass in Note.PLAY
    // to give 'note' a meaningful 'play something' value.
    BPM.beat => dur beat;

    if (note == Note.REST)
    {
      // We'll force silence from the sound buffer during a rest.
      mySndBufs[voice].samples() => mySndBufs[voice].pos;
      1.0 => mySndBufs[voice].rate;
    }
    else
    {
      0 => mySndBufs[voice].pos;
      1.0 => mySndBufs[voice].rate;
    }

    // Attack-Decay-Sustain phase.
    1 => myEnvelopes[voice].keyOn;
    style.getAttackFactor() * beats::beat => now;

    // Release phase.
    1 => myEnvelopes[voice].keyOff;
    style.getReleaseFactor() * beats::beat => now;
  }
}

//
// Ends.
//
