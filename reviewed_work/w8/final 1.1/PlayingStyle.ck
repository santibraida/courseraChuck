// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// A playing style describes how a note is played. At this
// point, all it does is encode what proportion of a note's
// 'play time' is comprised of its attack-decay-sustain phase
// versus its release phase. A style can also emulate a velocity 
// profile (how hard a note is played). The velocity profile is
// only useful for STK instruments which can make use of it.
public class PlayingStyle
{
  float myAttackFactor;      // Attack-Decay-Sustain time multiplier.
  float myReleaseFactor;     // Release time multiplier.
  float myAverageVelocity;   // Average note strike velocity.
  float myVelocityDeviation; // Effective velocity is average +/- deviation.

  // Emulate what is essentially a default constructor.
  initialise();

  fun void initialise()
  {
    // Initial settings.
    0.0 => myAttackFactor;
    0.0 => myReleaseFactor;
    0.0 => myAverageVelocity;
    0.0 => myVelocityDeviation;
  }

  // Returns the style's base velocity value.
  fun float getAverageVelocity()
  {
    return myAverageVelocity;
  }

  // Sets the style's "typical" velocity value.
  fun void setAverageVelocity(float vel)
  {
    vel => myAverageVelocity;
  }

  // Return the style's deviation. See below for description.
  fun float getVelocityDeviation()
  {
    return myVelocityDeviation;
  }

  // The deviation constrains the amount of random 
  // variation above or below the style's average
  // velocity value.
  fun void setVelocityDeviation(float dev)
  {
    dev => myVelocityDeviation;
  }

  // Return a velocity typical of this style.
  fun float getVelocity()
  {
    Math.randomf() * myVelocityDeviation => float deviation;
    Math.random2(-1, 1) => int sign;
    myAverageVelocity + (deviation * sign), 1.0 => float velocity;

    // Make sure the 0..1 range is not exceeded.
    Math.min(velocity, 1.0) => velocity;
    Math.max(velocity, 0.0) => velocity;

    return velocity;
  }

  // The Attack-Decay-Sustain portion of a beat's time.
  fun float getAttackFactor()
  {
    return myAttackFactor;
  }

  fun void setAttackFactor(float factor)
  {
    factor => myAttackFactor;
  }

  // The Release portion of a beat's time.
  fun float getReleaseFactor()
  {
    return myReleaseFactor;
  }

  fun void setReleaseFactor(float factor)
  {
    factor => myReleaseFactor;
  }
}

//
// Ends.
//
