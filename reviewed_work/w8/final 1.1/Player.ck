// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// A (instrument-) player is an object capable of playing musical
// lines or chords in a particular playing style.
public class Player
{
  Instrument   myInstrument;
  PlayingStyle myPlayingStyle;

  fun void initialise(Instrument instr, PlayingStyle style)
  {
    style @=> myPlayingStyle;
    instr @=> myInstrument;
  }

  // Play a given chord on the given instrument.
  fun void playChord(float chordNotes[])
  {
    BPM.beat => dur beat;

    // The first array element is always the number of beats.
    chordNotes[0] => float beats;

    int i;
    int voiceNumber;
    int note;
    for (1 => i; i < chordNotes.cap() - 1; i++)
    {
      // Play all but one of the chord notes concurrently. 
      // There may be a tiny amount of time between the 
      // start of each chord note but for most purposes 
      // this should not be noticeable.

      chordNotes[i] $ int => note;
      i-1 => voiceNumber;

      if (chordNotes[i] == Note.SKIP)
      {
        // Do nothing.
      }
      else
      {
        // Play the chosen note on voice i of the instrument.
        chordNotes[i] $ int => int note;
        spork ~ myInstrument.play(voiceNumber, myPlayingStyle, note, beats);
      }
    }

    // Play the final note in this context in order to
    // allow the correct amount of time to pass here also.
    chordNotes[i] $ int => note;
    i-1 => voiceNumber;
    if (chordNotes[i] == Note.SKIP)
    {
      // Nothing to play in this context so we just 
      // wait instead.
      beats::beat => now;
    }
    else
    {
      // Play the chosen note on voice i of the instrument.
      myInstrument.play(voiceNumber, myPlayingStyle, note, beats);
    }
  }

  fun void playLine(float phrase[][])
  {
    for (0 => int i; i < phrase.cap(); i++)
    {
      playChord(phrase[i]);
    }
  }
}

//
// Ends.
//
