// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// An instrument comprised of an arbitrary number of STK models (e.g., six Mandolins).
public class ModelledInstrument extends Instrument
{
  StkInstrument myInstrs[];
  ADSR          myEnvelopes[];

  fun void initialise(StkInstrument instrs[], ADSR envs[])
  {
    instrs @=> myInstrs;
    envs   @=> myEnvelopes;
  }

  // Play a note on a given voice using the given style for the
  // given number of beats.
  fun void play(int voice, PlayingStyle style, int note, float beats)
  {
    BPM.beat => dur beat;

    // Attack-Decay-Sustain phase.
    if (note == Note.REST)
    {
      // We'll use 0 Hz for rests, i.e., silence.
      0 => myInstrs[voice].freq;
    }
    else
    {
      Std.mtof(note) => myInstrs[voice].freq;
    }

    style.getVelocity() => myInstrs[voice].noteOn;
    1   => myEnvelopes[voice].keyOn;
    style.getAttackFactor() * beats::beat => now;
    
    // Release phase.
    1.0 => myInstrs[voice].noteOff;
    1   => myEnvelopes[voice].keyOff;
    style.getReleaseFactor() * beats::beat => now;
  }
}

//
// Ends.
//
