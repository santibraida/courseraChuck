// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// A class representing valid MIDI notes for use in our composition
// plus "special" values to be interpreted as control commands.
public class Note
{
  128 => static int REST; // a.k.a. "silence the current sound generator"
  129 => static int SKIP; // "do not disturb the current sound generator"
  130 => static int PLAY; // used with SndBufs as a generic "play now" value.

  00   => static int C0;  // "C of octave 0"
  02   => static int D0;
  04   => static int E0;
  05   => static int F0;
  07   => static int G0;
  09   => static int A0;
  11   => static int B0;

  12   => static int C1;
  14   => static int D1;
  16   => static int E1;
  17   => static int F1;
  19   => static int G1;
  21   => static int A1;
  23   => static int B1;

  24   => static int C2;
  26   => static int D2;
  28   => static int E2;
  29   => static int F2;
  31   => static int G2;
  33   => static int A2;
  35   => static int B2;

  36   => static int C3;
  38   => static int D3;
  40   => static int E3;
  41   => static int F3;
  43   => static int G3;
  44   => static int Ab3; 
  45   => static int A3;
  46   => static int Bb3; 
  47   => static int B3;

  48   => static int C4;
  49   => static int Db4;
  50   => static int D4;
  51   => static int Eb4; 
  52   => static int E4;
  53   => static int F4;
  54   => static int Gb4;
  55   => static int G4;
  56   => static int Ab4; 
  57   => static int A4;
  58   => static int Bb4; 
  59   => static int B4;

  60   => static int C5;
  61   => static int Db5;
  62   => static int D5;
  63   => static int Eb5; 
  64   => static int E5;
  65   => static int F5;
  66   => static int Gb5; 
  67   => static int G5;
  68   => static int Ab5; 
  69   => static int A5;
  70   => static int Bb5; 
  71   => static int B5;

  72   => static int C6;
  73   => static int Db6;
  74   => static int D6;
  75   => static int Eb6;
  76   => static int E6;
  77   => static int F6;
  78   => static int Gb6;
  79   => static int G6;
  80   => static int Ab6;
  81   => static int A6;
  82   => static int Bb6;
  83   => static int B6;

  84   => static int C7;
  85   => static int Db7;
  86   => static int D7;
  87   => static int Eb7;
  88   => static int E7;
  89   => static int F7;
  91   => static int G7;
  93   => static int A7;
  95   => static int B7;

  96   => static int C8;
  98   => static int D8;
  100  => static int E8;
  101  => static int F8;
  103  => static int G8;
  105  => static int A8;
  107  => static int B8;

  108  => static int C9;
  109  => static int Db9;
  110  => static int D9;
  111  => static int Eb9;
  112  => static int E9;
  113  => static int F9;
  114  => static int Gb9;
  115  => static int G9;
  116  => static int Ab9;
  117  => static int A9;
  118  => static int Bb9;
  119  => static int B9;

  120  => static int C10;
  121  => static int Db10;
  122  => static int D10;
  123  => static int Eb10;
  124  => static int E10;
  125  => static int F10;
  126  => static int Gb10;
  127  => static int G10;
}

// N.B., we need to instantiate a 'dummy' Note instance
// in order to force initialisation of the static members.
// (ChucK idiosyncrasy).
Note dummy;


//
// Ends.
//
