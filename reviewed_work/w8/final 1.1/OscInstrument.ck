// Assignment_Final_The_Minute_Chuck
// Course: Introduction to Programming for Musicians and Digital Artists
// By Ajay Kapur with Perry Cook and Ge Wang
// October 2013.
// Assignment 8: Final Project
//

// An instrument comprised of an arbitrary number of oscillators.
public class OscInstrument extends Instrument
{
  Osc          myOscs[];
  ADSR         myEnvelopes[];

  fun void initialise(Osc oscs[], ADSR envs[])
  {
    oscs   @=> myOscs;
    envs   @=> myEnvelopes;
  }

  // Play a note on a given voice using the given style for the
  // given number of beats.
  fun void play(int voice, PlayingStyle style, int note, float beats)
  {
    BPM.beat => dur beat;

    if (note == Note.REST)
    {
      // We'll use 0 Hz for rests, i.e., silence.
      0 => myOscs[voice].freq;
    }
    else
    {
      Std.mtof(note) => myOscs[voice].freq;
    }

    // Attack-Decay-Sustain phase.
    1 => myEnvelopes[voice].keyOn;
    style.getAttackFactor() * beats::beat => now;

    // Release phase.
    1 => myEnvelopes[voice].keyOff;
    style.getReleaseFactor() * beats::beat => now;
  }
}

//
// Ends.
//
