// Assignment 8: The End

// main melody A - lower tones

// load classes
BPM tempo;
mainVolume vol;

// local class: piano-like impulse
class ImpPiano 
{
    // sound chain
    Impulse impulse => ResonZ filt => PRCRev irev => Gain impulseGain => dac;
    impulseGain => Gain impulseFeedback => Delay delay => impulseGain; // delay line for the impulses
    
    // default settings
    100.0 => filt.Q;
    1000.0 => filt.freq;
    0.5 => irev.mix;
    tempo.eighthNote => delay.max;
    tempo.sixteenthNote => delay.delay;
    0.5 => impulseFeedback.gain;
    
    // methods
    fun void freq( float freq )
    {
        4*freq => filt.freq;
    }    
    
    fun void freq( int midiNum )
    {
        Std.mtof(midiNum+24) => filt.freq;
    } 
    
    fun void setQ( float Q )
    {
        Q => filt.Q;
    }
    
    fun void noteOn( float volume )
    {
        volume => impulse.next;
    }
    
    fun void setGain( float volume )
    {
        volume => impulseGain.gain;
    }
    
    fun void reverb( float mix )
    {
        mix => irev.mix;
    }
    
    fun void feedback( float amount )
    {
        amount => impulseFeedback.gain;
    }
}

// sound chain
Mandolin mand => JCRev mrev => dac;
ImpPiano imp;

// parameters
vol.set(0.15) => mand.gain;
vol.set(0.6) => imp.setGain;
0.7 => imp.reverb;
0.8 => imp.feedback;
400 => imp.setQ;

// scale
[49,50,52,54,0] @=> int MelA[];

// functions
// function 1: generates (almost) random MIDI melody pattern
fun int[] melody()
{
    int Mel2[16];
    for( 0 => int i; i < Mel2.cap(); i++ )
    {
        Math.random2(0, 4) => int a;
        MelA[a] => Mel2[i]; // make an array consisting of MIDI numbers from MelA and sometimes 0s -> these will be the pauses
    }
    
    return Mel2;
    
}

// function 2: generates random pattern of the impPiano "strenghtening" the mandolin
fun int[] impulseFun( string intensivity ) // 'heavy' or 'light'; if heavy, the piano will play more often
{
    int impPtrn[16];
    if( intensivity == "heavy" )
    {
        for( 0 => int i; i < impPtrn.cap(); i++ )
        {
            Math.random2(0,3) => impPtrn[i]; 
            /* I used random number from 0 to 3 because I wanted the impulses to occur 
            more often; the "section" function will play an impulse every time there is 
            something equal to or more than 1, so there is 75% chance of an impulse 
            occuring */
        }
    }
    else if( intensivity == "light" )
    {
        for( 0 => int i; i < impPtrn.cap(); i+2 => i )
        {
            Math.random2(0,3) => impPtrn[i];
        }
    }
    else
    {
        <<< "Enter 'light' or 'heavy' while using the impulseFun function" >>>;
    }
    return impPtrn;
}

// generate arrays (two melodies and a light piano pattern)
melody() @=> int Melody1[];
melody() @=> int Melody2[];
impulseFun( "light" ) @=> int pianoArray[];

// the loop (play the two melodies alternately)

while( true )
{
    for( 0 => int i; i < Melody1.cap(); i++ )
    {
        if( Melody1[i] != 0 )
        {
            Std.mtof(Melody1[i]) => mand.freq;
            mand.noteOn(Math.random2f(.5,.7));
            if( pianoArray[i] >= 1 )
            {
                Melody1[i]+12 => imp.freq;
                400 => imp.noteOn;
            }
        }
        tempo.eighthNote => now;
        vol.set(0.15) => mand.gain;
        vol.set(0.6) => imp.setGain;
    }
    for( 0 => int i; i < Melody2.cap(); i++ )
    {
        if( Melody2[i] != 0 )
        {
            Std.mtof(Melody2[i]) => mand.freq;
            mand.noteOn(Math.random2f(.5,.7));
            if( pianoArray[i] >= 1 )
            {
                Melody2[i]+12 => imp.freq;
                200 => imp.noteOn;
            }
        }
        tempo.eighthNote => now;
        vol.set(0.15) => mand.gain;
        vol.set(0.6) => imp.setGain;
    }
}