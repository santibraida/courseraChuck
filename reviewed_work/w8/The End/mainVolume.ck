// Assignment 8: The End

// mainVolume class
// global volume Class - simple but making it very easy to control the global volume in the score.ck file
public class mainVolume
{
    // set default mainVolume.factor()
    1 => static float factor;
    
    // make a function 
    
    fun float set( float level )
    {
        this.factor*level => float factored;
        return factored;
    }
}
