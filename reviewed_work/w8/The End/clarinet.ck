// Assignment 8: The End

// clarinet

// load classes
BPM tempo;
mainVolume vol;

// sound chain
Clarinet clar => JCRev crev => Gain clarGain => dac;

// parameters
vol.set(0.03) => clarGain.gain;
0.8 => crev.mix;

// clarinet melody array
[49,54,52,50] @=> int clarArray[];

// the loop

while( true )
{
    for( 0 => int i; i < clarArray.cap(); i++ )
    {
        Std.mtof(clarArray[i]) => clar.freq;
        1 => clar.noteOn;
        for( 0 => int j; j < 8; j++ )
        {
            vol.set(0.03) => clarGain.gain;
            tempo.sixteenthNote => now;
        }
    }
}