// Assignment 8: The End

// the score

mainVolume vol;
BPM tempo;
tempo.rate(80.0);

<<< "Assignment 8: The End" >>>;

fun void fadeout( dur lenghth )
{
    0::second => dur current;
    while( current < lenghth )
    {
        current + 10::ms => current;
        10::ms => now;
    }
    while( vol.factor >= 0 ) // fading out
    {
        vol.factor - 0.01 => vol.factor;
        60::ms => now;
    }
}

spork ~ fadeout( 57::second );

Machine.add(me.dir()+"/melodyA.ck") => int melodyID;
Machine.add(me.dir()+"/drums.ck") => int drumsID;
Machine.add(me.dir()+"/clarinet.ck") => int clarinetID;
16.0 * tempo.quarterNote => now;

Machine.add(me.dir()+"/shakers.ck") => int shakersID;
16.0 * tempo.quarterNote => now;

Machine.replace(melodyID, me.dir()+"/melodyB.ck");
32.0 * tempo.quarterNote => now;

Machine.replace(melodyID, me.dir()+"/melodyA.ck");
24.0 * tempo.quarterNote => now;

Machine.remove(melodyID);
Machine.remove(clarinetID);
Machine.remove(shakersID);
Machine.remove(drumsID);
<<< "Thank you for listening!" >>>;




