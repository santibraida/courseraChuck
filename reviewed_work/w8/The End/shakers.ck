// Assignment 8: The End

// shakers

// load classes
BPM tempo;
mainVolume vol;

// sound chain
Shakers shaker => PRCRev srev => Pan2 p => Gain shakerGain => dac;

// parameters
vol.set(0.4) => shakerGain.gain;
0.7 => srev.mix;

// function: generates (almost) random shaker pattern
fun int[] shakerFun( string beattype ) // beattype: "onbeat" ("on the kick") or "afterbeat" ("between the kick and the clap"); sets the overall style of the shaker pattern
{
    int Ptrn2[16];
    if( beattype == "onbeat" )
    {
        [1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0] @=> int Ptrn[]; // the onbeat "hardcoded" hits
        for( 0 => int i; i < Ptrn.cap(); i++ )
        {
            if( i != 0 && i != 4 && i != 8 && i != 12 )
            {
                Math.random2(0, 1) => Ptrn2[i]; // random hits between the onbeats
            } 
            else
            {
                1 => Ptrn2[i];
            }
        }
    }
    else if( beattype == "afterbeat" ) // the afterbeat "hardcoded" hits
    {
        [0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0] @=> int Ptrn[];
        for( 0 => int i; i < Ptrn.cap(); i++ )
        {
            if( i != 2 && i != 6 && i != 10 && i != 14 )
            {
                Math.random2(0, 1) => Ptrn2[i]; // random hits between the afterbeats
            } 
            else
            {
                1 => Ptrn2[i];
            }
        }
    }
    else
    {
        <<< "Enter 'onbeat' or 'afterbeat' while using the shakerFun function" >>>;
    }
    return Ptrn2;  
}

// generate two shaker patterns: an onbeat and an afterbeat one
shakerFun( "onbeat" ) @=> int shakOn[];
shakerFun( "afterbeat" ) @=> int shakAft[];

// the loop
// play the two patterns alternately
while( true )
{
    for( 0 => int i; i < shakOn.cap(); i++ )
    {
        if( shakOn[i] == 1 )
        {
            Math.random2(0, 3) => shaker.preset;
            Math.random2f(80, 128) => shaker.objects;
            Math.random2f(.8, 1) => shaker.decay;
            Math.random2f(.4,.8) => shaker.gain; // random, subtle gain 
            Math.random2f(-0.8,.8) => p.pan; // random pan
            shaker.noteOn(Math.random2f(0, 4));
        }
        tempo.eighthNote => now;
        vol.set(0.4) => shakerGain.gain;
    }
    for( 0 => int i; i < shakAft.cap(); i++ )
    {
        if( shakAft[i] == 1 )
        {
            Math.random2(0, 3) => shaker.preset;
            Math.random2f(80, 128) => shaker.objects;
            Math.random2f(.8, 1) => shaker.decay;
            vol.set(Math.random2f(.4,.8)) => shaker.gain;
            Math.random2f(-0.8,.8) => p.pan;
            shaker.noteOn(Math.random2f(0, 4));
        }
        tempo.eighthNote => now;
        vol.set(0.4) => shakerGain.gain;
    }
}