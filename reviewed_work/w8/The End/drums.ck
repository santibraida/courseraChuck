// Assignment 8: The End

// drums (kick&clap)

// load classes
BPM tempo;
mainVolume vol;

// sound chain
SndBuf kick => dac;
SndBuf clap => dac;

// load soundfiles
me.dir(-1) + "/audio/kick_04.wav" => kick.read;
me.dir(-1) + "/audio/clap_01.wav" => clap.read;

// set samples playheads to end
kick.samples() => kick.pos;
clap.samples() => clap.pos;

// volumes
vol.set(0.3) => kick.gain;
vol.set(0.3) => clap.gain;

// functions

// fun 1: kicks
fun void kicks()
{
    while( true )
    {
        for( 0 => int i; i < 16; i++ )
        {
            if( i%4 == 0 ) 
            {
                vol.set(0.3) => kick.gain;
                0 => kick.pos;
            }
            else if( i == 15 ) 
            {
                vol.set(0.15) => kick.gain;
                0 => kick.pos;
            }
            tempo.eighthNote => now;
        }
                
    }
}

// fun 2: claps
fun void claps()
{
    while( true )
    {
        for( 0 => int i; i < 2; i++ )
        {
            if( i == 1 ) 
            {
                vol.set(0.3) => clap.gain;
                0 => clap.pos;
            }
            2*tempo.quarterNote => now;
        }
                
    }
}

spork ~ kicks(); // sporking the functions
spork ~ claps();

while( true ) // keeping the parent alive!
{
    1::second => now;
}