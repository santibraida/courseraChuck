//assignement_final_never_knows
// bss.ck

Mandolin bass => NRev r => Pan2 bass_pan => dac;
BPM tempo;
// mldy
[48, 51, 54, 57, 54, 57, 60] @=> int scale[]; 

// parameter setup
0.01 => r.mix;
0.0 => bass.stringDamping;
0.03 => bass.stringDetune;
0.05 => bass.bodySize;
4 => int walkPos;
.8 => bass.gain;
-0.5 => bass_pan.pan;

// loop
while( true )  
{
    tempo.quarterNote => dur quarter;
    Math.random2(-3,3) +=> walkPos; 
    if (walkPos < 0) 1 => walkPos;
    if (walkPos >= scale.cap()) scale.cap()-4 => walkPos;
    Std.mtof(scale[walkPos]-24) => bass.freq;
    Math.random2f(0.05,0.1) => bass.pluckPos;
    .5 => bass.noteOn;
    
    quarter/2/*(Math.random2(4, 8)))*/ => now;
    1 => bass.noteOff;
    //Math.random2((15/2), (15/4)) :: ms => now;
}


