//assignement_final_never_knows
// score.ck

BPM tempo;
tempo.tempo(95.0);

me.dir() + "/stif.ck" => string stifkPath;
me.dir() + "/drmz2.ck" => string drmz2Path;
me.dir() + "/moo.ck" => string moogPath;
me.dir() + "/bss.ck" => string bassPath;
me.dir() + "/rhod.ck" => string rhPath;
me.dir() + "/clar.ck" => string clarPath;

Machine.add(stifkPath) => int stID;

5.0::second => now;

Machine.add(moogPath) => int moogID;
Machine.add(drmz2Path) => int drmzID;
Machine.add(bassPath) => int bassID;

18::second => now;

Machine.add(rhPath) => int rhID;

5.0::second => now;
Machine.add(clarPath) => int clarID;

Machine.remove(rhID);

10.0::second => now;

Machine.add(rhPath) => int rhID2;

10.0::second => now;

Machine.remove(stID);
Machine.remove(rhID2);
Machine.add(clarPath) => int clarID2;

(5-.625*2)::second => now;

Machine.remove(drmzID);

(.625*2)::second => now;

Machine.remove(bassID);
Machine.add(clarPath) => int clarID3;
Machine.remove(moogID);

2.0::second => now;
Machine.add(clarPath) => int clarID4;

2.0::second => now;
Machine.add(clarPath) => int clarID5;
8.0::second => now;

Machine.remove(clarID5);




