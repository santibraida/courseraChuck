//assignement_final_never_knows
//drmz2.ck

SndBuf snare => NRev rev => Gain g => dac;
SndBuf kick => Pan2 kick_pan => dac;
SndBuf hihat => Pan2 hh_pan => dac;
 
// parameter setup
.6 => hihat.gain;
-0.5 => kick_pan.pan;
0.5 => hh_pan.pan;
.04 => rev.mix;
.59 => g.gain;
.7 => kick.gain;
BPM tempo;
//FFade f;
//f.fade( 25 );

me.dir(-1) + "/audio/hihat_04.wav" => hihat.read;
me.dir(-1) + "/audio/kick_03.wav" => kick.read;
me.dir(-1) + "/audio/snare_03.wav" => snare.read;

kick.samples() => kick.pos;
snare.samples() => snare.pos;

//global vars
[1,0,1,0,1,1,1,0,1,0] @=> int kick_ptrn_1[];
[0,1,0,1,0,0,0,1,0,1] @=> int kick_ptrn_2[];
[0,1,0,1,0,0,0,1,0,1] @=> int snare_ptrn_1[];
[1,0,1,0,1,0,1,0,1,0] @=> int snare_ptrn_2[];

//funcs
fun void drm( int kickArray[], int snareArray[], dur beattime ) {
    for( 0=> int i; i < kickArray.cap(); i++ ) {
        if( kickArray[i] == 1 ) {
            0 => kick.pos;
        }
        if( snareArray[i] == 1 ) {
            0 => snare.pos;
        }
     beattime => now;
    }
}

fun void hh() {
    while( true )  
{
    tempo.eighthNote => dur eighth;
    Math.random2f(0.3,.6) => hihat.gain;
    Math.random2f(.95,1.0) => hihat.rate;
    (eighth*(Math.random2(1,2))) => now;
    0 => hihat.pos;
}
}


//main
spork ~ hh();

while( true ) {
    tempo.eighthNote => dur eighth;
    drm( kick_ptrn_1, snare_ptrn_1, eighth/2 );
    drm( kick_ptrn_2, snare_ptrn_2, eighth/2 );
    drm( kick_ptrn_1, snare_ptrn_1, eighth/2 );
}



