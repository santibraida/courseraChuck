//assignement_final_never_knows
//moo.ck

SinOsc s => Gain sg => Pan2 sp => dac;
Moog moo => Gain master => Pan2 moop => dac;
.15 => sg.gain;
.15 => master.gain;
0.5 => moop.pan;
1.0 => sp.pan;

//scale
[48, 51, 54, 57, 54, 57, 60] @=> int mscale[]; 

BPM tempo;
Chord ch;


//loop
while(true) {
    tempo.quarterNote => dur quarter;
    for(0 => int c; c < mscale.cap(); c++) { 
    Std.mtof(mscale[c]) => moo.freq => s.freq;
        ch.playChord( mscale[c]*2, quarter );
        1 => moo.noteOn;
    quarter*2 => now;
        1 => moo.noteOff;
        -1.0*moop.pan() => moop.pan;
    }
}