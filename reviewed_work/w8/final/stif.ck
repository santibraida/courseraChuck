//assignement_final_never_knows
// stif.ck

StifKarp sit => NRev r => Pan2 span => dac;

// melody
[48, 51, 54, 57, 60, 63, 66, 69] @=> int melody[]; 

// parameter setup
Math.random2f( 0.15, 0.25 ) => r.mix;
4 => int walkPos;
.29 => sit.gain;
0.2 => span.pan;

BPM tempo;
Chord ch;
// loop
while( true )  
{
    tempo.eighthNote => dur eighth;
    Math.random2(-1,1) +=> walkPos; 
    if (walkPos < 0) 1 => walkPos;
    if (walkPos >= melody.cap()) melody.cap()-4 => walkPos;
    Std.mtof(melody[walkPos]) => sit.freq;
    ch.playChord ( melody[walkPos], eighth );
    1 => sit.noteOn;
    
    (eighth/(Math.random2(3,5))) => now;
    1 => sit.noteOff;
    //(eighth/(Math.random2(1,3))) => now;
}


