//assignement_final_never_knows
//clar.ck

Pan2 p2 => dac;
Clarinet clar => NRev rev3 => p2 => dac;
-.5 => p2.pan;
.6 => clar.gain;
[48, 51, 54, 57, 54, 57, 60] @=> int tryg2[];

for( 0 => int d; d < (tryg2.cap() - 1)*10; d++ ) {
    1 => clar.noteOff;
    .4 => clar.noteOn;
    Std.mtof( tryg2[Math.random2( 0, tryg2.cap() - 1)] ) => clar.freq;
    100::ms => now;
    (clar.gain()*.9) => clar.gain;
    p2.pan() * -1 => p2.pan;
}
