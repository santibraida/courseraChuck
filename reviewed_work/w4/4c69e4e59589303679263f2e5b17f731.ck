<<< "Assignment 4 - Fun" >>>;

// Mixer setup
Gain master => dac;

SinOsc s => master;
0.3 => s.gain;

SndBuf kick => Pan2 kpan => master;
me.dir() + "/audio/kick_01.wav" => kick.read;
kick.samples() => kick.pos;

SndBuf clap => Pan2 cpan => master;
me.dir() + "/audio/clap_01.wav" => clap.read;
clap.samples() => clap.pos;
0.2 => cpan.pan;

SndBuf hihat => Pan2 hpan => master;
me.dir() + "/audio/hihat_02.wav" => hihat.read;
hihat.samples() => hihat.pos;
0.4 => hihat.gain;
-0.2 => hpan.pan;

// setup
[51, 53, 55, 56, 58, 60, 61, 63] @=> int notes[];
0.6::second => dur pulse;

// section 1
[0, 3, 5, 2, 6, 5, 4, 3] @=> int melody1[];
[1, 0, 0, 0, 1, 0, 0, 0] @=> int kick1[];
[0, 0, 1, 0, 0, 0, 1, 0] @=> int clap1[];
[1, 1, 1, 1, 1, 1, 1, 1] @=> int hats1[];

// section 2
[0, 3, 5, 2, 6, 5, 4, 4] @=> int melody2[];
[1, 1, 0, 0, 1, 1, 0, 1] @=> int kick2[];
[0, 0, 1, 0, 0, 0, 1, 0] @=> int clap2[];
[0, 1, 0, 1, 0, 1, 0, 1] @=> int hats2[];

// section 3
[0, 0, 0, 0, 5, 5, 5, 5] @=> int melody3[];
[1, 1, 0, 0, 0, 0, 0, 1] @=> int kick3[];
[0, 0, 1, 1, 1, 0, 1, 1] @=> int clap3[];
[0, 0, 0, 0, 0, 0, 0, 0] @=> int hats3[];

fun void play_beat(int beat, int k[], int c[], int h[]) {
    if(k[beat] == 1) {
        0 => kick.pos;
    }
    if(c[beat] == 1) {
        0 => clap.pos;
    }
    if(h[beat] == 1) {
        0 => hihat.pos;
    }
}

fun void play_melody(int beat, int m[], int transpose) {
    Std.mtof(notes[m[beat]] + 12 * transpose) => s.freq;
}

now + pulse * 50 => time end;

fun void play(int k[], int c[], int h[], int m[], int transpose) {
    for(0 => int i; i < 8; i++) {
        play_beat(i, k, c, h);
        play_melody(i, m, transpose);
        pulse / 2 => now;
    }
}

// MAIN
while(now < end) {
    play(kick1, clap1, hats1, melody1, 0);
    play(kick2, clap2, hats2, melody2, 0);
    play(kick1, clap1, hats1, melody1, 1);
    play(kick3, clap3, hats3, melody3, 0);
}