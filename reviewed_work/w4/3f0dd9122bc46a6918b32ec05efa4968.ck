<<< "Assignment_4_HavingFun" >>>;

// Duration
0.6::second => dur quarter;

// D Dorian scale array
[50, 52, 53, 55, 57, 59, 60, 62] @=> int notes[];

// Melodies
[3,4,2,-1,1,6,0,-1,  3,-1,7,-1,3,-1,0,-1] @=> int melody1[]; // Rythm GOOD
[2,3,4,-1,2,1,0,-1,  2,3,4,-1,5,6,7,-1] @=> int melody2[]; // Medium NOT GOOD
[3,4,2,-1,1,6,0,-1,  3,4,2,-1,1,6,0,-1] @=> int melody3[]; // Fast NOT TOO GOOD
[5,-1,5,-1,5,-1,5,-1,  4,-1,5,-1,6,-1,7,-1] @=> int melody4[]; // Medium GOOD
[7,-1,6,-1,5,-1,4,-1,  7,-1,6,-1,5,-1,4,-1] @=> int melody5[]; // Fast GOOD

Gain gain => dac;

// Directory of audio files
me.dir() + "/audio/" => string DIR;
// Array with filenames
["kick_04.wav","kick_05.wav"] @=> string kicks[];
["snare_01.wav","snare_03.wav"] @=> string snares[];
// Sound Buffer
Pan2 kickPan => gain;
SndBuf snareOne => kickPan;
SndBuf snareTwo => kickPan;
SndBuf kickOne => kickPan;
SndBuf kickTwo => kickPan;
DIR + kicks[0] => kickOne.read;
DIR + kicks[1] => kickTwo.read;
DIR + snares[0] => snareOne.read;
DIR + snares[1] => snareTwo.read;
// Not initial sound
kickOne.samples() => kickOne.pos;
kickTwo.samples() => kickTwo.pos;
snareOne.samples() => snareOne.pos;
snareTwo.samples() => snareTwo.pos;

// Oscilators
0.1 => float MAX_GAIN;
Pan2 oscPan => gain;
SawOsc s1 => oscPan;
SawOsc s2 => oscPan;
TriOsc t1 => oscPan;
// Not initial sound
0 => s1.gain;
0 => s2.gain;
0 => t1.gain;

0 => int i;
0 => int q;
0 => int loops;
1 => int delta;
50 => int deltaMax;
0 => gain.gain;

while (true) {
    <<< "loops " + loops >>>;

    // Increases volume
    if (loops <= 64) {
        (gain.gain() + 0.015625) => gain.gain;
    }
    // Decreases volume
    if (loops > 368) {
        (gain.gain() - 0.03125) => gain.gain;
    }
    
    shufflePanning();
    
    // Kick
    if (loops >= 64 && q % 8 == 0) {
        0 => kickOne.pos;
    }
    // Snare
    if (loops >= 128 && q % 8 == 3) {
        0 => snareOne.pos;
    }
    q++;
    
    getMelody(loops, "A") @=> int mA[];
    getMelody(loops, "B") @=> int mB[];
    // Prevents i grater than melody capacity
    if (i >= mA.cap()) {
        0 => i;
    }
    mA[i] => int indexA;
    mB[i] => int indexB;
    handleSawFrequencyGain(indexA, s1);
    handleSawFrequencyGain(indexB, s2);
    i++;
    
    if (loops >= 256) {
        handleTriFrequencyGain(t1);
    }
    
    // Delta handler
    delta++;
    if (delta > deltaMax) {
        1 => delta;
    }
    
    quarter/8 => now;
    loops++;
}

function void handleSawFrequencyGain(int index, SawOsc s) {
    if (index != -1) {
        MAX_GAIN => s.gain;
        Std.mtof( notes[index] + (12 / (1*delta) ) ) => s.freq;
    } else {
        0 => s.gain;
    }
}

function void handleTriFrequencyGain(TriOsc t) {
    0.2 => t.gain;
    Std.mtof( notes[Math.random2(0,notes.cap()-1)] + (12 / (1*delta) ) ) => t.freq;
}

function void shufflePanning() {
    if (kickPan.pan() == 1) {
        -1 => kickPan.pan;
        1  => oscPan.pan;
    } else {
        1  => kickPan.pan;
        -1 => oscPan.pan;
    }
}

function int[] getMelody(int loop, string type) {
    if (loop < 64) {
        if (type == "A") {
            return melody5;
        } else if (type == "B") {
            return melody3;
        }
    } else if (loop >= 64 && loop < 128) {
        if (type == "A") {
            return melody3;
        } else if (type == "B") {
            return melody2;
        }
    } else if (loop >= 128 && loop < 192) {
        if (type == "A") {
            return melody2;
        } else if (type == "B") {
            return melody1;
        }
    } else if (loop >= 192 && loop < 256) {
        if (type == "A") {
            return melody1;
        } else if (type == "B") {
            return melody5;
        }
    } else if (loop >= 256 && loop < 320) {
        if (type == "A") {
            return melody4;
        } else if (type == "B") {
            return melody1;
        }
    } else if (loop >= 320 && loop < 384) {
        if (type == "A") {
            return melody1;
        } else if (type == "B") {
            return melody5;
        }
    } else if (loop >= 384 && loop < 400) {
        if (type == "A") {
            return melody3;
        } else if (type == "B") {
            return melody2;
        }
    }
}
