///sound chain
<<<"Assigment 4 30 seconds to kick">>>;
SinOsc s=>dac;
[51, 53, 55, 56, 58, 60, 61, 63 ]@=>int a[];

SndBuf kick=>dac;
SndBuf click=>dac;
SndBuf snare=>dac;
me.dir()+"/audio/kick_02.wav"=>kick.read;
me.dir()+"/audio/click_05.wav"=>click.read;
me.dir()+"/audio/snare_03.wav"=>snare.read;
///pattern

[0,1,1,0,1,0,1,0]@=> int click_ptrn_1[];
[1,1,0,1,1,0,1,0]@=> int click_ptrn_2[];
[1,1,1,0,1,0,1,0]@=> int kick_ptrn_1[];
[0,1,1,0,1,0,0,0]@=> int kick_ptrn_2[];
[0,1,0,1,0,0,1,0]@=> int snare_ptrn_1[];
[0,1,1,0,1,0,1,0]@=> int snare_ptrn_2[];

//FUNCTIONS
///function section
fun void section(int kickarray[],int clickarray[],int snarearray[],float beattime)
{
    for (0=>int i;i<kickarray.cap();i++)
    {
        if(kickarray[i]==1)
        {
            0=>kick.pos;
        }
        if ( clickarray[i]==1)
        {
            0=>click.pos;
        }
        if (snarearray[i]==1)
        {
            0=>snare.pos;
        }
        beattime::second=>now;
    }
}
 //returns a ramdom float   

//changes the sound of Sound
fun void swell (float begin, float end, float grain)
{
    for (begin => float j; j<end; j+grain=>j)
    {
        j=>s.gain;
        .01::second=>now;
    }
    
}
fun void Final_sound(float rithym,int tempo)
{
    for (0=> int i;i<tempo;i++)
    {
        0=>snare.pos;
        rithym::ms=>now;
     }
}

//Main program  
for(0=>int beat;beat<20;beat++)
{
    0=>s.gain;
    if (beat%5 == 0)
    {
        Math.mtof(a[beat%8])=>s.freq;///set frequency(MIDI)
        swell(.2,0.4,.01);
    }
    if(beat%2==1){
        section(kick_ptrn_1,click_ptrn_1,snare_ptrn_1,.1);
    }
    else
    {
        section(kick_ptrn_2,click_ptrn_1,snare_ptrn_2,.1);
    }
    0.6::second=>now;///advance time(QUARTER NOTE)
        
}
Final_sound(80.0, 20);