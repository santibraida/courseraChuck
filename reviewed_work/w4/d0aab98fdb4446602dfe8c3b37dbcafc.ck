
//==============
// Sound network
//==============

Gain master => dac;

SinOsc s => Pan2 p => master;
TriOsc t => master;
SqrOsc q => p => master;
SawOsc w => master;

SndBuf kick => master;
SndBuf hihat => master;
SndBuf snare => master;

int prevNote, nextNote, delta;

0.3 => master.gain;

//Math.random2(0,23) => prevNote; //find a random note
15 => prevNote; //Ensure we start the composition from somewhere in the middle of the notes array

//Read the audio samples for percussion
me.dir() + "/audio/kick_01.wav" => kick.read;
me.dir() + "/audio/hihat_01.wav" => hihat.read;
me.dir() + "/audio/snare_01.wav" => snare.read;
kick.samples() => kick.pos;
hihat.samples() => hihat.pos;
snare.samples() => int numSnareSamples;

//=============================
//Initialize the notes to play 
//=============================
float notes[24];
fun void initialize_notes()
{
    [51, 53, 55, 56, 58, 60, 61, 63] @=> int scale[];
    for (0=>int i; i<24; i++) //Generate frequencies for 3 octaves of the scale
    {
        Std.mtof(scale[i%8]) *((i/8) $ int + 1) => notes[i];
        <<< notes[i] >>>;
    }
}

//========================================================
//Generate the next note randomly, given the previous note
//========================================================
fun int generate_next_note(int previous_note)
{
    //Make sure next note is less than 3 notes away from previous note
    Math.random2(-3, 3) => int delta;
    
    previous_note + delta => int nextNote;
    if (nextNote > 23)
        previous_note - delta => nextNote;
    if (nextNote < 0)
        previous_note - delta => nextNote;
    return (nextNote);
}

//==============================================
//Play the beats to be played in this iteration
//==============================================
fun void play_beats(int iteration)
{
    iteration % 8 => int beat;
    //Play bass
    //if ((beat == 0) || (beat == 4))
    if (beat % 2 == 0)
    {
       (iteration % 8)*0.1 => kick.gain;
       Math.random2f(0.1, 0.3) => kick.rate; //set rate 
       0 => kick.pos;         
    }
    //Play hihat 
    if ((beat == 0) || (beat == 4))
    {
        (iteration % 8)*0.05 => hihat.gain;
        Math.random2f(0.1, 0.2) => hihat.rate; //set rate 
        0 => hihat.pos;
    }
    //Play snare
    if (beat % 2 == 1)
    {
       (iteration % 8)*0.05 => snare.gain;
       Math.random2f(0.1, 0.3) => snare.rate; //set rate 
       0 => snare.pos;  
    }
}

//==============================================
//Play the notes to be played in this iteration
//==============================================
fun void play_notes(int iteration)
{

    generate_next_note(prevNote) => nextNote;
        
    //Print out the previous note, next note and the notes that each osc will play for debugging
    <<< prevNote, nextNote >>>;   
       
    //Play the sine osc   
    nextNote => int sNote ;
    notes[sNote] => s.freq; //set frequency for sine osc
    0.3 => s.gain;//set gain for sine osc

 
    //Play the square osc
    nextNote + 4 => int qNote;
    if (qNote > 23) 
        nextNote - 4 => qNote;
    notes[qNote] => q.freq;
    0.2 => q.gain;
    
    //Play the triangle osc
    nextNote + 4 => int tNote;
    if (tNote > 23) 
        nextNote - 4 => tNote;
    notes[tNote] => t.freq;
    0.05 => t.gain;
    
    //Play the sawtooth osc
    nextNote - 4 => int wNote;
    if (wNote < 0) 
        nextNote + 4 => wNote;
    notes[wNote] => w.freq;    
    0.2 => w.gain;
    
    nextNote => prevNote ;
}

//==========================================================
//Panning: Decide which speaker to use based on a sine wave 
//==========================================================
fun void set_pan(int iteration)
{
    //
    Math.sin(iteration * 4 * Math.PI / 120) => p.pan;
}


//MAIN PROGRAM


now/second => float started_at; //Remember the time we start music creation
initialize_notes();

/*
** A pulse of 0.25s implies 30/0.25 = 120 random notes
** Split 30 secs into 120 durations of 0.25s each.
** In each iteration of the loop, choose the next note randomly 
** from the scale, less than 4 notes from previous note.
** Dervive notes for each oscillator (sine, square, triangle, sawtooth)
** and play all osciallators simultaneously for 250ms.
** Divide 30 seconds duration into 2 full cycles of a sine wave
** Pan the sound based on the above sine wave.
*/

for( 0 => int i; i < 30*4; i++ ) 
{
    set_pan(i);
    play_notes(i);
    play_beats(i); 
    250::ms => now; // move time by 250ms (0.25 seconds) in each iteration
}

//Print the total time for which music was played
<<< "Total Duration", now/second - started_at >>>;