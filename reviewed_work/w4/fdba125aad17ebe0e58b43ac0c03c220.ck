// Assignment 4 - Functional Techno 1
<<< "Assignment 4 - Functional Techno 1" >>>;

/* Look closely at and evaluate the student?s code. Make sure that it runs in miniAudicle without having to edit the code. Note whether the composition is roughly the required length (30 seconds), or if it is drastically shorter or longer. 
Make sure the code includes all of the following:
-Use of Oscillator
-Use of SndBuf
-Use of if/else statements
-Use of for loop or while 
-Use of variables
-Use of comments
-Std.mtof()
-Random Number
-Use of Arrays
-Use of Panning
-Use of right timing (.6::second quarter notes)
-Use of right melodic notes (Eb Mixolydian scale)
-Use of at least three defined functions (-two can be from lecture, one must be newly created) */

// functions to make chords

// sound chain
Gain master => Pan2 p => dac;

SndBuf click =>  master;
SndBuf kick => master;
SndBuf hihat =>  master;
TriOsc chord[3];
for( 0 => int i; i < chord.cap(); i++)
{
    // use array to chuck unit geerator to master
    chord[i] => Pan2 p => master;
    1.0/chord.cap() => chord[i].gain;
}

//load soundfile into sndbuf
me.dir() + "/audio/kick_01.wav" => kick.read;
me.dir() + "/audio/hihat_01.wav" => hihat.read;


//open audio files
me.dir() + "/audio/kick_01.wav" => kick.read;
me.dir() + "/audio/snare_03.wav" => click.read;
//take away playback at initialization
kick.samples() => kick.pos;
click.samples() => click.pos;

/*// KICK array of strings (paths)
string kick_samples [2];

me.dir() + "/audio/kick_02.wav" => kick_samples[0];
me.dir() + "/audio/kick_03.wav" => kick_samples[1];*/






// global variables
//Sequencer patterns
[1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0] @=> int kick_ptrn_1[];
[0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0] @=> int kick_ptrn_2[];
[0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0] @=> int click_ptrn_1[];
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1] @=> int click_ptrn_2[];

// Synth notes array declaration 
[51, 53, 55, 56, 58, 60, 61, 63, 53, 51, 56, 55, 60, 58, 63, 61] @=> int melo1[];



// declare time as durations for a composition at 120bpm
// quarter note
.6::second => dur quarter;
// sixteenth
.5::quarter => dur sixteenth;
// half note
2::quarter => dur half;
// whole note
2::half => dur whole;

// declare variables to count bars and beats
0 => int counter;
0 => int barNum;
0 => int loopTime;


// before we play anything though, lets save the start time so we can
// check for the 30s requirement later.
now => time tStart;
now => time tEnd;
now + 30::second => time tTotal;
// to fadeout
tTotal - 8::sixteenth => time fadetime;

// "home made" function -- random panning
fun void human( float min, float max)
{
        Math.random2f(min, max) => float p1; //generate random number
        p1 => p.pan;
}


// function from video -- synth brain
fun void playChord( int root, string quality, float legnth)
{
    // function will make major or minor chords
    
    // root
    Std.mtof(root) => chord[0].freq;
    
    // third
    if( quality == "major" )
    {
        Std.mtof(root+4) => chord[1].freq;
    }
    
    else if( quality == "minor" )
    {
        Std.mtof(root+3) => chord[1].freq;
    }
    
    // forth
    if( quality == "forth" )
    {
        Std.mtof(root+5) => chord[1].freq;
    }
    
    // fifth
    Std.mtof(root+7) => chord[2].freq;
    
}

// function from video: drum trigger
fun void drumsection( int kickArray[], int clickArray[], float beattime )
{
    //sequencer for bass drums and snare drum beats
    for( 0 => int i; i < kickArray.cap(); i++ )
    {
        // if 1 in array than play kick
        if( kickArray[i] == 1)
        { 
            0 => kick.pos;
        }
        // if 1 in array than play click
        if( clickArray[i] == 1)
        { 
            0 => click.pos;
        }
        human(0.4,0.6);
        .25::sixteenth => now;
    }
}


// MAIN PROGRAM
.5 => master.gain;
// infinite loop
// infanite loop
while(true)
{
    // beat goes from 0 to 15 (16 positions)
    counter % 16 => int beat;
    
    // random pan
    human(0.2,0.9);
    
    //call synth
    playChord(melo1[beat], "minor", .25);
    drumsection(kick_ptrn_2, click_ptrn_2, .7);
    // random pan
    human(0.2,0.9);
    1::sixteenth => now;

    playChord(melo1[beat], "fourth", .25);
    drumsection(kick_ptrn_1, click_ptrn_1, .7);
    counter++;

    // update tTotal
    now => tEnd;
    <<< tEnd >>>;
    
    // when the end is 30 seconds, the loop ends   
    if (tEnd >= tTotal)
    {
        break;
    }
      
    
}
// Print total time
<<< "Total length:", (tEnd - tStart) / second >>>;