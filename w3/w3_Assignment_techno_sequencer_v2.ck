// sound chain
Gain master => dac;

// set volumes main & each sound
.6 => master.gain;

// samples per loop
8 => int samples;

// initialize counter variable
0 => int counter;

// loop duration
30::second => dur loopDuration;

// define time for the loop to end
now + loopDuration => time later;

fun SndBuf readFiles (string File[], float volume) {
    SndBuf sound => master;
    me.dir() + "/audio/" + File[Math.random2(0, File.cap() - 1)] => sound.read;
    volume => sound.gain;
    sound.samples() => sound.pos;
    return sound;
}

["kick_01.wav", "kick_02.wav", "kick_03.wav", "kick_04.wav", "kick_05.wav"] @=> string Kick[];
["snare_01.wav", "snare_02.wav", "snare_03.wav"] @=> string Snare[];
["hihat_01.wav", "hihat_02.wav", "hihat_03.wav", "hihat_04.wav"] @=> string Hihat[];

// LOOP
while (now < later) {

    readFiles(Kick, .4) @=> SndBuf kick;
    readFiles(Snare, .4) @=> SndBuf snare;
    readFiles(Hihat, .4) @=> SndBuf hihat;

    // 8 positions sequencer
    counter % samples => int beat;
    
    // kick
    if (beat == 0 || beat == 2 || beat == 4 || beat == 6) {
        0 => kick.pos;
    }
    
    // snare
    if (beat == 2 || beat == 6) {
        0 => snare.pos;
    }
    
    // hihat
    if (beat == 1 || beat == 3 || beat == 5 || beat == 7) {
        0 => hihat.pos;
    }
     
    <<< "total counter: ", counter, "beat: ", beat >>>;
    
    counter ++;

    .25::second => now;

}