/* ******************************************
    Author: Petar Jercic
    Datum:  06/11/2013
    Note:   Technocracy Composition
******************************************* */

//  Program name
<<< "Assignment 3 - Technocracy Composition" >>>;

//  Get directory path
me.dir()                => string dir_path;
//<<< dir_path >>>;   //  Print dir path

//  Get sound library path
dir_path + "/audio/"    => string snd_dir_path;
//<<< snd_dir_path >>>;   //  Print sound library dir path

//  Initial setup variables define
int     section_no; //  Number of musical sections
float   base_freq;  //  Frequency of the base melody
dur     melody_dur; //  Duration of the composition (default 30 sec).

//  Initial setup variables init
[50, 52, 53, 55, 57, 59, 60, 62]    @=> int midi_notes[];
Math.random2(3, 7) * 2              =>  section_no; //  Random number of sections
//  Random MIDI piano note to start from
Std.mtof(midi_notes[Math.random2(0, midi_notes.cap() - 1)]) =>  base_freq;  
60::second                          =>  melody_dur; //  Song duration

[1, 2]      @=> int harm_melody[];  //  Random choice for melody hamonics
[1, 3, 5]   @=> int harm_arp[];     //  Random choice for arpeggio harmonics

//Math.srandom(135);  //  Random number seed

//  Array of strings (paths to the soundfiles)
string snare_samples[3];
string kick_samples[3];
string hihat_samples[3];
string fx_samples[3];

//  Load array with sound filepaths
snd_dir_path + "snare_01.wav"       => snare_samples[0];
snd_dir_path + "snare_02.wav"       => snare_samples[1];
snd_dir_path + "snare_03.wav"       => snare_samples[2];

snd_dir_path + "kick_01.wav"        => kick_samples[0];
snd_dir_path + "kick_02.wav"        => kick_samples[1];
snd_dir_path + "kick_03.wav"        => kick_samples[2];

snd_dir_path + "hihat_01.wav"       => hihat_samples[0];
snd_dir_path + "hihat_02.wav"       => hihat_samples[1];
snd_dir_path + "hihat_03.wav"       => hihat_samples[2];

snd_dir_path + "stereo_fx_01.wav"   => fx_samples[0];
snd_dir_path + "stereo_fx_02.wav"   => fx_samples[1];
snd_dir_path + "stereo_fx_03.wav"   => fx_samples[2];

//  Master gain object for control
Gain    master  => dac; 

//  Base melody oscilators chain (SinOsc, SqrOsc, TriOsc, SawOsc)
Gain    gain_melody     => master;      //  Master gain for melody object for control
SinOsc  base_osc_sin    => gain_melody;
SqrOsc  base_osc_sqr    => gain_melody;
TriOsc  base_osc_tri    => gain_melody;
SawOsc  base_osc_saw    => gain_melody;

//  Arpeggio oscilators chain (SinOsc, SqrOsc, TriOsc, SawOsc) and panning
Pan2    pan_arp         => dac;
float   pan_rate_arp;
SinOsc  arp_osc_sin     => pan_arp;
SqrOsc  arp_osc_sqr     => pan_arp;
TriOsc  arp_osc_tri     => pan_arp;
SawOsc  arp_osc_saw     => pan_arp;

//  Beat sound chain
Gain    gain_beat       => master; //  Master gain for melody object for control
SndBuf  kick            => gain_beat;
SndBuf  snare           => gain_beat;
SndBuf  hihat           => gain_beat;

//  Effect sound chain
Pan2    pan_fx          => dac;
float   pan_rate_fx;
SndBuf2 fx              => pan_fx;

//  Print general composition details (DEBUG)
//<<< "section_no ",section_no,";melody_dur ",melody_dur/second >>>;
   
//  Loop through all sections in this composition
for (0 => int i; i < section_no; i++) {
    
    //  Choose between intro, outro, verse, chorus or bridge
    int section_def;
    if((i != 0) && (i != section_no - 1))
        Math.random2(1,3)   =>  section_def;
    else
        i => section_def;
    
    //<<< "Section definition: ", section_def >>>;    //  Debug section def
    //  Sequence the beats with sound files
    if(section_def == 0)                            //  Intro
        0 => master.gain;   //  Set master gain to 0
    
    if(section_def == 1 || section_def == 2 ) {     //  Verse and chorus
        //  Load beat samples
        int sample_id;
        Math.random2(0, snare_samples.cap() - 1)    =>  sample_id;  //  Choose random sample
        snare_samples[sample_id]    => snare.read;  //  Read in the sound file
        
        Math.random2(0, kick_samples.cap() - 1)     =>  sample_id;  //  Choose random sample
        kick_samples[sample_id]     => kick.read;   //  Read in the sound file
        
        Math.random2(0, hihat_samples.cap() - 1)    =>  sample_id;  //  Choose random sample
        hihat_samples[sample_id]    => hihat.read;  //  Read in the sound file
        
        //  Set all playheads to the end, silence
        kick.samples()      => kick.pos;
        hihat.samples()     => hihat.pos;
        snare.samples()     => snare.pos;
    }
        
    if(section_def == 3) {  //  Bridge
        //  Load beat samples
        int sample_id;
        Math.random2(0, snare_samples.cap() - 1)    =>  sample_id;  //  Choose random sample
        snare_samples[sample_id]    => snare.read;  //  Read in the sound file
        
        Math.random2(0, kick_samples.cap() - 1)     =>  sample_id;  //  Choose random sample
        kick_samples[sample_id]     => kick.read;   //  Read in the sound file
        
        Math.random2(0, hihat_samples.cap() - 1)    =>  sample_id;  //  Choose random sample
        hihat_samples[sample_id]    => hihat.read;  //  Read in the sound file
        
        Math.random2(0, fx_samples.cap() - 1)       =>  sample_id;  //  Choose random sample
        fx_samples[sample_id]       => fx.read;     //  Read in the sound file
        
        
        //  Set all playheads to the end, silence
        kick.samples()      => kick.pos;
        hihat.samples()     => hihat.pos;
        snare.samples()     => snare.pos;
        
        if(Math.random2(0,1) == 0) {
            0                       => fx.pos;  //  Turn on the effect soundfile
            Math.random2f(0.2, 1.8) => fx.rate; //  Set random reproduction rate
        }
        else {
            fx.samples()                => fx.pos;  //  Turn on the effect soundfile
            -Math.random2f(0.2, 1.8)    => fx.rate; //  Reverse a sample
        }
        
        Math.random2f(0.25, 2.0) =>  pan_rate_fx;
    }
    
    if(section_def >= 4)                            //  Outro
        ;
    
    //  Choose new Frequency of the base melody at 2rd or 4th harmonic
    if (Math.random2(0, 1) > 0)
        Math.max(30, Std.mtof(midi_notes[Math.random2(0, midi_notes.cap() - 1)]) * harm_melody[Math.random2(0, harm_melody.cap() - 1)]) => base_freq;  //  up from base frequency
    else
        Math.max(30, Std.mtof(midi_notes[Math.random2(0, midi_notes.cap() - 1)]) / harm_melody[Math.random2(0, harm_melody.cap() - 1)]) => base_freq;  //  down from base frequency
    
    //  Round to the smallest integer frequency value
    Math.floor(base_freq) => base_freq;
    
    //  Set the value for Frequency of the base melody to oscilators
    base_freq => base_osc_sin.freq;
    base_freq => base_osc_sqr.freq;
    base_freq => base_osc_tri.freq;
    base_freq => base_osc_saw.freq;
    
    //  Set the volume for oscilators of the base melody to zero
    0 => base_osc_sin.gain;
    0 => base_osc_sqr.gain;
    0 => base_osc_tri.gain;
    0 => base_osc_saw.gain;
    
    //  Choose only one oscilator of the base melody 
    Math.random2(0, 3)          => int osc_choice;
    //  Set the volume for oscilators of the base melody to random value 
    //  Leave space for arpeggio oscilators
    Math.random2f(0.0, 0.15)    => float osc_volume;
    
    if (osc_choice == 0) osc_volume => base_osc_sin.gain;
    if (osc_choice == 1) osc_volume => base_osc_sqr.gain;
    if (osc_choice == 2) osc_volume => base_osc_tri.gain;
    if (osc_choice == 3) osc_volume => base_osc_saw.gain;
        
    //  Set the volume for oscilators of the arpeggio to zero
    0 => arp_osc_sin.gain;
    0 => arp_osc_sqr.gain;
    0 => arp_osc_tri.gain;
    0 => arp_osc_saw.gain;
    
    //  Choose only one oscilator of the arpeggio
    Math.random2(0, 3)          => int arp_choice;
    
    //  Set the volume for beats
    Math.random2f(0.35, 0.45)   => float beat_volume;
    beat_volume =>  gain_beat.gain;
    
    //  Set the volume for fx
    Math.random2f(0.35, 0.45)   => float fx_volume;
    fx_volume =>  fx.gain;
    
    if(section_def == 3) {  //  Bridge
        if(Math.random2(0,1) == 0) {
            0 => beat_volume;
            0 => gain_beat.gain;
        }
        else {
            0 => osc_volume;
            0 => base_osc_sin.gain;
            0 => base_osc_sqr.gain;
            0 => base_osc_tri.gain;
            0 => base_osc_saw.gain;
        }
            
    }
    //  Set all playheads to the end, silence
    kick.samples()      => kick.pos;
    hihat.samples()     => hihat.pos;
    snare.samples()     => snare.pos;
    
    //  Initialize sequencer counter variable
    0 => int counter;
    
    //  Arpeggio rate of panning
    Math.random2f(0.5, 1.0) =>  pan_rate_arp;
    
    //  Print section details (DEBUG)
    //<<< "Section ",i+1,";duration ",(melody_dur/section_no)/second >>>;
    //<<< "osc_choice ",osc_choice,";base_freq ",base_freq,";osc_volume",osc_volume >>>;
    
    //  Prepare absolute time for the end of this section
    now + melody_dur / section_no => time section_end;
        
    //  Loop arpeggios for this section
    while (now < section_end) {
        
        //  Section definition execution
        if(section_def == 0)    //  Intro
            Math.min(master.gain() + 0.10, 1.0) => master.gain;   //  Set master gain to 0
        
        if(section_def == 1) {  //  Verse
            
            //  Sequencer beat goes from 0 - 7 (8 positions)
            counter % 4 => int beat;
            
                //  Sequencer definition execution
            if((beat == 0) || (beat == 4)) {  //  Bass drum on 0 and 4
                Math.random2f(0.6, 1.0) => kick.gain;   //  Set volume
                Math.random2f(0.8, 1.2) => kick.rate;   //  Set random reproduction rate
                0   => kick.pos;
            }
            
            if((beat == 2) || (beat == 6)) {            //  Snare drum on 0 and 4
                Math.random2f(0.6, 1.0) => snare.gain;  //  Set volume
                Math.random2f(0.6, 1.4) => snare.rate;  //  Set random reproduction rate
                0   => snare.pos;
            }
            
            0   => hihat.pos;                           //  Snare drum on 0 and 4
            0.2 => hihat.gain;                          //  Set volume
            Math.random2f(0.2, 1.8)     => hihat.rate;  //  Set random rate of playback
            
            //<<< "Counter: ", counter, " Beat: ", beat >>>;  //  Debug print
        }
        
         if(section_def == 2) {  //  Chorus
            
            //  Sequencer beat goes from 0 - 7 (8 positions)
            counter % 4 => int beat;
            
            if((beat == 1) || (beat == 3)) {            //  Snare drum on 0 and 4
                Math.random2f(0.8, 1.0) => snare.gain;  //  Set volume
                Math.random2f(0.8, 1.2) => snare.rate;  //  Set random reproduction rate
                0   => snare.pos;
            }
            
            Math.random2f(0.95, 1.0)    => kick.gain;   //  Set volume
            Math.random2f(0.95, 1.05)   => kick.rate;   //  Set random reproduction rate
            0   => kick.pos;
            
            0   => hihat.pos;                           //  Snare drum on 0 and 4
            0.2 => hihat.gain;                          //  Set volume
            Math.random2f(0.9, 1.1)     =>  hihat.rate;     //  Set random rate of playback
            
            //<<< "Counter: ", counter, " Beat: ", beat >>>;  //  Debug print
        }
        
        if(section_def == 3) {  //  Bridge
            
            //  Sequencer beat goes from 0 - 7 (8 positions)
            counter % 8 => int beat;
             
                //  Sequencer definition execution
            if((beat == 0) || (beat == 4)) {  //  Bass drum on 0 and 4
                Math.random2f(0.9, 1.0)     => kick.gain;   //  Set volume
                Math.random2f(0.85, 1.15)   => kick.rate;   //  Set random reproduction rate
                0   => kick.pos;
            }
            
            if((beat == 2) || (beat == 6)) {                //  Snare drum on 0 and 4
                Math.random2f(0.75, 1.0)    => snare.gain;  //  Set volume
                Math.random2f(0.7, 1.3)     => snare.rate;  //  Set random reproduction rate
                0   => snare.pos;
            }
            
            0   => hihat.pos;                           //  Snare drum on 0 and 4
            0.2 => hihat.gain;                          //  Set volume
            Math.random2f(0.8, 1.2)         =>  hihat.rate;     //  Set random rate of playback
            
            Math.sin(now/pan_rate_fx::second * 2 * pi) => pan_fx.pan;
            
            //<<< "Counter: ", counter, " Beat: ", beat >>>;  //  Debug print
        }
        
        if(section_def >= 4) {  //  Outro
            Math.max(master.gain() - 0.10, 0.0) => master.gain;   //  Set master gain to 0
        }
        
        //  Choose the 1st, 3rd or 5th harmonic of the base melody frequency
        harm_arp[Math.random2(0, harm_arp.cap() - 1)] => int arpp_harmon_no;
        //  Define the .25 second duration of the note, based on duration of melody and this section
        //melody_dur / section_no / Math.random2(1, 35) =>   dur arpp_harmon_duration;
        0.25::second =>   dur arpp_harmon_duration;
        //  If duration is too long, finish the note on the section end
        if (arpp_harmon_duration > (section_end - now))
            section_end - now => arpp_harmon_duration;
        //  Choose random volume for the arrpegio, base on the volume of the base melody
        Math.random2f(0, 1 - osc_volume - beat_volume - fx_volume) => float arpp_harmon_volume;
        
        //  Choose new Frequency of the arrpegio at 1st, 3rd or 5th harmonic
        float arp_harmon_freq;
        if (Math.random2(0, 1) > 0)
            base_freq * arpp_harmon_no => arp_harmon_freq;  //  up from base frequency
        else
            base_freq / arpp_harmon_no => arp_harmon_freq;  //  down from base frequency
        
        //  Set the value for Frequency of the arrpegio to oscilators
        arp_harmon_freq => arp_osc_sin.freq;
        arp_harmon_freq => arp_osc_sqr.freq;
        arp_harmon_freq => arp_osc_tri.freq;
        arp_harmon_freq => arp_osc_saw.freq;
        
        //  Set the volume for choosen oscilators of the arpeggio to random value 
        if (arp_choice == 0) arpp_harmon_volume => arp_osc_sin.gain;
        if (arp_choice == 1) arpp_harmon_volume => arp_osc_sqr.gain;
        if (arp_choice == 2) arpp_harmon_volume => arp_osc_tri.gain;
        if (arp_choice == 3) arpp_harmon_volume => arp_osc_saw.gain;
            
        //  Random sine panning
        Math.sin(now/pan_rate_arp::second * 2 * pi) => pan_arp.pan;
        
        counter++;  //  Increment counter
            
        //  Print note details (DEBUG)
        //<<< "arp_choice ",arp_choice,";arpp_harmon_no ",arpp_harmon_no,";arpp_harmon_volume",arpp_harmon_volume,";arpp_harmon_duration",arpp_harmon_duration/second >>>;
        
        // advance time for the duration of the note
        arpp_harmon_duration => now; 
    }   
}

<<< "End!" >>>;
