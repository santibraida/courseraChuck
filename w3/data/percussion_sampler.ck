// Written by Charles Cave

<<< "Sample Sequencer / Drum Machine" >>>;
 
string sampleFiles[4];

Gain master => dac;
0.6 => master.gain;

me.dir() + "/audio/" => string dirPath;
 
dirPath + "clap_01.wav" => sampleFiles[0]; 
dirPath + "hihat_03.wav"   => sampleFiles[1]; 
dirPath + "kick_04.wav"    => sampleFiles[2]; 
dirPath + "snare_01.wav"   => sampleFiles[3]; 

SndBuf sounds[4];
Pan2   panCtl[4];

for (0 => int n; n < sampleFiles.cap(); n++)
{
    sounds[n] => panCtl[n] => dac;
    sampleFiles[n] => sounds[n].read;
}

// Set Pan Controls
-0.7 => panCtl[0].pan;
-0.2 => panCtl[1].pan;
0.2  => panCtl[2].pan;
0.7  => panCtl[3].pan;

// Sequencer pattern is loaded into four arrays - one array for each sample
// This sequencer has a cycle of 16 steps

[0,1,0,1, 0,0,1,1, 0,0,0,1, 0,1,0,1] @=> int S1[];
[0,1,0,0, 0,1,0,0, 0,1,0,0, 1,1,0,0] @=> int S2[];
[0,0,1,0, 0,0,1,0, 0,0,1,0, 0,0,1,1] @=> int S3[];
[1,1,1,1, 1,1,1,1, 0,0,1,1, 0,0,1,0] @=> int S4[];

16 => int steps;    // Change to 4 or 8 or 12 if you want shorter cycles 

for (0 => int counter; counter < 120 ; counter++)
{
    counter % steps => int beat;  // index into the arrays
    // Set playback position in each sample to the end so it doesnt play
    // until specified later 
    for (0 => int n; n < sampleFiles.cap(); n++)
    {
        sounds[n].samples() => sounds[n].pos;  // position play head to the end 
    }
    <<< counter, beat, S1[beat], S2[beat], S3[beat], S4[beat] >>>;
   
    if (S1[beat] == 1)   // Should Sample 1 be played?
    {
        0 => sounds[0].pos;
    }
    
    if (S2[beat] == 1)  // Should Sample 2 be played?
    {
        0 => sounds[1].pos;
    }
    if (S3[beat] == 1)    // Should Sample 3 be played?
    {
        0 => sounds[2].pos;
    }
    if (S4[beat] == 1)   // Should Sample 4 be played?
    {
        0 => sounds[3].pos;
    }
    // Randomise the playback rate for each Sound
    for (0 => int n; n < sampleFiles.cap(); n++)
    {
        Math.random2f(0.6,1.2) => sounds[n].rate;  // position play head to the end 
    }
    0.25::second => now;
}