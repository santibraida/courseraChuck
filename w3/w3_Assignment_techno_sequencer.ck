<<< "Assignment week 3: Techno sequencer" >>>;

// sound chain
Gain master => dac;
SndBuf kick => master;
SndBuf hihat => master;
SndBuf snare => master;
SndBuf clap => master;
SinOsc s1 => Pan2 ps1 => master;
SinOsc s2 => Pan2 ps2 => master;

// Array declaration of MIDI notes and duration

// 50 MIDI equals to D
// 52 MIDI equals to E
// 53 MIDI equals to F
// 55 MIDI equals to G
// 57 MIDI equals to A
// 59 MIDI equals to B
// 60 MIDI equals to C
// 62 MIDI equals to D

[50, 52, 53, 55, 57, 59, 60, 62] @=> int Scale[];

// set volumes main & each sound
.6 => master.gain;
.4 => kick.gain;
.3 => snare.gain;
.2 => hihat.gain;
.2 => clap.gain;

// global setting
int beat;

// initialize panning position
1.0 => float panPosition;

// samples per loop
8 => int samples;

// initialize counter variable
0 => int counter;

// loop duration
30000::ms => dur loopDuration;

// define time for the loop to end
now + loopDuration => time later;

// load sound files into sndbuf
["kick_01.wav", "kick_02.wav", "kick_03.wav", "kick_04.wav", "kick_05.wav"] @=> string KickFiles[];
["snare_01.wav", "snare_02.wav", "snare_03.wav"] @=> string SnareFiles[];
["hihat_01.wav", "hihat_02.wav", "hihat_03.wav", "hihat_04.wav"] @=> string HihatFiles[];

me.dir() + "/audio/" + KickFiles[Math.random2(0, KickFiles.cap() - 1)] => kick.read;
me.dir() + "/audio/" + SnareFiles[Math.random2(0, SnareFiles.cap() - 1)] => snare.read;
me.dir() + "/audio/" + HihatFiles[Math.random2(0, HihatFiles.cap() - 1)] => hihat.read;
me.dir() + "/audio/clap_01.wav" => clap.read;

// set all playheads to end so no sound is made
kick.samples() => kick.pos;
snare.samples() => snare.pos;
hihat.samples() => hihat.pos;
clap.samples() => clap.pos;

// store number of samples of each sound
kick.samples() => int kickSamples;
snare.samples() => int snareSamples;
hihat.samples() => int hihatSamples;
clap.samples() => int clapSamples;


// LOOP
while (now < later) {
    
    // 8 positions sequencer
    counter % samples => beat;
    
    // kick
    if (beat == 0 || beat == 2 || beat == 4 || beat == 6) {
        0 => kick.pos;
        1.0 => kick.rate;
        Std.mtof(Scale[Math.random2(0,7)]) => s1.freq;
        Std.mtof(Scale[Math.random2(3, 4)]) => s2.freq;
    }
    
    // snare
    if (beat == 2 || beat == 6) {
        0 => snare.pos;
        1.0 => snare.rate;
        Std.mtof(Scale[Math.random2(1, 6)]) => s1.freq;
        Std.mtof(Scale[Math.random2(2, 5)]) => s2.freq;
    }
    
    // hihat
    if (beat == 1 || beat == 3 || beat == 5 || beat == 7) {
        0 => hihat.pos;
        1.0 => hihat.rate;
        Std.mtof(Scale[Math.random2(3, 4)]) => s1.freq;
        Std.mtof(Scale[Math.random2(0, 7)]) => s2.freq;
    }
     
    <<< "total counter: ", counter, "beat: ", beat >>>;
    
    counter ++;

    if (beat % 2 != 0) {
        Math.random2f(.0,.1) => s1.gain;
        Math.random2f(.0,.1) => s2.gain;
        // reversing clap sample only if beat is not even
        clapSamples => clap.pos;
        Math.random2f(-1, -.5) => clap.rate;

    } else {
        Math.random2f(.1,.3) => s1.gain;
        Math.random2f(.1,.3) => s2.gain;
        // playing clap sample forward if beat is even
        0 => clap.pos;
        Math.random2f(.5, 1) => clap.rate;
    }

    Math.random2f(-1, 0) => ps1.pan;
    Math.random2f(0, 1) => ps2.pan;

    250::ms => now;

}