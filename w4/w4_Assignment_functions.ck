<<< "Assignment week 4: Use functions" >>>;

// checklist

// Use of Oscillator
// Use of SndBuf
// Use of if/else statements
// Use of for loop or while 
// Use of variables
// Use of comments
// Std.mtof()
// Random Number
// Use of Arrays
// Use of Panning
// Use of right timing (.6::second quarter notes)
// Use of right melodic notes (Eb Mixolydian scale)
// Use of at least three defined functions (two can be from lecture, one must be newly created)

// sound chain
SawOsc chord[3];
Pan2 p[3];
Gain master => dac;

for (0 => int i; i < chord.cap(); i++) {
    chord[i] => p[i] => master;
    1.0/chord.cap() => chord[i].gain;
    panningPosition (i) => p[i].pan;
}

fun float[] panningPosition (int elements) {

    float SoundPositions[];

    SoundPositions.cap() => int elementQuantity;
    -1 => SoundPositions[0];    
    (elementQuantity - 1) / elementQuantity => float step;

    for (1 => int j; j <= elementQuantity - 1; j++) {
        SoundPositions[j - 1] + step @=> SoundPositions[j] => p[j].pan;
    }

    return SoundPositions;

}

SndBuf kick => master;
SndBuf hihat => master;
SndBuf snare => master;

// Eb Mixolydian mode (D#, F, G, G#, A#, C, C#, D#)
[51, 53, 55, 56, 58, 60, 61, 63] @=> int Scale[];

// time references
.6::second => dur bar;
1::bar => dur q;
2::bar => dur h;
4::bar => dur w;

// global setting
.6 => master.gain;
int beat;
8 => int samples;
0 => int counter;
30::second => dur loopDuration;
now + loopDuration => time later;

// load sound files
["kick_01.wav", "kick_02.wav", "kick_03.wav", "kick_04.wav", "kick_05.wav"] @=> string KickFiles[];
//me.dir() + "/audio/" + KickFiles[Math.random2(0, KickFiles.cap() - 1)] => kick.read;
kick.samples() => kick.pos;
kick.samples() => int kickSamples;
.4 => kick.gain;

["snare_01.wav", "snare_02.wav", "snare_03.wav"] @=> string SnareFiles[];
//me.dir() + "/audio/" + SnareFiles[Math.random2(0, SnareFiles.cap() - 1)] => snare.read;
snare.samples() => snare.pos;
snare.samples() => int snareSamples;
.3 => snare.gain;

["hihat_01.wav", "hihat_02.wav", "hihat_03.wav", "hihat_04.wav"] @=> string HihatFiles[];
//me.dir() + "/audio/" + HihatFiles[Math.random2(0, HihatFiles.cap() - 1)] => hihat.read;
hihat.samples() => hihat.pos;
hihat.samples() => int hihatSamples;
.2 => hihat.gain;

// function will make major or minor chord
fun void playChord(int root, string quality, float lenght) {

    // root
    Std.mtof(root) => chord[0].freq;

    // third
    if (quality == "major") {
        Std.mtof(root + 4) => chord[1].freq;
        } else if (quality == "minor") {
            Std.mtof(root + 3) => chord[1].freq;
        } else {
            <<< "must specify 'major' or 'minor'" >>>;
        }

    // fifth
    Std.mtof(root + 7) => chord[2].freq;

    lenght::ms => now;

}

// MAIN PROGRAM
.1 => master.gain;

// loop

while (now < later) {

    // chord player
    playChord(Math.random2(60, 72), "minor", 250);
    
    // 8 positions sequencer
    counter % samples => beat;
    
    // kick
    if (beat == 0 || beat == 2 || beat == 4 || beat == 6) {
        0 => kick.pos;
        1.0 => kick.rate;
    }
    
    // snare
    if (beat == 2 || beat == 6) {
        0 => snare.pos;
        1.0 => snare.rate;
    }
    
    // hihat
    if (beat == 1 || beat == 3 || beat == 5 || beat == 7) {
        0 => hihat.pos;
        1.0 => hihat.rate;
    }

    <<< "total counter: ", counter, "beat: ", beat >>>;
    
    counter ++;

    250::ms => now;

}