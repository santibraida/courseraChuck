// sound chain
Phasor chord[3];
Gain master => dac;

for (0 => int i; i < chord.cap(); i++) {
    chord[i] => master;
    1.0/chord.cap() => chord[i].gain;
}

// time references
.6::second => dur bar;
1::bar => dur q;
2::bar => dur h;
4::bar => dur w;

// Eb Mixolydian mode (D#, F, G, G#, A#, C, C#, D#)
[51, 53, 55, 56, 58, 60, 61, 63] @=> int Scale[];
[q, h, w] @=> dur Durations;

fun int melody(int root, dur duration) {

}

// function will make major or minor chord
fun void playChord(int root, string quality, float lenght) {

    // root
    Std.mtof(root) => chord[0].freq;

    // third
    if (quality == "major") {
        Std.mtof(root + 4) => chord[1].freq;
    } else if (quality == "minor") {
        Std.mtof(root + 3) => chord[1].freq;
    } else {
        <<< "must specify 'major' or 'minor'" >>>;
    }

    // fifth
    Std.mtof(root + 7) => chord[2].freq;

    lenght::ms => now;

}

// MAIN PROGRAM
.1 => master.gain;

// infinite loop
while (true) {
    playChord(Math.random2(60, 72), "minor", 250);
}