[1,2,4,5,7,8,9] @=> int arr[];


make_array(arr, 2, 12) @=> int rewrite_arr[];

//used for printing the new array to console
repr_int_array(rewrite_arr);  

fun int[] make_array(int arr[], int num, int x) {
    
    /*
    0 - won't do anything
    1 - [2,3,5,7,8,9,1+x]
    2 - [4,5,7,8,9,1+x,2+x]
    3 - and so on.
    */
    
    // return early
    if (num==0) return arr;
    
    int temp_arr[0];
    for(num => int i; i<arr.cap(); i++){
        temp_arr << arr[i];
    }
    for(0 => int j; j<num; j++){
        temp_arr << arr[j] + x;
    }
    return temp_arr;
   
}

fun void repr_int_array(int arr[]) {
    arr.cap() => int num_items;
    chout <= "[";
    for(0 => int i; i<num_items; i++){
        if (i>0) chout <= ",";    
        chout <= arr[i];
    }
    chout <= "]";
    chout <= IO.newline();
}