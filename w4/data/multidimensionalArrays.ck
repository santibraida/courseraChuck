SinOsc osc => dac;

[[67, 250], [65, 250], [63, 250]] @=> int notes[][];

for (0 => int i; i < notes.cap(); i++) {
    Std.mtof(notes[i][0]) => osc.freq;  //Assumes osc is a SinOsc
    notes[i][1]::ms => now;             //Play the note
}