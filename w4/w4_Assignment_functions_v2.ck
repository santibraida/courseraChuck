<<< "Assignment week 4: Use functions" >>>;
// make a new function to load files from arrays and prepare SndBuf, that's my contribution!
// couldn't make it sound like I wanted, panning and volume is very difficult to manage.

// sound chain
3 => int elements;
SinOsc chord[elements];
Pan2 p[elements];
Gain master => dac;

for (0 => int i; i < chord.cap(); i++) {
    chord[i] => p[i] => master;
    1.0 / chord.cap() => chord[i].gain;
}

// Eb Mixolydian mode (D#, F, G, G#, A#, C, C#, D#)
[51, 53, 55, 56, 58, 60, 61, 63] @=> int Scale[];

// time references
.6::second => dur bar;
30::second => dur loopDuration;
now + loopDuration => time later;

// global setting
.6 => master.gain;
int beat;
4 => int samples;
0 => int counter;

// arrays of soundFiles
["kick_01.wav", "kick_02.wav", "kick_03.wav", "kick_04.wav", "kick_05.wav"] @=> string Kick[];
["snare_01.wav", "snare_02.wav", "snare_03.wav"] @=> string Snare[];
["hihat_01.wav", "hihat_02.wav", "hihat_03.wav", "hihat_04.wav"] @=> string Hihat[];

// function to load sound files and prepare SoundBuf
fun SndBuf readFiles (string File[], float volume) {
    SndBuf sound => master;
    me.dir() + "/audio/" + File[Math.random2(0, File.cap() - 1)] => sound.read;
    volume => sound.gain;
    sound.samples() => sound.pos;
    return sound;
}

// function will make major or minor chord
fun void playChord (int root, string quality) {
    
    // root
    Math.sin (now / 1 :: second * 2 * pi) => p[0].pan;
    Std.mtof(root) => chord[0].freq;
    
    // third
    if (quality == "major") {
        swell(.1, .2, .10);
        Math.sin (now / 1 :: second * 2.2 * pi) => p[1].pan;
        Std.mtof(root + 4) => chord[1].freq;
        } else if (quality == "minor") {
            swell(.4, .8, .20);
            Math.sin (now / 1 :: second * 2.2 * pi) => p[1].pan;
            Std.mtof(root + 3) => chord[1].freq;
        } else {
            <<< "Must specify if 'major' or 'minor' chord type." >>>;
        }
    
    // fifth
    Math.sin (now / 1 :: second * 2.4 * pi) => p[2].pan;
    Std.mtof(root + 7) => chord[2].freq;
}

// function that will go up and down the volume
fun void swell(float begin, float end, float grain) {
    for (begin => float j; j < end; j + grain => j) {
        for (0 => int i; i < chord.cap(); i++) {
            j => chord[i].gain;
        }
    }
    
    for (end => float j; j > begin; j - grain => j) {
        for (0 => int i; i < chord.cap(); i++) {
            j => chord[i].gain;
        }
    }
}

// MAIN PROGRAM
.3 => master.gain;

    readFiles(Kick, .2) @=> SndBuf kick;
    readFiles(Snare, .2) @=> SndBuf snare;
    readFiles(Hihat, .2) @=> SndBuf hihat;

// loop
while (now < later) {

    // 8 positions sequencer
    counter % samples => beat;

    // kick
    if (beat == 0 || beat == 2 || beat == 4 || beat == 6) {
        0 => kick.pos;
    }
    
    // snare
    if (beat == 2 || beat == 6) {
        0 => snare.pos;
    }
    
    // hihat
    if (beat == 1 || beat == 3 || beat == 5 || beat == 7) {
        0 => hihat.pos;
    }
    
    Math.random2(0, Scale.cap() - 1) => int randomScaleNote;

    if (beat % 2 == 0) {
        playChord(Scale[randomScaleNote], "minor");
    } else {
        playChord(Scale[randomScaleNote], "major");
    }
    
    <<< "total counter: ", counter, "beat: ", beat >>>;
    
    counter ++;
    
    bar => now;

}