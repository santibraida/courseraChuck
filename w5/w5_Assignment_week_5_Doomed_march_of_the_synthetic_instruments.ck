<<< "Assignment week 5: Doomed march of the synthetic instruments" >>>;
// DOOOOOOMMMMMMMM!!!!!

// Global settings
int beat;
3 => int elements;
0 => int counter; // Initialize counter
8 => int samples; // Samples per loop

// Notes and timing settings
[49, 50, 52, 54, 56, 57, 59, 61] @=> int Notes[]; // Db Phrygian scale
Notes[0] => int initialNote; // Melody starts with the first note os the scale
.75::second => dur ref; // Time refence for a quarter note
30::second => dur loopDuration; // Loop duration
now + loopDuration => time later; // Define time for the loop to end

// Sound chain
Pan2 master => dac;
1 => master.gain;

// Modal bar
ModalBar m => Gain mGain => master;
mGain => Gain mFeedback => Delay delay => mGain;
6 => m.preset; // beats
ref => delay.max;
ref / 4 => delay.delay;
.75 => mFeedback.gain;

// Mandolin
Mandolin chord[elements];

for (0 => int i; i < chord.cap(); i++) {
    chord[i] => master;
    1.0 / chord.cap() => chord[i].gain;
    .5 => chord[i].pluckPos;
    .5 => chord[i].bodySize;

}

// Shakers
Shakers shak => master;
11 => shak.preset;

// SinOsc
SinOsc sin1 => Pan2 p1 => master;
SinOsc sin2 => Pan2 p2 => master;
0.4 => sin1.gain;
0.4 => sin2.gain;

// Impulse
Impulse i => ResonZ filt => master;
220 => filt.freq;
660 => filt.Q;

// Set up SndBuf for drums
["kick_01.wav", "kick_02.wav", "kick_03.wav", "kick_04.wav", "kick_05.wav"] @=> string Kick[];
readFiles(Kick, .3) @=> SndBuf kick;
["snare_01.wav", "snare_02.wav", "snare_03.wav"] @=> string Snare[];
readFiles(Snare, .3) @=> SndBuf snare;
["hihat_01.wav", "hihat_02.wav", "hihat_03.wav", "hihat_04.wav"] @=> string Hihat[];
readFiles(Hihat, .3) @=> SndBuf hihat;

// MAIN PROGRAM

// Loop
while (now < later) {
	
    // Play sinOsc 1
    -1 => p1.pan;
    Std.mtof(play(initialNote - 12, "sinOsc 1")) => sin1.freq;

    // Play sinOsc 2
    1 => p2.pan;
    Std.mtof(play(initialNote - 24, "sinOsc 2")) => sin2.freq;

    // Play Impulse
    Std.mtof(play(initialNote - 12, "Impulse")) => i.next;
    Math.random2f(220, 2200) => filt.freq;

    // Play Mandolin
    playChord(play(initialNote, "Mandolin"), "minor");

    // Play Modal bar
    Std.mtof(play(initialNote + 24, "Modal bar")) => m.freq;
    (Math.random2f(0.2, 0.4)) => m.noteOn;

    // Play Shakers
    Math.random2f(0.0, 128.0) => shak.objects;
    Math.random2f(0.1, 1.0) => shak.decay;
    1.0 => shak.energy;

    // Play Drums
    drummer();
    
    // Advance time by time reference
    ref => now;

}

// FUNCTIONS

// Function will make major or minor chord
fun void playChord (int root, string quality) {

    // Root
    Std.mtof(root) => chord[0].freq;
    .4 => chord[0].noteOn;
    
    // Third
    if (quality == "major") {
        Std.mtof(root + 4) => chord[1].freq;
        .3 => chord[1].noteOn;
        } else if (quality == "minor") {
            Std.mtof(root + 3) => chord[1].freq;
            .3 => chord[1].noteOn;
        } else {
            <<< "Must specify if 'major' or 'minor' chord type." >>>;
        }
    
    // Fifth
    Std.mtof(root + 7) => chord[2].freq;
    .2 => chord[2].noteOn;

}

// Function to load sound files and prepare SoundBuf
fun SndBuf readFiles (string File[], float volume) {
    
    SndBuf sound => master;
    me.dir() + "/audio/" + File[Math.random2(0, File.cap() - 1)] => sound.read;
    volume => sound.gain;
    sound.samples() => sound.pos;
    
    return sound;

}

// Function to generate notes
fun int play (int playNote, string instrument) {

    Math.random2(-5, 5) => int delta;
    playNote + delta => int nextNote;

    if ((nextNote > Notes[Notes.cap() - 1]) || (nextNote < 0)) {
        playNote - delta => nextNote;
    }

    <<< instrument, "is playing midi note: ", nextNote >>>;

    return nextNote;

}

// Function to control drumming sequence
fun void drummer() {
	
    .2 => kick.gain;
    .2 => snare.gain;
    .1 => hihat.gain;
    
    // 8 positions sequencer
    counter % samples => beat;
	
    if (beat % 2 == 0) {
		// kick
        0 => kick.pos;
	}
	
    if (beat == 2 || beat == 6) {
        // snare
        0 => snare.pos;
    }
    
    if (beat == 1 || beat == 3 || beat == 5 || beat == 7) {
        // hihat
        0 => hihat.pos;
    }
	
    counter ++ ;
	
    <<< "total counter: ", counter, "beat: ", beat >>>;

}