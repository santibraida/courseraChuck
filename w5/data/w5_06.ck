SqrOsc clar => ADSR env => dac;
(0.5::second, 0.1::second, 0.1, 2.0::second);

1 => env.keyOn;
2.0::second => now;

1 => env.keyOff;
2.0::second => now;