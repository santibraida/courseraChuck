// sound chain
Impulse imp => ResonZ filt => dac; 
800.0 => filt.freq;
400 => filt.Q;

200.0 => imp.next; // generate 1 for on sample

2.0::second => now;
