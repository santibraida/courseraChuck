// sound chain
Mandolin m => dac;

0.6 => m.pluckPos;
1.0 => m.noteOn; // plucks strings

//0.5 => m.stringDetune;
0.7 => m.bodySize;
500 => m.freq;

2.0::second => now;