// sound chain
SinOsc vib => SawOsc viol => ADSR env => dac;
0.1 => dac.gain;
(0.5::second, 0.1::second, 0.6, 0.5::second) => env.set;

660 => viol.freq; // change pitch
6.0 => vib.freq; // vibrato freq
2 => viol.sync; // set sync mode to FM (2)

3.0 => vib.gain; // change vibrato

1 => env.keyOn; // turn envelope on
2.0::second => now;

1 => env.keyOff; // turn envelope off
2.0::second => now;