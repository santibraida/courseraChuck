// sound chain
Impulse imp => dac;

while (true) {
    0.1::second => now;
    1.0 => imp.next;
} 