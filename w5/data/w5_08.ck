// sound chain
SinOsc mod => SinOsc car => ADSR env => dac;
0.1 => dac.gain;
(0.5::second, 0.1::second, 0.6, 0.5::second) => env.set;

100.0 => car.freq; // change pitch
100.0 => mod.freq; // vibrato freq
10000 => mod.gain;
2 => car.sync; // set sync mode to FM (2)

1 => env.keyOn; // turn envelope on
2.0::second => now;

1 => env.keyOff; // turn envelope off
2.0::second => now;