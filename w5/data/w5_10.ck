// sound chain
SinOsc mod => SinOsc car => ADSR env => dac;
0.1 => dac.gain;
(0.1::second, 0.01::second, 0.6, 0.2::second) => env.set;

10000 => mod.gain;
2 => car.sync; // set sync mode to FM (2)

while (true) {
    Math.random2f(100.0, 1000.0) => car.freq;
    Math.random2f(100.0, 1000.0) => mod.freq;
    1 => env.keyOn;
    0.2::second => now;
    1 => env.keyOff;
    0.2::second => now;
}