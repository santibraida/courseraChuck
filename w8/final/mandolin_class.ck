// mandolin_class.ck
// Assignment_Final_Mandolines_from_hell

public class Mando {

    // make an arrary of four mandolin strings and connect them up
    Mandolin m[4];

    // set all four string frequencies in one function
    fun void freqs (float gStr, float aStr, float dStr, float eStr) {
        
        m[0].freq (gStr);
        m[1].freq (aStr);
        m[2].freq (dStr);
        m[3].freq (eStr);

    }
    
    // set all four string notes in one function        
    fun void notes (int gNote, int aNote, int dNote, int eNote)  {
        
        m[0].freq (Std.mtof(gNote));
        m[1].freq (Std.mtof(aNote));
        m[2].freq (Std.mtof(dNote));
        m[3].freq (Std.mtof(eNote));

    }
    
    // more MandoPlayer functions
    // damp all strings by amount
    // 0.0 = lots of damping, 1.0 = none
    fun void damp (float amount) { 
        
        for (0 => int i; i < 4; i++) {
            amount => m[i].stringDamping;
        }

    }
    
    // a few named chords to get you started, add your own!!
    fun void chord (string which) {
        
        if (which == "C") {this.notes(64, 67, 71, 72);}
        if (which == "D") {this.notes(62, 66, 69, 73);}
        if (which == "E") {this.notes(63, 64, 68, 71);}
        if (which == "F") {this.notes(64, 65, 69, 72);}
        if (which == "G") {this.notes(62, 66, 67, 71);}
        if (which == "A") {this.notes(64, 68, 69, 73);}
        if (which == "B") {this.notes(63, 66, 70, 71);}

    }
    
    // roll a chord from lowest note to highest at rate
    fun void roll (string chord, dur rate) {
        
        this.chord (chord);
        
        for (0 => int i; i < 4; i++)  {
            1 => m[i].noteOn;
            rate => now;
        }

    }
    
    // archetypical mandolin strumming
    fun void strum(int octaveVariation, int note, dur howLong) {
        
        int whichString;
        
        if (note < 62) {
            0 => whichString;
        } else if (note < 69) {
            1 => whichString;
        } else if (note < 76) {
            2 => whichString;
        } else {
            3 => whichString;
        }

        now + howLong => time stop;

        Std.mtof(note + octaveVariation) => m[whichString].freq;

        while (now < stop) {
            Std.rand2f(0.5, 1.0) => m[whichString].noteOn;
            Std.rand2f(0.05, 0.09) :: second => now;
        }

    }

}