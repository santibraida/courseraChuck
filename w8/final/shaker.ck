// shaker.ck
// Assignment_Final_Mandolines_from_hell

class Shaker {

	//sound chain
	Pan2 master => dac;

	// Shakers
	Shakers shak => master;
    .5 => shak.gain;

    // Play Shakers
    fun void player() {
        
        Math.random2(1, 16) => shak.preset;
        Math.random2f(0.0, 128.0) => shak.objects;
        Math.random2f(0.1, 1.0) => shak.decay;
        1.0 => shak.energy;
    
    }

}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

// instantiating the class
Shaker s1;

// loop
while (true) {

    0 => int i;

    while (i < durations.cap() - 1) {
        
        s1.player();
        durations[i] => now; // advance time

        i++;

    }

}