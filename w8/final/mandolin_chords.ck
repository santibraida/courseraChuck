// mandolin_chords.ck
// Assignment_Final_Mandolines_from_hell

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

["G", "G", "G", "D", "D", "G", "D", "C", "D", "C", "E", "A", "B", "C", "G", "G", "G", "D", "D", "G", "D", "C", "D", "C", "E", "A", "B", "C", "G", "G", "G", "D", "D", "G", "D", "C", "D", "C", "E", "A"] @=> string chords[]; // chords

[67, 64, 69, 67, 72, 67, 67, 65, 64, 62, 64, 62, 62, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 64, 65, 67, 69, 74, 72, 71, 69, 71, 72] @=> int melody[]; // melodic line

// instantiate class
Mando m;
m.m[0] => JCRev rev => Gain master => dac;
m.m[1] => rev;
m.m[2] => rev;
m.m[3] => rev;
0.4 => rev.mix;
.3 => m.m[0].gain => m.m[1].gain => m.m[2].gain => m.m[3].gain;

.8 => m.m[0].pluckPos => m.m[1].pluckPos => m.m[2].pluckPos => m.m[3].pluckPos;
.8 => m.m[0].bodySize => m.m[1].bodySize => m.m[2].bodySize => m.m[3].bodySize;
.5 => m.m[1].stringDamping => m.m[2].stringDamping => m.m[2].stringDamping => m.m[3].stringDamping;
.9 => m.m[1].stringDetune => m.m[2].stringDetune => m.m[2].stringDetune => m.m[3].stringDetune;

// counter to iterate through the arrays
0 => int i;

// roll the basic chords, reading through the arrays
while (i < chords.cap()) {
    m.roll(chords[i],durations[i]);
    i++;
}

// damp it to silence, letting it ring a little
m.damp(0.01);
1.0 :: second => now;