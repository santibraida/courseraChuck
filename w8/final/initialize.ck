// initialize.ck
// Assignment_Final_Mandolines_from_hell

// adding BPM class
Machine.add(me.dir() + "/BPM_class.ck") => int bpmID;

// adding Mandolin class
Machine.add(me.dir() + "/mandolin_class.ck") => int MandolinClassID;

// adding Score
Machine.add(me.dir() + "/score.ck") => int scoreID;