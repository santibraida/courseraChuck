// modalBar.ck
// Assignment_Final_Mandolines_from_hell

class Modal {
    
    BPM tempo;
    tempo.n2 => dur n2;
    tempo.n4 => dur n4;
    tempo.n8 => dur n8;
    
    //sound chain
    Pan2 master => dac;
        
    // Modal bar
    ModalBar m => Gain mGain => master;
    mGain => Gain mFeedback => Delay delay => mGain;
    6 => m.preset; // beats
    n2 => delay.max;
    n4 => delay.delay;
    .75 => mFeedback.gain;

    [67, 64, 69, 67, 72, 67, 67, 65, 64, 62, 64, 62, 62, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 64, 65, 67, 69, 74, 72, 71, 69, 71, 72, 67, 64, 69, 67, 72, 67, 67, 65, 64, 62, 64, 62, 62, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 64, 65, 67, 69, 74, 72, 71, 69, 71, 72] @=> int melody[]; // melodic line

    melody[0] => int initialNote;

    // Function to generate notes
    fun int nextNote (int playNote) {

        Math.random2(-5, 5) => int delta;
        playNote + delta => int nextNote;

        if ((nextNote > melody[melody.cap() - 1]) || (nextNote < 0)) {
            playNote - delta => nextNote;
        }

        return nextNote;

    }
    
    fun void player() {

        Std.mtof(nextNote(initialNote + 24)) => m.freq;
        (Math.random2f(0.2, 0.4)) => m.noteOn;

    }
}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

Modal mb1;

// loop
while (true) {
    
    0 => int i;
    
    while (i < durations.cap()) {

        mb1.player();
        durations[i] => now; // advance time

        i++;

    }
}