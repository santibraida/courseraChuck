// osc.ck
// Assignment_Final_Mandolines_from_hell

class Osc {

	//sound chain
	Pan2 master => dac;

	// SinOsc
	SinOsc sin1 => Pan2 p1 => master;
	SinOsc sin2 => Pan2 p2 => master;
	0 => sin1.gain;
	0 => sin2.gain;

	// Impulse
	Impulse i => ResonZ filt => master;
	220 => filt.freq;
	660 => filt.Q;

    [67, 64, 69, 67, 72, 67, 67, 65, 64, 62, 64, 62, 62, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 64, 65, 67, 69, 74, 72, 71, 69, 71, 72, 67, 64, 69, 67, 72, 67, 67, 65, 64, 62, 64, 62, 62, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 74, 71, 67, 72, 71, 69, 67, 64, 65, 67, 69, 74, 72, 71, 69, 71, 72] @=> int melody[]; // melodic line

    melody[0] => int initialNote;

    fun int nextNote (int playNote) {

        Math.random2(-5, 5) => int delta;
        playNote + delta => int nextNote;

        if ((nextNote > melody[melody.cap() - 1]) || (nextNote < 0)) {
            playNote - delta => nextNote;
        }

        return nextNote;

    }

    fun void player() {
        
        // Play sinOsc 1
        -1 => p1.pan;
        0.1 => sin1.gain;
        Std.mtof(nextNote(initialNote - 12)) => sin1.freq;

        // Play sinOsc 2
        1 => p2.pan;
        0.1 => sin2.gain;
        Std.mtof(nextNote(initialNote - 24)) => sin2.freq;

        // Play Impulse
        Std.mtof(nextNote(initialNote - 12)) => i.next;
        Math.random2f(220, 2200) => filt.freq;

    }

}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4,  n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

Osc o1;

// loop
while (true) {

    0 => int i;

    while (i < durations.cap() - 1) {
        
        o1.player();
        durations[i] => now; // advance time

        i++;

    }

}