// BPM.ck
// Assignment_Final_Mandolines_from_hell

// global BPM conductor Class
public class BPM {
    
    // global variables
    dur myDuration[6];
    static dur n1, n2, n4, n8, n16, n32;
    static float beat;
    
    fun void tempo(float beat) {
        
        // beat is BPM, example 120 beats per minute
        60.0 / (beat) => float beat; // seconds per beat
        beat :: second => n4;
        n4 * 2 => n2;
        n2 * 2 => n1;
        n4 * .5 => n8;
        n8 * .5 => n16;
        n16 * .5 => n32;
        
        // store data in array
        [n1, n2, n4, n8, n16, n32] @=> myDuration;
        
    }

}