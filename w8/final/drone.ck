// drone.ck
// Assignment_Final_Mandolines_from_hell

class Drone {

	//sound chain
	Gain master => dac;

    3 => int elements;

	// Singers
	VoicForm singerDrones[elements];
	NRev droneRev => master;
    .2 => droneRev.mix;
	
    for (0 => int i; i < singerDrones.cap(); i++) {
	    singerDrones[i] => droneRev;
	    (.8 / singerDrones.cap()) => singerDrones[i].gain;
	}

    fun void player() {
        
        for (0 => int i; i < singerDrones.cap(); i++) {
            .02 => singerDrones[i].vibratoGain;
            "lll" => singerDrones[i].phoneme;
            Std.mtof(Math.random2(62, 74)) => singerDrones[i].freq;
        }

    }

}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

Drone d1;
0 => int i;

// loop
while (i < durations.cap()) {

    d1.player();
    durations[i] => now; // advance time

    i++;

}