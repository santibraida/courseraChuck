// score.ck
// Assignment_Final_Mandolines_from_hell

// RECORDING WAV
dac => WvOut2 w => blackhole;
me.path() + ".wav" => w.wavFilename;
1 => w.record;

60.1::second => dur loopDuration; // Loop duration, adding 0.1 second to be sure that the loop ends after all the machines were removed.
now + loopDuration => time later; // Define time for the loop to end

// main program
spork ~ setScore();

// loop
while (now < later) {
	1::second => now;
}

fun void setScore() {

	BPM tempo;
	tempo.tempo(80.0);

	// add shakers
	Machine.add(me.dir() + "/shaker.ck") => int shakeID;
	
	// add drone
	Machine.add(me.dir() + "/drone.ck") => int droneID;
	
	// add modal Bar
	Machine.add(me.dir() + "/ModalBar.ck") => int modalID;

	// add oscillators
	Machine.add(me.dir() + "/osc.ck") => int oscID;
	3.0 * tempo.n4 => now;

	// add drums
	Machine.add(me.dir() + "/kick.ck") => int kickID;
	Machine.add(me.dir() + "/snare.ck") => int snareID;
	Machine.add(me.dir() + "/hat.ck") => int hatID;
	3.0 * tempo.n4 => now;

	// add mandolin chords
	Machine.add(me.dir() + "/mandolin_chords.ck") => int mandolinChordsID;
	20.0 * tempo.n4 => now;

	// remove band
	Machine.remove(mandolinChordsID);
	Machine.remove(shakeID);
	Machine.remove(droneID);
	Machine.remove(modalID);
	Machine.remove(oscID);

	// add solo: mandolin arpeggio
	Machine.add(me.dir() + "/mandolin_arpeggio.ck") => int mandolinArpeggioID;
	20.0 * tempo.n4 => now;

	// remove band
	Machine.remove(mandolinArpeggioID);
	Machine.add(me.dir() + "/shaker.ck") => shakeID;
	Machine.add(me.dir() + "/drone.ck") => droneID;
	Machine.add(me.dir() + "/ModalBar.ck") => modalID;
	Machine.add(me.dir() + "/osc.ck") => oscID;
	
	// adding mandolin chords
	Machine.add(me.dir() + "/mandolin_chords.ck") => mandolinChordsID;
	16.0 * tempo.n4 => now;

	// remove drums
	Machine.remove(kickID);
	Machine.remove(snareID);
	Machine.remove(hatID);
	3.0 * tempo.n4 => now;

	// remove bass
	Machine.remove(oscID);
	3.0 * tempo.n4 => now;

	// remove drone
	Machine.remove(droneID);
	3.0 * tempo.n4 => now;

	// remove drone & shake
	Machine.remove(shakeID);
	3.0 * tempo.n4 => now;

	// remove modal bar
	Machine.remove(modalID);
	6.0 * tempo.n4 => now;
	
	// remove mandolin chords again
	Machine.remove(mandolinChordsID);
	
}

// RECORDING WAV. Stop recording sound file
0 => w.record;