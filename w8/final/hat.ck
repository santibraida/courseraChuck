// hat.ck
// Assignment_Final_Mandolines_from_hell

// sound chain
SndBuf hat => dac;
me.dir(-1) + "/audio/hihat_02.wav" => hat.read;
hat.samples() => hat.pos;
0.06 => hat.gain;

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8] @=> dur durations[]; // rythm pattern

// loop
while (true) {
    
    0 => int i;

    while (i < durations.cap() - 1) {

        0 => hat.pos;
        durations[i] => now; // advance time

        i++;

    }
    
}