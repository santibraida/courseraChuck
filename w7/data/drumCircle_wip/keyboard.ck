// keyboard.ck
// Assignment 7: too moog noise

class Keyboard {

    // sound chain
    Pan2 master => dac;
    Moog key[3];
    Echo echo[3];
    Chorus chorus[3];

    // setting parameters for every moog
    for (0 => int i; i < key.cap(); i++) {
        
        key[i] => echo[i] => chorus[i] => master;
        (.6 / key.cap()) => key[i].gain;
        .2 => key[i].filterQ;
        440 => key[i].vibratoFreq;
        .1 => key[i].vibratoGain;
        .1 => key[i].afterTouch;
        1.25::second => echo[i].max;
        0.625::second => echo[i].delay;
        .1 => chorus[i].modFreq;
        .1 => chorus[i].modDepth;
        .4 => chorus[i].mix;

    }

    // function to play the moogs
    fun void keyPlayer(int walkPos, int scaleArray[], dur durationsArray[]) {
        
        for (0 => int i; i < durationsArray.cap() - 1; i++) {
            
            Math.random2(-3, 3) +=> walkPos;
        
            if (walkPos < 0) {
                1 => walkPos;
            } else if (walkPos >= scaleArray.cap()) {
                scaleArray.cap() - 2 => walkPos;
            }

            playChord(scaleArray[walkPos], "minor");

        }

    }

    // Function will make major or minor chord
    fun void playChord (int root, string quality) {

        // Root
        Std.mtof(root) => key[0].freq;
        1 => key[0].noteOn;
        
        // Third
        if (quality == "major") {
            Std.mtof(root + 4) => key[1].freq;
            .6 => key[1].noteOn;
        } else if (quality == "minor") {
            Std.mtof(root + 3) => key[1].freq;
            .6 => key[1].noteOn;
        } else {
            <<< "Must specify if 'major' or 'minor' chord type." >>>;
        }
        
        // Fifth
        Std.mtof(root + 7) => key[2].freq;
        .4 => key[2].noteOn;

    }

}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4,  n2, n4, n8, n8, n8, n8, n2] @=> dur durations[]; // rythm pattern

[48, 50, 52, 53, 55, 57, 59, 60] @=> int scale[]; // C Ionian mode

// instantiating the class Keyboard into the object k1
Keyboard k1;
0 => int i;

// loop
while (i < durations.cap()) {

    k1.keyPlayer(4, scale, durations);
    durations[i] => now; // advance time

    i++;

}