// player.ck
// Assignment 7: too moog noise

public class Player {
    
    fun void playMusic (int walkPos, int scaleArray[], dur durationsArray[]) {

        for (0 => int i; i < durationsArray.cap() - 1; i++) {
            
            Math.random2(-1, 1) +=> walkPos;
        
            if (walkPos < 0) {
                1 => walkPos;
            } else if (walkPos >= scaleArray.cap()) {
                scaleArray.cap() - 2 => walkPos;
            }

            playChord(scaleArray[walkPos], "minor");

        }

    }

    fun void playChord (int root, string quality) {

        // Root
        Std.mtof(root) => bass[0].freq;
        -1 => bassPan[0].pan;

        // Third
        if (quality == "major") {
            Std.mtof((root - 24) + 4) => bass[1].freq;
            0 => bassPan[1].pan;
        } else if (quality == "minor") {
            Std.mtof((root - 24) + 3) => bass[1].freq;
            0 => bassPan[1].pan;
        } else {
            <<< "Must specify if 'major' or 'minor' chord type." >>>;
        }
        
        // Fifth
        Std.mtof((root - 12) + 7) => bass[2].freq;
        1 => bassPan[2].pan;

    }

}