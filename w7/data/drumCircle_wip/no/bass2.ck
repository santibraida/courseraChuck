// bass.ck
// Assignment 7: too moog noise

// sound chain
Pan2 master => dac;
SinOsc bass[3];
Pan2 bassPan[3];
PitShift bassEffect[3];

for (0 => int i; i < bass.cap(); i++) {
    bass[i] => bassEffect[i] => bassPan[i] => master;
    .2 => bass[i].gain;
    .7 => bassEffect[i].mix;
    .5 => bassEffect[i].shift;
}

// MAIN PROGRAM

// timing variables
Player player;
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8] @=> dur durations[]; // rythm pattern

[48, 50, 52, 53, 55, 57, 59, 60] @=> int scale[]; // C Ionian mode

// instantiating the class bass into the object b1
0 => int i;

// loop
while (i < durations.cap()) {

    player.playMusic (4, scale, durations);
    durations[i] => now; // advance time

    i++;

}