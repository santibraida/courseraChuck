// bass.ck
// Assignment 7: too moog noise

class Bass {
    
    // sound chain
    Pan2 master => dac;
    SinOsc bass[3];
    Pan2 bassPan[3];
    PitShift bassEffect[3];

    for (0 => int i; i < bass.cap(); i++) {
        bass[i] => bassEffect[i] => bassPan[i] => master;
        .2 => bass[i].gain;
        .7 => bassEffect[i].mix;
        .5 => bassEffect[i].shift;
    }

    // function to play the bass
    fun void bassPlayer(int walkPos, int scaleArray[], dur durationsArray[]) {

        for (0 => int i; i < durationsArray.cap() - 1; i++) {
            
            Math.random2(-1, 1) +=> walkPos;
        
            if (walkPos < 0) {
                1 => walkPos;
            } else if (walkPos >= scaleArray.cap()) {
                scaleArray.cap() - 2 => walkPos;
            }

            playChord(scaleArray[walkPos], "minor");

        }

    }

    // Function will make major or minor chord
    fun void playChord (int root, string quality) {

        // Root
        Std.mtof(root) => bass[0].freq;
        -1 => bassPan[0].pan;

        // Third
        if (quality == "major") {
            Std.mtof((root - 24) + 4) => bass[1].freq;
            0 => bassPan[1].pan;
        } else if (quality == "minor") {
            Std.mtof((root - 24) + 3) => bass[1].freq;
            0 => bassPan[1].pan;
        } else {
            <<< "Must specify if 'major' or 'minor' chord type." >>>;
        }
        
        // Fifth
        Std.mtof((root - 12) + 7) => bass[2].freq;
        1 => bassPan[2].pan;

    }

}

// MAIN PROGRAM

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;

[n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8] @=> dur durations[]; // rythm pattern

[48, 50, 52, 53, 55, 57, 59, 60] @=> int scale[]; // C Ionian mode

// instantiating the class bass into the object b1
Bass b1;
0 => int i;

// loop
while (i < durations.cap()) {

    b1.bassPlayer(4, scale, durations);
    durations[i] => now; // advance time

    i++;

}