// score.ck
// Assignment 7: too moog noise

// RECORDING WAV
dac => WvOut2 w => blackhole;
me.path() + ".wav" => w.wavFilename;
1 => w.record;

30.1::second => dur loopDuration; // Loop duration, adding 0.1 second to be sure that the loop ends after all the machines were removed.
now + loopDuration => time later; // Define time for the loop to end

// main program
spork ~ setScore();

// loop
while (now < later) {
	1::second => now;
}

fun void setScore() {

	BPM tempo;
	tempo.tempo(96.0); // 60000 ms divided by quarter note duration (625) = 96 BPM

	// adding kick
	Machine.add(me.dir() + "/kick.ck") => int kickID;
	3.0 * tempo.n4 => now;

	// adding snare
	Machine.add(me.dir() + "/snare.ck") => int snareID;
	3.0 * tempo.n4 => now;

	// adding hat
	Machine.add(me.dir() + "/hat.ck") => int hatID;
	3.0 * tempo.n4 => now;

	// adding bass
	Machine.add(me.dir() + "/bass2.ck") => int bassID;
	12.0 * tempo.n4 => now;

	// adding keyboard
	Machine.add(me.dir() + "/keyboard.ck") => int keyboardID;
	17.0 * tempo.n4 => now;

	// remove drums
	Machine.remove(kickID);
	Machine.remove(snareID);
	Machine.remove(hatID);
	10.0 * tempo.n4 => now;
	
	// remove bass & keyboard
	Machine.remove(keyboardID);
	Machine.remove(bassID);

}

// RECORDING WAV. Stop recording sound file
0 => w.record;