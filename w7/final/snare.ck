// snare.ck
// Assignment 7: too moog noise

// sound chain
SndBuf snare => dac;
me.dir(-1) + "/audio/snare_01.wav" => snare.read;
snare.samples() => snare.pos;
0.1 => snare.gain;

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;
[n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4, n8] @=> dur durations[]; // rythm pattern

// loop
while (true) {
    
    0 => int i;

    while (i < durations.cap() - 1) {

        0 => snare.pos;
        durations[i] => now; // advance time

        i++;

    }
    
}