// kick.ck
// Assignment 7: too moog noise

// sound chain
SndBuf kick => dac;
me.dir(-1) + "/audio/kick_04.wav" => kick.read;
kick.samples() => kick.pos;
0.1 => kick.gain;

// timing variables
BPM tempo;
tempo.n2 => dur n2;
tempo.n4 => dur n4;
tempo.n8 => dur n8;
[n8, n8, n4, n4, n4, n4, n4, n8, n8, n4, n4, n4, n4, n2, n4, n8, n8, n8, n8, n2, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n4 + (n4/2), n8, n4, n4, n4, n4, n2, n4, n4, n4, n4] @=> dur durations[]; // rythm pattern

// loop
while (true) {

	0 => int i;

    while (i < durations.cap() - 1) {
        
        0 => kick.pos;
        durations[i] => now; // advance time

        i++;

    }

}