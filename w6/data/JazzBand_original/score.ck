// define class 
class fixDir
{
    // declare instance variable 'm_dir'
    string m_dir;
        
    // function that returns value of m_dir
    fun string dir() {
    
        me.dir() => m_dir;
        if (m_dir.substring(1,1) == ":")
        m_dir.replace(1,1,"\\:");
        return m_dir;
     }
}

// instantiate an fixDir
fixDir my;

// score.ck

// paths to chuck file
my.dir() + "/piano.ck" => string pianoPath;
my.dir() + "/bass.ck" => string bassPath;
my.dir() + "/drums.ck" => string drumsPath;
my.dir() + "/flute.ck" => string flutePath;

// start piano
Machine.add(pianoPath) => int pianoID; 
4.8::second => now;

// start drums
Machine.add(drumsPath) => int drumsID;
4.8::second => now;

// start bass
Machine.add(bassPath) => int bassID;
4.8::second => now;

// start flute solo
Machine.add(flutePath) => int fluteID;
4.8::second => now;

// remove drums
Machine.remove(drumsID);
4.8::second => now;

// add drums back in 
Machine.add(drumsPath) => drumsID;

