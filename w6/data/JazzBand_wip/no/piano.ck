// piano.ck
// Assignment 6: Annoying Guitar Jazz Band (A.G.J.B.)

[46, 48, 49, 51, 53, 54, 56, 58] @=> int Scale[]; // Bb Aeolian mode

// Sound chain
Pan2 master => dac;
Rhodey piano[4];
piano[0] => Pan2 p0 => master;
piano[1] => Pan2 p1 => master;
piano[2] => Pan2 p2 => master; 
piano[3] => Pan2 p3 => master;
.6 => piano[0].gain => piano[1].gain => piano[2].gain => piano[3].gain;

[0.5, 1.0, 1.0, 0.5] @=> float p[];

0.625::second => dur q;

// Loop
while (true) {

    0 => int i;

    Math.random2(1, 7) => int factor;

    while (i < 7) {
        
        1 => p0.pan;
        -1 => p1.pan;
        -1 => p2.pan;
        1 => p3.pan;
        
        if (i + factor < 7) {
            Std.mtof(Scale[i + factor]) => piano[0].freq;
            Std.mtof(Scale[i + factor] * -1) => piano[2].freq;
            Std.mtof(Scale[i + factor] + 12) => piano[1].freq;
            Std.mtof((Scale[i + factor] + 12) * -1) => piano[3].freq;
        } else {
            Std.mtof(Scale[i]) => piano[0].freq;
            Std.mtof(Scale[i] * -1) => piano[2].freq;
            Std.mtof(Scale[i] + 12) => piano[1].freq;
            Std.mtof((Scale[i] + 12) * -1) => piano[3].freq;
        }
        
        .3 => piano[0].noteOn;
        .7 => piano[2].noteOn;
        .3 => piano[1].noteOn;
        .7 => piano[3].noteOn;
        
        for (0 => int i; i < 3; i++){
            q * p[i] => now;
        }
        
        i++;
    }

}