// solo.ck
// Assignment 6: Annoying Guitar Jazz Band (A.G.J.B.)

[46, 48, 49, 51, 53, 54, 56, 58] @=> int Scale[]; // Bb Aeolian mode
0.625::second => dur q;

[0.5, 0.5, 1.0, 1.0] @=> float p[];

// sound chain
Pan2 master => dac;

// solo
HevyMetl solo => Echo echo => master;
q => echo.max;
q / 4 => echo.delay;
.1 => solo.gain;

// SinOsc 1
SinOsc s1 => Pan2 p1 => master;
.3 => s1.gain;
-1 => p1.pan;

// SinOsc 2
SinOsc s2 => Pan2 p2 => master;
.3 => s2.gain;
1 => p2.pan;

while (true)  {

    Math.random2(0, Scale.cap() - 1) => int note;
    
    // solo freq
    Math.mtof(Scale[note] + 12) => solo.freq;
    
    // SinOsc 1 freq
    Math.mtof(Scale[note] - 12) => s1.freq;
    
    // SinOsc 2 freq
    Math.mtof(Scale[note] - 24) => s2.freq;
    
    // note duration
    Math.random2f(.50, .75) => solo.noteOn;
    
    2 => solo.controlOne;
    1 => solo.controlTwo;

    for (0 => int i; i < 3; i++) {
        // advance time
        q * p[i] => now;
    }

}